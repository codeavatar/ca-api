<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\lib\encrypt;


abstract class IEncrypt
{
    protected abstract function  aes_encrypt($str, $secret_key);
    protected abstract function  aes_decrypt($str, $secret_key);
}