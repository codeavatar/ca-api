<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\lib\encrypt;

/**
 * Class OpensslLib 加密工具类 (PHP 7.0+ 以下可用)
 * @package toolkit\lib\encrypt
 */
class OpensslLib extends IEncrypt
{
    /**
     * 加密方法
     * @param $str 明文
     * @param $secret_key 密钥（16位）
     * @return string
     */
    protected function aes_encrypt($str, $secret_key)
    {
        // openssl_encrypt 加密不同Mcrypt，对秘钥长度要求，超出16加密结果不变
        $data = openssl_encrypt($str, 'AES-128-ECB', $secret_key, OPENSSL_RAW_DATA);
        return strtolower(bin2hex($data));
    }

    /**
     * 解密方法
     * @param $str 密文
     * @param $secret_key 密钥（16位）
     * @return false|string
     */
    protected function aes_decrypt($str, $secret_key)
    {
        $decrypted = openssl_decrypt(hex2bin($str), 'AES-128-ECB', $secret_key, OPENSSL_RAW_DATA);
        return $decrypted;
    }
}
