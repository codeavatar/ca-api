<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\lib\encrypt;

/**
 * Class McryptLib 加密工具类 (PHP 7.0- 以下可用)
 * @package toolkit\lib\encrypt
 */
class McryptLib extends IEncrypt
{

    /**
     * AES加密
     * @param $str 明文
     * @param $secret_key 密钥（16位）
     * @return string
     */
    protected function aes_encrypt($str, $secret_key)
    {
        // TODO: Implement aes_encrypt() method.
        //AES, 128 模式加密数据 CBC
        $secret_key = base64_decode($secret_key);
        $str = trim($str);
        $str = addPKCS7Padding($str);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), 1);
        $encrypt_str = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $secret_key, $str, MCRYPT_MODE_CBC);
        return base64_encode($encrypt_str);
    }

    /**
     * AES解密
     * @param $str 密文
     * @param $secret_key 密钥（16位）
     * @return string
     */
    protected function aes_decrypt($str, $secret_key)
    {
        // TODO: Implement aes_decrypt() method.
        //AES, 128 模式加密数据 CBC
        $str = base64_decode($str);
        $secret_key = base64_decode($secret_key);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), 1);
        $encrypt_str = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $secret_key, $str, MCRYPT_MODE_CBC);
        $encrypt_str = trim($encrypt_str);

        $encrypt_str = stripPKSC7Padding($encrypt_str);
        return $encrypt_str;
    }

    /**
     * 填充算法
     * @param string $source
     * @return string
     */
    private function addPKCS7Padding($source)
    {
        $source = trim($source);
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $pad = $block - (strlen($source) % $block);
        if ($pad <= $block) {
            $char = chr($pad);
            $source .= str_repeat($char, $pad);
        }
        return $source;
    }

    /**
     * 移去填充算法
     * @param string $source
     * @return string
     */
    private function stripPKSC7Padding($source)
    {
        $source = trim($source);
        $char = substr($source, -1);
        $num = ord($char);
        if ($num == 62) return $source;
        $source = substr($source, 0, -$num);
        return $source;
    }
}
