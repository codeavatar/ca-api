<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;

class CaXMLTool
{

    /**
     * 数组转换XML
     * @param $array
     * @return string
     */
    function formatArrayToXML($array){
        $xml = "<?xml version='1.0' encoding='UTF-8'?>";
        $xml .="<xml>";
        if(is_array($array)){
            foreach($array as $key => $value){
                $xml .= "<$key><![CDATA[".$value."]]></$key>";
            }
        }
        $xml .="</xml>";
        return $xml;
    }

    /**
     * XML转换数组
     * @param $xmlstr
     * @return mixed
     */
    function formatXmlToArray($xmlstr){
        //提取目标数据，过滤响应头部信息
        $xmlstr = substr($xmlstr, strpos($xmlstr,'<xml>'));
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $xml = simplexml_load_string($xmlstr, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlArray = json_decode(json_encode($xml),true);
        return $xmlArray;
    }

}