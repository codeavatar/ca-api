<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;


class CaAmountTool
{
    //清除末尾0
    public function clearEndZero($amount)
    {
        $amount = trim(strval($amount));
        if (preg_match('#^-?\d+?\.0+$#', $amount)) {
            return preg_replace('#^(-?\d+?)\.0+$#','$1',$amount);
        }
        if (preg_match('#^-?\d+?\.[0-9]+?0+$#', $amount)) {
            return preg_replace('#^(-?\d+\.[0-9]+?)0+$#','$1',$amount);
        }
        return $amount;
    }
}