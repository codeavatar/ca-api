<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;

use think\helper\Str;

class CaStringTool extends Str
{
    /**
     * 获取安全文本（尾部省略）
     * @param $text
     * @param $percent
     * @param string $symbol
     * @return string
     */
    public function getLastSafeText($text, $percent, $symbol = '.')
    {
        $length = intval(mb_strlen($text) * $percent);
        if($length>0){
            return mb_substr($text, 0, -$length) . $symbol;
        }
        return $text.$symbol;
    }

    /**
     * 获取安全文本（中部省略）00
     * @param $text
     * @param $startIndex
     * @param $length
     * @param string $symbol
     * @return mixed
     */
    public function getSafeText($text, $startIndex, $length, $symbol = '*')
    {
        if ($startIndex == 0 && $length == 0) {
            if (mb_strlen($text) > 2) {
                //strlen(), substr()， str_split()该方法不支持汉字（GBK为2字节，UTF-8为3字节）
                $txts = $this->splitChinese($text);
                return implode('', array($txts[0], '**', $txts[count($txts) - 1]));
            } else if (mb_strlen($text) == 2) {
                $txts = $this->splitChinese($text);
                return implode('', array($txts[0], '**', $txts[1]));
            } else if (mb_strlen($text) < 2) {
                return $symbol . $symbol;
            }
        } else if (mb_strlen($text) > ($startIndex + $length + 1)) {
            $searchStr = mb_substr($text, $startIndex, $length);
            $replaceStr = '';
            for ($i = 0; $i < mb_strlen($searchStr); $i++) {
                $replaceStr .= $symbol;
            }
            return str_replace($searchStr, $replaceStr, $text);
        }
        return $text;
    }

    /**
     * 检测字符串是否为空
     * @param $value
     * @return bool
     */
    public function checkEmpty($value) {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }

    /**
     * 字符串截取
     * @param $str
     * @param int $start
     * @param int $length
     * @param string $charset 默认utf-8编码(支持utf-8，gb2312,gbk,big5四种编码)
     * @param bool $suffix
     * @return string
     */
    public function getSubstr($str, $start = 0, $length = 15, $charset = "utf-8", $suffix = true)
    {
        if (function_exists("mb_substr")) {
            $slice = mb_substr($str, $start, $length, $charset);
            if ($suffix & $slice != $str) return $slice . "…";
            return $slice;
        } elseif (function_exists('iconv_substr')) {
            return iconv_substr($str, $start, $length, $charset);
        }
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
        if ($suffix && $slice != $str) return $slice . "…";
        return $slice;
    }

    /**
     * 获取随机字符串
     *
     * @param int $len 字符串长度
     * @return bool|string 默认为32位的md5字符串
     */
    public function getRandomStr($len = 0)
    {
        if (!is_numeric($len)) {
            $len = 0; //提高容错
        }

        $rndstr = md5(microtime()) . '' . md5(round(99999999));
        if ($len != 0) {
            if ($len > 64) {
                $rndstr = '';
                $loop = ($len / 32) + 1;
                for ($i = 0; $i < $loop; $i++) {
                    $rndstr .= md5(microtime() . '' . md5(round(99999999)));
                }
            }
            return substr($rndstr, 0, $len);
        }
        return md5($rndstr);
    }

    /**
     * 读取随机数
     *
     * @param $length
     * @return int
     */
    public function getRandomNum($len = 6)
    {
        return self::random($len,1);
    }

    //唯一标识(不含前缀17位数字)
    public function getUniqueTimestamp($prefix='SN', $millisecond_lenght = 7)
    {
        list($millisecond,$second) = explode(" ",microtime());
        return ($prefix . ($second.substr($millisecond,2,$millisecond_lenght)));
    }

    //唯一标识(不含前缀19位数字)
    public function getUniqueSN($prefix='SN')
    {
        return ($prefix . date("YmdHis", time()) . (substr(explode(" ",microtime())[0],2,7)));
    }

    //唯一标识(不含前缀32位字符)
    public function getUniqueStr($prefix='SN'){
        return ($prefix.md5($this->getUniqueSN()));
    }

    //获取毫秒
    public function getMillisecond()
    {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }

    /**
     * 分割汉字成数组
     * @param $chinese
     * @return array[]|false|string[]
     */
    public function splitChinese($chinese)
    {
        return preg_split('/(?<!^)(?!$)/u', $chinese);
    }

}