<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;

use toolkit\lib\encrypt\IEncrypt;
use toolkit\lib\encrypt\McryptLib;
use toolkit\lib\encrypt\OpensslLib;

/**
 * Class CaEncrypTool 加密工具类
 * @package toolkit\tool
 */
class CaEncrypTool extends IEncrypt
{
    /**
     * 加密方法
     * @param $str 明文
     * @param $secret_key 密钥（16位）
     * @return string
     */
    public function aes_encrypt($str, $secret_key)
    {
        // TODO: Implement aes_encrypt() method.
        return $this->init()->aes_encrypt($str, $secret_key);
    }

    /**
     * 解密方法
     * @param $str 密文
     * @param $secret_key 密钥（16位）
     * @return false|string
     */
    public function aes_decrypt($str, $secret_key)
    {
        // TODO: Implement aes_decrypt() method.
        return $this->init()->aes_decrypt($str, $secret_key);
    }

    //实例对象
    private function init(): IEncrypt
    {
        $init = self::isHasMcrypt() ? new McryptLib() : new OpensslLib();
        return $init;
    }

    //检测加密函数
    private function isHasMcrypt()
    {
        return function_exists('mcrypt_encrypt');
    }
}
