<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;


class CaOtherTool
{
//    public function content_image($str){
//        if(empty($str)) return '';
/*        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/";*/
//        preg_match_all($pattern,$str,$match);
//
//        foreach($match[1] as $key=>$value){
//            $temp=explode('/static/upload',$value);
//            if(!empty($temp[1])) $str = str_replace($value,oss_link("static/upload".$temp[1]),$str);
//        }
//        return $str;
//    }

    //将秒数转换成 小时：分钟：秒的格式
    public function secondsToHis($seconds){
        if ($seconds>3600){
            $hours = intval($seconds/3600);
            $time = $hours.":".gmstrftime('%M:%S', $seconds);
        }else{
            $time = gmstrftime('%H:%M:%S', $seconds);
        }
        return $time;
    }
}