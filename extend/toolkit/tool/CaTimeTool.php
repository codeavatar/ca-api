<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;


/**
 * 测速工具（用于优化代码）
 * Class CaTimeTool
 * @package tool
 */
class CaTimeTool
{
    private $begintime;
    private $endtime;

    public function start(){
        $this->begintime = self::microtime_float();
    }

    public function stop(){
        $this->endtime = self::microtime_float();
    }

    public function outprint($prefix='--'){
        $seconds_diff = $this->endtime - $this->begintime;
        CaLogTool::writelog("【{$prefix}】运行耗时{$seconds_diff}秒");
    }

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}