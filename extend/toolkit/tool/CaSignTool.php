<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;


use constval\ConstKey;

/**
 * 核以参考支付宝支付SDK
 *
 * 推荐使用密钥生成工具（工具地址：https://doc.open.alipay.com/docs/doc.htm?treeId=291&articleId=106097&docType=1 ）
 *
 * Class CaSignTool 签名及验签工具类
 * @package app\common\utils
 */
class CaSignTool
{
    const version = '1.0.0';

    //私钥文件路径
    private $rsaPrivateKeyFilePath;

    //私钥值
    private $rsaPrivateKey;

    // 表单提交字符集编码
    private $postCharset = "UTF-8";

    private $fileCharset = "UTF-8";

    //公钥文件路径，使用文件读取文件格式，请只传递该值
    private $rsaPublicKeyFilePath;

    //公钥值，使用读取字符串格式，请只传递该值
    private $rsaPublicKey;

    //参考支付宝的SDK版本
    private $alipaySdkVersion = "3.3.0"; //alipay-sdk-php-20180705

    //md5签名密钥，推荐由数字+大小写字母+标点符号+合法特殊字符（无空格和加号），长度在200位以上的随机字符串
    private $md5SercetKey;

    /**
     * FwSignUtil constructor.
     * @param bool $useMd5 默认不使用
     * @param null $rsaPrivatekey 私钥 (若启用md5该值为md5密钥，不设置则使用配置文件中的密钥）
     * @param null $rsaPublicKey 公钥
     * @param bool $keyfromfile 从证书中读取（默认不读取），注意：私钥与公钥读取方式必须一致
     */
    function __construct($useMd5 = false, $rsaPrivatekey = null, $rsaPublicKey = null, $keyfromfile = false)
    {
        if ($useMd5) {
            $this->md5SercetKey = (($rsaPrivatekey) ? $rsaPrivatekey : config('apiconfig.ca_api')['md5_secret_key']);
            if ($this->checkEmpty($this->md5SercetKey)) {
                die('请配置MD5签名密钥！');
            }
        } else {
            if ($rsaPrivatekey && $rsaPublicKey) {
                if ($keyfromfile) {
                    $this->rsaPrivateKeyFilePath = $rsaPrivatekey;
                    $this->rsaPublicKeyFilePath = $rsaPublicKey;
                } else {
                    $this->rsaPrivateKey = $rsaPrivatekey;
                    $this->rsaPublicKey = $rsaPublicKey;
                }
            } else {
                if ($keyfromfile) {
                    $this->rsaPrivateKeyFilePath = config('apiconfig.ca_api')['rsa_private_key_file_path'];
                    $this->rsaPublicKeyFilePath = config('apiconfig.ca_api')['rsa_public_key_file_path'];
                    if ($this->checkEmpty($this->rsaPrivateKeyFilePath) || $this->checkEmpty($this->rsaPublicKeyFilePath)) {
                        die('请配置私钥和公钥的证书地址！');
                    }
                } else {
                    $this->rsaPrivateKey = config('apiconfig.ca_api')['rsa_private_key'];
                    $this->rsaPublicKey = config('apiconfig.ca_api')['rsa_public_key'];
                    if ($this->checkEmpty($this->rsaPrivateKey) || $this->checkEmpty($this->rsaPublicKey)) {
                        die('请配置私钥和公钥！');
                    }
                }
            }
        }
    }

//    +===================================================================+
//    + 对外调用区域
//    +===================================================================+

    /**
     * 生成MD5签名字符串
     * @param $params
     * @return mixed （签名时键名sign及值为null或""均需过滤）
     */
    public function md5Sign($params)
    {
        return $this->generateSignByMd5($params);
    }

    /**
     * MD5签名校验
     * @param $params
     * @param $signstr 待验签的签名字符串
     * @return bool
     */
    public function md5Check($params, $signstr='')
    {
        if(empty($signstr)) $signstr = $params[ConstKey::KEY_SIGN];
        return $this->checkSignVerify($params, $signstr);
    }

    /**
     * 生成签名字符串
     * @param $params
     * @param string $signType 签名类型  支持RSA和RSA2两种类型
     * @return string
     */
    public function rsaSign($params, $signType = "RSA")
    {
        return $this->sign($this->getSignContent($params), $signType);
    }


    /** rsaCheckV1 & rsaCheckV2
     *  验证签名
     *  在使用本方法前，必须初始化FwsignUtil且传入公钥参数。
     *  公钥是否是读取字符串还是读取文件，是根据初始化传入的值判断的。
     **/
    public function rsaCheckV1($params, $signType = 'RSA')
    {
        $sign = (false !== strpos($params[ConstKey::KEY_SIGN],'%')) ? urldecode($params[ConstKey::KEY_SIGN]) : $params[ConstKey::KEY_SIGN];
        unset($params[ConstKey::KEY_SIGN]);
        return $this->verify($this->getSignContent($params), $sign, $signType);
    }

    public function rsaCheckV2($params, $signType = 'RSA2')
    {
        $sign = (false !== strpos($params[ConstKey::KEY_SIGN],'%')) ? urldecode($params[ConstKey::KEY_SIGN]) : $params[ConstKey::KEY_SIGN];
        unset($params[ConstKey::KEY_SIGN]);
        return $this->verify($this->getSignContent($params), $sign, $signType);
    }

//    +===================================================================+
//    + RSA签名区域
//    +===================================================================+

    /**
     * 获取排序及组装后的签名前字符串
     * @param $params
     * @return string
     */
    private function getSignContent($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
//        //补充因转义的符号无法验证通过故而手动新增的过滤(推荐请求为UTF-8编码格式)
//        if(FALSE !== strpos($stringToBeSigned, '&quot;')){
//            $stringToBeSigned = str_replace('&quot;','"',$stringToBeSigned);
//        }
        return $stringToBeSigned;
    }

    /**
     * 获取排序及组装后的签名前字符串
     * （此方法对value做urlencode）
     * @param $params
     * @return string
     */
    private function getSignContentUrlencode($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . urlencode($v);
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . urlencode($v);
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 使用密钥对数据进行签名加密
     * @param $data
     * @param string $signType
     * @return string
     */
    private function sign($data, $signType = "RSA")
    {
        if ($this->checkEmpty($this->rsaPrivateKeyFilePath)) {
            $priKey = $this->rsaPrivateKey;
            $res = "-----BEGIN RSA PRIVATE KEY-----\n" .
                wordwrap($priKey, 64, "\n", true) .
                "\n-----END RSA PRIVATE KEY-----";
        } else {
            $priKey = file_get_contents($this->rsaPrivateKeyFilePath);
            $res = openssl_get_privatekey($priKey);
        }

        ($res) or die('您使用的私钥格式错误，请检查RSA私钥配置');

        if ("RSA2" == $signType) {
            openssl_sign($data, $sign, $res, OPENSSL_ALGO_SHA256);
        } else {
            openssl_sign($data, $sign, $res);
        }

        if (!$this->checkEmpty($this->rsaPrivateKeyFilePath)) {
            openssl_free_key($res);
        }
        $sign = base64_encode($sign);
        return $sign;
    }

    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    private function characet($data, $targetCharset)
    {

        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset, $fileType);
                //				$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }
        return $data;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    private function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }

    /**
     * RSA验签
     * @param $data
     * @param $sign
     * @param string $signType
     * @return bool
     */
    private function verify($data, $sign, $signType = 'RSA')
    {

        if ($this->checkEmpty($this->rsaPublicKeyFilePath)) {

            $pubKey = $this->rsaPublicKey;
            $res = "-----BEGIN PUBLIC KEY-----\n" .
                wordwrap($pubKey, 64, "\n", true) .
                "\n-----END PUBLIC KEY-----";
        } else {
            //读取公钥文件
            $pubKey = file_get_contents($this->rsaPublicKeyFilePath);
            //转换为openssl格式密钥
            $res = openssl_get_publickey($pubKey);
        }

        ($res) or die('支付宝RSA公钥错误。请检查公钥文件格式是否正确');

        //调用openssl内置方法验签，返回bool值

        $result = FALSE;
        if ("RSA2" == $signType) {
            $result = (openssl_verify($data, base64_decode($sign), $res, OPENSSL_ALGO_SHA256) === 1);
        } else {
            $result = (openssl_verify($data, base64_decode($sign), $res) === 1);
        }

        if (!$this->checkEmpty($this->rsaPublicKeyFilePath)) {
            //释放资源
            openssl_free_key($res);
        }

        return $result;
    }

//    +===================================================================+
//    + MD5签名区域
//    +===================================================================+

    /**
     * 获取签名字符串
     * @param $paramArray
     * @param $secret_key
     * @return mixed
     */
    private function generateSignByMd5($paramArray)
    {

        //除去待签名参数数组中的空值和签名参数
        $para_filter = $this->paraFilter($paramArray);

        //对待签名参数数组排序
        $para_sort = $this->argSort($para_filter);

        //生成签名结果
        $mysign = $this->buildMysign($para_sort, $this->md5SercetKey);

        return $mysign;
    }

    /**
     * 签名验签
     * @param $paramArray 需校验的参数数组
     * @param $signstr 签名字符串
     * @return bool
     */
    private function checkSignVerify($paramArray, $signstr)
    {
        $mysign = $this->generateSignByMd5($paramArray);
        if ($mysign === $signstr) {
            return true;
        }
        return false;
    }

    /**
     * 除去数组中的空值和签名参数
     * @param $para 签名参数组
     * return 去掉空值与签名参数后的新签名参数组
     */
    private function paraFilter($para)
    {
        $para_filter = array();
        foreach ($para as $key=>$val){
            if ($key == ConstKey::KEY_SIGN || $val == "" || $val == null) continue;
            else    $para_filter[$key] = $para[$key];
        }
        return $para_filter;
    }

    /**
     * 对数组排序
     * @param $para 排序前的数组
     * return 排序后的数组
     */
    private function argSort($para)
    {
        ksort($para);
        reset($para);
        return $para;
    }

    /**
     * 生成签名结果
     * @param $sort_para 要签名的数组
     * @param $key 支付宝交易安全校验码
     * @param $sign_type 签名类型 默认值：MD5
     * return 签名结果字符串
     */
    private function buildMysign($sort_para, $key)
    {
        //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        $prestr = $this->createLinkstring($sort_para);
        //把最终的字符串签名，获得签名结果
        $mysgin = $this->signByMd5($prestr, $key);
        return $mysgin;
    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param $para 需要拼接的数组
     * return 拼接完成以后的字符串
     */
    private function createLinkstring($para)
    {
        $arg = "";
        foreach ($para as $key => $val) {
            $arg .= $key . "=" . $val . "&";
        }

        //去掉最后一个&字符
        $arg = trim($arg, '&');

        //如果存在转义字符，那么去掉转义
        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
            $arg = stripslashes($arg);
        }

        return $arg;
    }

    /**
     * 签名字符串
     * @param $prestr 需要签名的字符串
     * @param $key 私钥
     * return 签名结果
     */
    private function signByMd5($prestr, $key)
    {
        $prestr = $prestr . "&key=" . $key;
        return md5($prestr);
    }
}