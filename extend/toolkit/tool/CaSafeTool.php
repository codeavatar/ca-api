<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;

use constval\ConstRedis;
use think\facade\Cache;

class CaSafeTool
{
    /**
     * 检测是否锁定本次请求
     * @param $unique 唯一标识
     * @return bool true:锁定，false:解锁
     */
    public function isLockedOfRequest($unique)
    {
        $redis = Cache::store('redis');
        $tmpKey = ConstRedis::PREFIX_LOCK . md5($unique);
        if ($redis->has($tmpKey)) {
            if ((time() - $redis->get($tmpKey)) > config('apiconfig.ca_limit')['request_locktime']) {
                return false;
            }else{
                return true;
            }
        } else {
            $redis->set($tmpKey, time(), config('apiconfig.ca_limit')['request_locktime']);
        }
        return false;
    }

    /**
     * 是否超限被锁定
     * @param $cacheKey 缓存键名
     * @param $limitTimes 限制次数
     * @param $lockedTime 锁定时间（单位秒）
     * @return mixed
     */
    public function isLockedOfAction($cacheKey,$limitTimes=0,$lockedTime=0){
        if(Cache::store('redis')->has($cacheKey)){
            if(1 === intval(Cache::store('redis')->get($cacheKey))){
                return true;
            }else{
                Cache::store('redis')->dec($cacheKey);
            }
        }else{
            if($limitTimes == 0) $limitTimes = config('apiconfig.ca_limit')['action_times'];
            if($lockedTime == 0) $lockedTime = config('apiconfig.ca_limit')['action_locktime'];
            Cache::store('redis')->set($cacheKey, $limitTimes, $lockedTime);
        }
        return false;
    }
}