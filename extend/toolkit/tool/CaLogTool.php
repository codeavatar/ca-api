<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace toolkit\tool;

use think\facade\Log;

/**
 * TP框架日志输出
 *
 * Class CaLogLib
 * @package lib
 */
class CaLogTool
{
    //tp框架查看日志（/runtime/log/yyyymm/dd.log）
    //其他在同级目录（./log/yyyymm/dd.log）
    public static function writelog($logs, $level = \think\Log::WARNING)
    {
        Log::record('==============================================================', $level);
        if (is_array($logs)) {
            foreach ($logs as $log) {
                Log::record($log, $level);
            }
        } else {
            Log::record($logs, $level);
        }
        Log::record('==============================================================' . PHP_EOL, $level);
        Log::save();
    }
}