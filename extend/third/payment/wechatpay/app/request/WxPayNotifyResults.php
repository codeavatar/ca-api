<?php
/**
 *
 * 回调回包数据基类
 *
 **/

namespace third\payment\wechatpay\app\request;


use third\payment\wechatpay\app\WxPayConfigInterface;
use third\payment\wechatpay\app\WxPayException;
use third\payment\wechatpay\app\WxPayResults;
use third\payment\wechatpay\app\WxPayConfig;
use think\Log;


class WxPayNotifyResults extends WxPayResults
{
    /**
     * 将xml转为array
     * @param WxPayConfigInterface $config
     * @param string $xml
     * @return WxPayNotifyResults
     * @throws WxPayException
     */
    public static function Init(WxPayConfigInterface $config, $xml)
    {
        if (null == $config) {
            $config = new WxPayConfig();
        }

        $obj = new self();
        $obj->FromXml($xml);
        /*记录日志缓存*/
        Log::write("微信异步通知内容：".json_encode($xml),'notice');
        //return $obj;//屏蔽签名验证
        //失败则直接返回失败
        if ($obj->CheckSign($config)) {
            return $obj;
        }
        throw new WxPayException("签名错误！");
    }

    /**
     * @param WxPayConfigInterface $config
     * @return bool|void
     */
    public static function CheckSignStr(WxPayConfigInterface $config, $xml)
    {
        $obj = new self();
        $obj->FromXml($xml);
        //失败则直接返回失败
        return $obj->CheckSign($config);
    }
}