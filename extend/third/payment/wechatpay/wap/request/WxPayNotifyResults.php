<?php
/**
 *
 * 回调回包数据基类
 *
 **/
namespace third\payment\wechatpay\wap\request;


use third\payment\wechatpay\wap\WxPayDataBase;

class WxPayNotifyResults extends WxPayDataBase
{
    /**
     * 将xml转为array
     * @param WxPayConfigInterface $config
     * @param string $xml
     * @return WxPayNotifyResults
     * @throws WxPayException
     */
    public static function Init(WxPayConfigInterface $config, $xml)
    {
        $obj = new self();
        $obj->FromXml($xml);
        //失败则直接返回失败
        $obj->CheckSign($config);
        return $obj;
    }
}