<?php
// +----------------------------------------------------------------------
// | @Author: code avatar   @Year：2021
// +----------------------------------------------------------------------
// | @Email: codeavatar@aliyun.com
// +----------------------------------------------------------------------

namespace third\payment\wechatpay;


use third\payment\wechatpay\app\request\WxPayNotifyResults;
use third\payment\wechatpay\app\request\WxPayOrderQuery;
use third\payment\wechatpay\app\WxPayConfigInterface;
use third\payment\wechatpay\app\WxPayNotify;

class WechatNotifyCallback extends WxPayNotify
{
    private $callback;

    public function __construct($callbackname)
    {
        $this->callback = $callbackname;
    }

    /**
     * 查询订单是否在第三方平台存在
     * @param $transaction_id
     * @return bool
     * @throws \payment\wechatpay\app\WxPayException
     */
    public function QueryOrder($transaction_id)
    {
        $input = new WxPayOrderQuery();
        $input->SetTransaction_id($transaction_id);
        $result = AopAppClient::orderQuery($input);
        if (array_key_exists("return_code", $result)
            && array_key_exists("result_code", $result)
            && $result["return_code"] == "SUCCESS"
            && $result["result_code"] == "SUCCESS") {
            return true;
        }
        return false;
    }

    /**
     *
     * 回包前的回调方法
     * 业务可以继承该方法，打印日志方便定位
     * @param string $xmlData 返回的xml参数
     *
     **/
    public function LogAfterProcess($xmlData)
    {
        ca_log("call back， return xml:" . $xmlData);
        return;
    }


    /**
     * 重写回调处理函数
     *
     * @param WxPayNotifyResults $data 回调解释出的参数
     * @param WxPayConfigInterface $config
     * @param string $msg 如果回调处理失败，可以将错误信息输出到该方法
     * @return true回调出来完成不需要继续回调，false回调处理未完成需要继续回调
     */
    public function NotifyProcess(WxPayNotifyResults $objData, WxPayConfigInterface $config, &$msg)
    {
        //参数说明：https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_7&index=3
        $data = $objData->GetValues();

        //TODO 1、进行参数校验
        if (!array_key_exists("return_code", $data)
            || (array_key_exists("return_code", $data) && $data['return_code'] != "SUCCESS")) {
            //TODO失败,不是支付成功的通知
            //如果有需要可以做失败时候的一些清理处理，并且做一些监控
            $msg = "异常异常";
            return false;
        }
        if (!array_key_exists("transaction_id", $data)) {
            $msg = "输入参数不正确";
            return false;
        }

        //TODO 2、进行签名验证
        try {
            $checkResult = $objData->CheckSign($config);
            if ($checkResult == false) {
                //签名错误
                $msg = '签名错误';
                return false;
            }
        } catch (Exception $e) {
            ca_log(json_encode($e));
        }

        //查询订单，判断订单真实性
        if (!$this->QueryOrder($data["transaction_id"])) {
            $msg = "订单查询失败";
            return false;
        }

        //TODO 3、处理业务逻辑
        if ($data['appid'] === $config->GetAppId() && $data['mch_id'] === $config->GetMerchantId() && $data['appid'] === $config->GetAppId()) {
            //执行逻辑
            $doAction = call_user_func($this->callback, $data);
            return $doAction;
        }
        $msg = "订单查询失败";
        return false;
    }

}