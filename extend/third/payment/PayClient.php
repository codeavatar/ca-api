<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\payment;

use third\payment\entity\PayCheckEntity;
use third\payment\entity\PayEntity;

class PayClient
{
    private $ipay;
    private $paymodal;

    /**
     * 默认微信支付
     * PayFactory constructor.
     * @param $paymodal
     */
    public function __construct($paymodal)
    {
        $this->paymodal = $paymodal;
        switch ($this->paymodal) {
            case PayType::PAY_WECHAT:
                $this->ipay = new Wechatpay();
                break;
            case PayType::PAY_ALIPAY:
                $this->ipay = new Alipay();
                break;
            default:
                $this->ipay = new Wechatpay();
                break;
        }
    }

    /**
     * 读取支付
     * @param IPay $pay
     * @param PayEntity $entity
     * @return mixed
     */
    public function getPayInfo(PayEntity $entity)
    {
        if(empty($entity->getNotifyUrl())){
            $entity->setNotifyUrl(config('apiconfig.ca_pay')[($this->paymodal === PayType::PAY_WECHAT) ? 'wxpay_notifyUrl' : 'alipay_notifyUrl']);
        }
        return $this->ipay->getPayInfo($entity);
    }

    /**
     * 校验支付
     * @param IPay $pay
     * @param PayCheckEntity $entity
     * @return mixed
     */
    public function checkSign(PayCheckEntity $entity)
    {
        return $this->ipay->checkRsa($entity);
    }
}