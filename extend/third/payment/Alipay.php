<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\payment;


use third\payment\alipay\AopClient;
use third\payment\alipay\request\AlipayTradeAppPayRequest;
use third\payment\entity\PayCheckEntity;
use third\payment\entity\PayEntity;

class Alipay implements IPay
{
    public function getPayInfo(PayEntity $entity)
    {
        $aopClient = new AopClient();
        $request = new AlipayTradeAppPayRequest();
        $bizcontent = json_encode(array('body'=>$entity->getBody(),'subject'=>$entity->getSubject(),'out_trade_no'=>$entity->getTradeNo(),'timeout_express'=>'30m','total_amount'=>$entity->getAmount(),'product_code'=>'QUICK_MSECURITY_PAY','passback_params'=>$entity->getPassbackParam()));
        $request->setNotifyUrl($entity->getNotifyUrl());
        $request->setBizContent($bizcontent);
        return $aopClient->sdkExecute($request);
    }

    public function checkRsa(PayCheckEntity $entity)
    {
        $aopClient = new AopClient();
        return $aopClient->rsaCheckV1($entity->getPostParam(),null,$entity->getSignType());
    }
}