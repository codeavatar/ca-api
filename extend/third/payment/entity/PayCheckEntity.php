<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\payment\entity;


class PayCheckEntity
{
    private $postParam;
    private $signType = 'RSA2';

    /**
     * @return mixed
     */
    public function getPostParam()
    {
        return $this->postParam;
    }

    /**
     * @param mixed $postParam
     */
    public function setPostParam($postParam)
    {
        $this->postParam = $postParam;
    }

    /**
     * @return mixed
     */
    public function getSignType()
    {
        return $this->signType;
    }

    /**
     * @param mixed $signType
     */
    public function setSignType($signType)
    {
        $this->signType = $signType;
    }


}