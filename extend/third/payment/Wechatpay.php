<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\payment;


use third\payment\entity\PayCheckEntity;
use third\payment\entity\PayEntity;
use third\payment\wechatpay\AopAppClient;
use third\payment\wechatpay\app\request\WxPayUnifiedOrder;

class Wechatpay implements IPay
{
    /**
     * 获取支付数据
     * 接口示例：https://wxpay.wxutil.com/pub_v2/app/app_pay.php
     * @param PayEntity $entity
     * @return mixed
     */
    public function getPayInfo(PayEntity $entity)
    {
        //统一下单
        $input = new WxPayUnifiedOrder();
        $input->SetOut_trade_no($entity->getTradeNo()); //$config->GetMerchantId() . date("YmdHis")
        $input->SetBody($entity->getBody());
        $input->SetTotal_fee($entity->getAmount()*100); //以分为单位1元的值100。
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetAttach($entity->getPassbackParam());
        $prepayInfo = AopAppClient::unifiedOrder($input);
        if (!empty($prepayInfo)) {
            $response = array('appid' => $prepayInfo['appid'], 'partnerid' => $prepayInfo['mch_id'], 'prepayid' => $prepayInfo['prepay_id'], 'package' => 'Sign=WXPay', 'noncestr' => AopAppClient::getNonceStr(), 'timestamp' => AopAppClient::getTimestamp());
            $response['sign'] = $input->GetSignStr($response);
            return $response;
        }
        return [];
//        return array(
//            'appid' => 'wxb4ba3c02aa476ea1',
//            'partnerid' => '1900006771',
//            'package' => 'Sign=WXPay',
//            'noncestr' => 'a940170ad69498891b59e45a63c1afc0',
//            'timestamp' => 1572663531,
//            'prepayid' => 'wx021058510813489efc8139891058805732',
//            'sign' => '13CCAC3368D63677CBAFDF98562E3DC3'
//        );
    }

    /**
     * 校验签名
     * @param CheckEntity $entity
     * @return mixed
     */
    public function checkRsa(PayCheckEntity $entity)
    {

    }
}