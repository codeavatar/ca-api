<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\payment;

use third\payment\entity\PayCheckEntity;
use third\payment\entity\PayEntity;

interface IPay
{
    /**
     * 获取支付数据
     * @param PayEntity $entity
     * @return mixed
     */
    public function getPayInfo(PayEntity $entity);

    /**
     * 校验签名
     * @param PayCheckEntity $entity
     * @return mixed
     */
    public function checkRsa(PayCheckEntity $entity);
}