<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\oss;

interface IOss
{
    //上传文件
    public function putFile($bucket, $filename, $filePath): OssEntity;

    //删除文件
    public function deleteFile($bucket,$filename): OssEntity;

    //读取文件
    public function getFile($bucket,$filename): OssEntity;
}