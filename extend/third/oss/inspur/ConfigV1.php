<?php
// +----------------------------------------------------------------------
// | @Author: code avatar   @Year：2021
// +----------------------------------------------------------------------
// | @Email: codeavatar@aliyun.com
// +----------------------------------------------------------------------
namespace third\oss\inspur;


class ConfigV1
{
    private $sdk_ver = '1.0.8';
    //空间域名后缀,请查看控制台上空间域名再配置此处
    //https://docs.ucloud.cn/storage_cdn/ufile/tools/introduction
    private $ucloud_proxy_suffix = '.infile.inspurcloud.cn';
    private $ucloud_public_key = 'TOKEN_f98f2779-627a-4a93-9147-4e383cc62ea5';
    private $ucloud_private_key = '372ad062-453a-43ae-884f-151cb740743e';

    //自定义变量
    private $ucloud_bucket = 'xiaoyingyoupin'; //xiaoyingyoupin2.infile.inspurcloud.cn
    private $ucloud_bucket_public = false; //当前存储空间，true：公开空间；false：私有空间
    private $ucloud_bucket_http = 'http'; //设置当前网址协议https或http


    /**
     * @return string
     */
    public function getSdkVer()
    {
        return $this->sdk_ver;
    }

    /**
     * @return string
     */
    public function getUcloudProxySuffix()
    {
        return $this->ucloud_proxy_suffix;
    }

    /**
     * @return string
     */
    public function getUcloudPublicKey()
    {
        return $this->ucloud_public_key;
    }

    /**
     * @return string
     */
    public function getUcloudPrivateKey()
    {
        return $this->ucloud_private_key;
    }

    /**
     * @return string
     */
    public function getUcloudBucket()
    {
        return $this->ucloud_bucket;
    }

    /**
     * @return bool
     */
    public function isUcloudBucketPublic()
    {
        return $this->ucloud_bucket_public;
    }

    /**
     * @return string
     */
    public function getUcloudBucketHttp()
    {
        return $this->ucloud_bucket_http;
    }



}