<?php
// +----------------------------------------------------------------------
// | @Author: code avatar   @Year：2021
// +----------------------------------------------------------------------
// | @Email: codeavatar@aliyun.com
// +----------------------------------------------------------------------
namespace third\oss\inspur;

use third\oss\inspur\v1\ProxyV1;
use third\oss\IOss;
use third\oss\OssEntity;

class OssInspur implements IOss
{
    private $proxyV1;
    private $configV1;

    public function __construct()
    {
        $this->proxyV1 = new ProxyV1();
        $this->configV1 = new ConfigV1();
    }

    /**
     * 兼容上传文件
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @return mixed
     */
    public function uploadFile($bucket, $filename, $filePath)
    {
        $filename = str_replace('\\', '/', $filename);
        $bucket = $this->getBucket($bucket);
        //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
        list($data, $err) = $this->proxyV1->UCloud_PutFile($bucket, $filename, $filePath);
        return $this->getResponse($data, $err);
    }

    /**
     * 上传文件
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @return mixed
     */
    public function putFile($bucket, $filename, $filePath): OssEntity
    {
        $filename = str_replace('\\', '/', $filename);
        $bucket = $this->getBucket($bucket);
        //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
        list($data, $err) = $this->proxyV1->UCloud_PutFile($bucket, $filename, $filePath);
        return $this->getResponse($data, $err);
    }

    /**
     * 上传附加文件
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @param int $position //当前append 的文件已有的大小, 新建填0
     * @return mixed
     */
    public function appendFile($bucket, $filename, $filePath, $position = 0)
    {
        $filename = str_replace('\\', '/', $filename);
        $bucket = $this->getBucket($bucket);
        //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
        list($data, $err) = $this->proxyV1->UCloud_AppendFile($bucket, $filename, $filePath, $position);
        return $this->getResponse($data, $err);

    }

    /**
     * 删除文件
     * @param $bucket
     * @param $filename
     * @return mixed
     */
    public function deleteFile($bucket, $filename): OssEntity
    {
        $bucket = $this->getBucket($bucket);
        //待实现，并调试
    }

    /**
     * 读取文件
     * @param $bucket
     * @param $filename
     * @return mixed
     */
    public function getFile($bucket, $filename): OssEntity
    {
        $bucket = $this->getBucket($bucket);
        $httpurl = '';
        if ($this->configV1->isUcloudBucketPublic()) {
            //公开空间
            $httpurl = $this->proxyV1->UCloud_MakePublicUrl($bucket, $filename);
        } else {
            //私有空间
            $httpurl = $this->proxyV1->UCloud_MakePrivateUrl($bucket, $filename);
        }

        if (false === stripos($httpurl, $this->configV1->getUcloudBucketHttp())) {
            $httpurl = $this->configV1->getUcloudBucketHttp() . '://' . $httpurl;
        }

        return $this->getResponse(array('url' => $httpurl), null);
    }

    /**
     *
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @return mixed
     */
    public function multipartForm($bucket, $filename, $filePath)
    {
        $bucket = $this->getBucket($bucket);
        //待实现，并调试
    }

    /**
     * 分片上传大文件
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @param $uploadId
     * @param $blkSize
     * @return mixed
     */
    public function mUploadFile($bucket, $filename, $filePath, $uploadId, $blkSize)
    {
        $bucket = $this->getBucket($bucket);
        //待实现，并调试
    }

    /**
     * 秒传，非上传
     * @param $bucket
     * @param $filename
     * @param $filePath
     * @return mixed
     */
    public function uploadHit($bucket, $filename, $filePath)
    {
        $bucket = $this->getBucket($bucket);
        //待实现，并调试
    }

    /**
     * 默认存储空间名
     * @param $bucket
     * @return string
     */
    private function getBucket($bucket)
    {
        if (empty($bucket)) {
            $bucket = $this->configV1->getUcloudBucket();
        }
        return $bucket;
    }

    /**
     * 统一响应数据
     * @param $data
     * @param $err
     * @return OssEntity
     */
    private function getResponse($data, $err)
    {

//        //可用于调试
//        if ($err) {
//            echo "error: " . $err->ErrMsg . "\n";
//            echo "code: " . $err->Code . "\n";
//            exit;
//        }
//        echo "ETag: " . $data['ETag'] . "\n";
        if ($err) {
            return new OssEntity(false, "code: " . $err->Code . '\n error:' . $err->ErrMsg . '\n', null);
        }
        return new OssEntity(true, '', $data);
    }
}