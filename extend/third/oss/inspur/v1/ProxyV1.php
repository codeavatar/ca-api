<?php

namespace third\oss\inspur\v1;

use third\oss\inspur\ConfigV1;

class ProxyV1
{
    private $configV1;
    private $utilsV1;
    private $httpV1;

    public function __construct()
    {
        $this->configV1 = new ConfigV1();
        $this->utilsV1 = new UtilsV1();
        $this->httpV1 = new HttpV1();
    }

//------------------------------普通上传------------------------------
    public function UCloud_PutFile($bucket, $key, $file)
    {
        $action_type = ActionType::PUTFILE;
        $err = $this->utilsV1->CheckConfig(ActionType::PUTFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UCloud_Error(-1, -1, "open $file error"));

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = $key;
        $content = @fread($f, filesize($file));
        list($mimetype, $err) = $this->utilsV1->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }
        $req = new HTTP_Request('PUT', array('host' => $host, 'path' => $path), $content, $bucket, $key, $action_type);
        $req->Header['Expect'] = '';
        $req->Header['Content-Type'] = $mimetype;

        $client = new UCloud_AuthHttpClient(null, $mimetype);
        list($data, $err) = $this->httpV1->UCloud_Client_Call($client, $req);
        fclose($f);
        return array($data, $err);
    }

//------------------------------表单上传------------------------------
    public function UCloud_MultipartForm($bucket, $key, $file)
    {
        $action_type = ActionType::POSTFILE;
        $err = $this->utilsV1->CheckConfig(ActionType::POSTFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UCloud_Error(-1, -1, "open $file error"));

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = "";
        $fsize = filesize($file);
        $content = "";
        if ($fsize != 0) {
            $content = @fread($f, filesize($file));
            if ($content == FALSE) {
                fclose($f);
                return array(null, new UCloud_Error(0, -1, "read file error"));
            }
        }
        list($mimetype, $err) = $this->utilsV1->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }

        $req = new HTTP_Request('POST', array('host' => $host, 'path' => $path), $content, $bucket, $key, $action_type);
        $req->Header['Expect'] = '';
        $token = UCloud_SignRequest(null, $req, $mimetype);

        $fields = array('Authorization' => $token, 'FileName' => $key);
        $files = array('files' => array('file', $file, $content, $mimetype));

        $client = new UCloud_AuthHttpClient(null, ConstV1::NO_AUTH_CHECK);
        list($data, $err) = UCloud_Client_CallWithMultipartForm($client, $req, $fields, $files);
        fclose($f);
        return array($data, $err);
    }

//------------------------------分片上传------------------------------
    public function UCloud_MInit($bucket, $key)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::MINIT);
        if ($err != null) {
            return array(null, $err);
        }

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = $key;
        $querys = array(
            "uploads" => ""
        );
        $req = new HTTP_Request('POST', array('host' => $host, 'path' => $path, 'query' => $querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new UCloud_AuthHttpClient(null);
        return $this->httpV1->UCloud_Client_Call($client, $req);
    }

//@results: (tagList, err)
    public function UCloud_MUpload($bucket, $key, $file, $uploadId, $blkSize, $partNumber = 0)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::MUPLOAD);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UCloud_Error(-1, -1, "open $file error"));

        $etagList = array();
        list($mimetype, $err) = $this->utilsV1->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }
        $client = new UCloud_AuthHttpClient(null);
        for (; ;) {
            $host = $bucket . $this->configV1->getUcloudProxySuffix();
            $path = $key;
            if (@fseek($f, $blkSize * $partNumber, SEEK_SET) < 0) {
                fclose($f);
                return array(null, new UCloud_Error(0, -1, "fseek error"));
            }
            $content = @fread($f, $blkSize);
            if ($content == FALSE) {
                if (feof($f)) break;
                fclose($f);
                return array(null, new UCloud_Error(0, -1, "read file error"));
            }

            $querys = array(
                "uploadId" => $uploadId,
                "partNumber" => $partNumber
            );
            $req = new HTTP_Request('PUT', array('host' => $host, 'path' => $path, 'query' => $querys), $content, $bucket, $key);
            $req->Header['Content-Type'] = $mimetype;
            $req->Header['Expect'] = '';
            list($data, $err) = $this->httpV1->UCloud_Client_Call($client, $req);
            if ($err) {
                fclose($f);
                return array(null, $err);
            }
            $etag = @$data['ETag'];
            $part = @$data['PartNumber'];
            if ($part != $partNumber) {
                fclose($f);
                return array(null, new UCloud_Error(0, -1, "unmatch partnumber"));
            }
            $etagList[] = $etag;
            $partNumber += 1;
        }
        fclose($f);
        return array($etagList, null);
    }

    public function UCloud_MFinish($bucket, $key, $uploadId, $etagList, $newKey = '')
    {

        $err = $this->utilsV1->CheckConfig(ActionType::MFINISH);
        if ($err != null) {
            return array(null, $err);
        }

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = $key;
        $querys = array(
            'uploadId' => $uploadId,
            'newKey' => $newKey,
        );

        $body = @implode(',', $etagList);
        $req = new HTTP_Request('POST', array('host' => $host, 'path' => $path, 'query' => $querys), $body, $bucket, $key);
        $req->Header['Content-Type'] = 'text/plain';

        $client = new UCloud_AuthHttpClient(null);
        return $this->httpV1->UCloud_Client_Call($client, $req);
    }

    public function UCloud_MCancel($bucket, $key, $uploadId)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::MCANCEL);
        if ($err != null) {
            return array(null, $err);
        }

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = $key;
        $querys = array(
            'uploadId' => $uploadId
        );

        $req = new HTTP_Request('DELETE', array('host' => $host, 'path' => $path, 'query' => $querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new UCloud_AuthHttpClient(null);
        return $this->httpV1->UCloud_Client_Call($client, $req);
    }

//------------------------------秒传------------------------------
    public function UCloud_UploadHit($bucket, $key, $file)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::UPLOADHIT);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UCloud_Error(-1, -1, "open $file error"));

        $content = "";
        $fileSize = filesize($file);
        if ($fileSize != 0) {
            $content = @fread($f, $fileSize);
            if ($content == FALSE) {
                fclose($f);
                return array(null, new UCloud_Error(0, -1, "read file error"));
            }
        }
        list($fileHash, $err) = $this->utilsV1->UCloud_FileHash($file);
        if ($err) {
            fclose($f);
            return array(null, $err);
        }
        fclose($f);

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = "uploadhit";
        $querys = array(
            'Hash' => $fileHash,
            'FileName' => $key,
            'FileSize' => $fileSize
        );

        $req = new HTTP_Request('POST', array('host' => $host, 'path' => $path, 'query' => $querys), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new UCloud_AuthHttpClient(null);
        return $this->httpV1->UCloud_Client_Call($client, $req);
    }

//------------------------------删除文件------------------------------
    public function UCloud_Delete($bucket, $key)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::DELETE);
        if ($err != null) {
            return array(null, $err);
        }

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
        $path = "$key";

        $req = new HTTP_Request('DELETE', array('host' => $host, 'path' => $path), null, $bucket, $key);
        $req->Header['Content-Type'] = 'application/x-www-form-urlencoded';

        $client = new UCloud_AuthHttpClient(null);
        return $this->httpV1->UCloud_Client_Call($client, $req);
    }

//------------------------------追加上传------------------------------
    //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
    public function UCloud_AppendFile($bucket, $key, $file, $position)
    {
        $action_type = ActionType::APPENDFILE;
        $err = $this->utilsV1->CheckConfig(ActionType::APPENDFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $f = @fopen($file, "r");
        if (!$f) return array(null, new UCloud_Error(-1, -1, "open $file error"));

        $host = $bucket . $this->configV1->getUcloudProxySuffix();
//        $key = $key . "?append&position=" . $position; //源始文件运行该代码，出现权限错误，故在此注释了。
        $path = $key;
        $content = @fread($f, filesize($file));
        list($mimetype, $err) = $this->utilsV1->GetFileMimeType($file);
        if ($err) {
            fclose($f);
            return array("", $err);
        }
        $req = new HTTP_Request('PUT', array('host' => $host, 'path' => $path), $content, $bucket, $key, $action_type);
        $req->Header['Expect'] = '';
        $req->Header['Content-Type'] = $mimetype;

        $client = new UCloud_AuthHttpClient(null, $mimetype);
        list($data, $err) = $this->httpV1->UCloud_Client_Call($client, $req);
        fclose($f);
        return array($data, $err);
    }


//------------------------------生成公有文件Url------------------------------
// @results: $url
    public function UCloud_MakePublicUrl($bucket, $key)
    {
        return $bucket . $this->configV1->getUcloudProxySuffix() . "/" . rawurlencode($key);
    }
//------------------------------生成私有文件Url------------------------------
// @results: $url
    public function UCloud_MakePrivateUrl($bucket, $key, $expires = 0)
    {

        $err = $this->utilsV1->CheckConfig(ActionType::GETFILE);
        if ($err != null) {
            return array(null, $err);
        }

        $public_url = $this->UCloud_MakePublicUrl($bucket, $key);
        $req = new HTTP_Request('GET', array('path' => $public_url), null, $bucket, $key);
        if ($expires > 0) {
            $req->Header['Expires'] = $expires;
        }

        $client = new UCloud_AuthHttpClient(null);
        $temp = $client->Auth->SignRequest($req, null,  ConstV1::QUERY_STRING_CHECK);
        $signature = substr($temp, -28, 28);
        $url = $public_url . "?UCloudPublicKey=" . rawurlencode($this->configV1->getUcloudPublicKey()) . "&Signature=" . rawurlencode($signature);
        if ('' != $expires) {
            $url .= "&Expires=" . rawurlencode($expires);
        }
        return $url;
    }

}