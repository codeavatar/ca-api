<?php
namespace third\oss\inspur\v1;

class UCloud_AuthHttpClient
{
    private $httpV1;
    private $digestV1;

    public $Auth;
    public $Type;
    public $MimeType;

    public function __construct($auth, $mimetype = null, $type = ConstV1::HEAD_FIELD_CHECK)
    {
        $this->httpV1 = new HttpV1();
        $this->digestV1 = new DigestV1();
        $this->Type = $type;
        $this->MimeType = $mimetype;
        $this->Auth = $this->digestV1->UCloud_MakeAuth($auth, $type);
    }

    //@results: ($resp, $error)
    public function RoundTrip(HTTP_Request $req)
    {
        if ($this->Type === ConstV1::HEAD_FIELD_CHECK) {
            $token = $this->Auth->SignRequest($req, $this->MimeType, $this->Type);
            $req->Header['Authorization'] = $token;
        }
        return $this->httpV1->UCloud_Client_Do($req);
    }
}