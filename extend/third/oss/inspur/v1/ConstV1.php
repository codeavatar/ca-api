<?php

namespace third\oss\inspur\v1;

class ConstV1
{
    const NO_AUTH_CHECK = 0;
    const HEAD_FIELD_CHECK = 1;
    const QUERY_STRING_CHECK = 2;

}