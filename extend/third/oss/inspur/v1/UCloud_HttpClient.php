<?php
namespace third\oss\inspur\v1;

class UCloud_HttpClient
{
    private $httpV1;

    public function __construct()
    {
        $this->httpV1 = new HttpV1();
    }

    //@results: ($resp, $error)
    public function RoundTrip($req)
    {
        return $this->httpV1->UCloud_Client_Do($req);
    }
}