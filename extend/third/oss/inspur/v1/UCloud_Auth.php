<?php
namespace third\oss\inspur\v1;

class UCloud_Auth
{
    private $httpV1;
    private $digestV1;
    public $PublicKey;
    public $PrivateKey;

    public function __construct($publicKey, $privateKey)
    {
        $this->httpV1 = new HttpV1();
        $this->digestV1 = new DigestV1();
        $this->PublicKey = $publicKey;
        $this->PrivateKey = $privateKey;
    }

    public function Sign($data)
    {
        $sign = base64_encode(hash_hmac('sha1', $data, $this->PrivateKey, true));
        return "UCloud " . $this->PublicKey . ":" . $sign;
    }

    //@results: $token
    public function SignRequest($req, $mimetype = null, $type = ConstV1::HEAD_FIELD_CHECK)
    {
        $url = $req->URL;
        $url = parse_url($url['path']);
        $data = '';
        $data .= strtoupper($req->METHOD) . "\n";
        $data .= $this->httpV1->UCloud_Header_Get($req->Header, 'Content-MD5') . "\n";
        if ($mimetype)
            $data .=  $mimetype . "\n";
        else
            $data .= $this->httpV1->UCloud_Header_Get($req->Header, 'Content-Type') . "\n";
        if ($type === ConstV1::HEAD_FIELD_CHECK)
            $data .= $this->httpV1->UCloud_Header_Get($req->Header, 'Date') . "\n";
        else
            $data .= $this->httpV1->UCloud_Header_Get($req->Header, 'Expires') . "\n";
        $data .= $this->digestV1->CanonicalizedUCloudHeaders($req->Header);
        $data .= $this->digestV1->CanonicalizedResource($req->Bucket, $req->Key);
        return $this->Sign($data);
    }
}