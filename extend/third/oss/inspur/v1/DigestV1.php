<?php

namespace third\oss\inspur\v1;

use third\oss\inspur\ConfigV1;

class DigestV1
{

    function CanonicalizedResource($bucket, $key)
    {
        return "/" . $bucket . "/" . $key;
    }

    function CanonicalizedUCloudHeaders($headers)
    {

        $keys = array();
        foreach ($headers as $header) {
            $header = trim($header);
            $arr = explode(':', $header);
            if (count($arr) < 2) continue;
            list($k, $v) = $arr;
            $k = strtolower($k);
            if (strncasecmp($k, "x-ucloud") === 0) {
                $keys[] = $k;
            }
        }

        $c = '';
        sort($keys, SORT_STRING);
        foreach ($keys as $k) {
            $c .= $k . ":" . trim($headers[$v], " ") . "\n";
        }
        return $c;
    }

    function UCloud_MakeAuth($auth)
    {
        if (isset($auth)) {
            return $auth;
        }

        $configV1 = new ConfigV1();
        return new UCloud_Auth($configV1->getUcloudPublicKey(), $configV1->getUcloudPrivateKey());
    }

//@results: token
    function UCloud_SignRequest($auth, $req, $type = ConstV1::HEAD_FIELD_CHECK)
    {
        return UCloud_MakeAuth($auth)->SignRequest($req, $type);
    }
}
