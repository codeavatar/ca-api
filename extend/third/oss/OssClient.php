<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\oss;

use third\oss\inspur\OssInspur;

class OssClient
{
    private $ioss;

    public function putFile($filePath, $filename): OssEntity
    {
        return self::init()->putFile(null, $filename, $filePath);
    }

    public function deleteFile($filename): OssEntity{
        return self::init()->deleteFile(null, $filename);
    }

    public function getFile($filename): OssEntity
    {
        return self::init()->getFile(null, $filename);
    }

    //实例对象
    private function init(): IOss
    {
        if (isset($this->ioss)) {
            return $this->ioss;
        }

        $switch = config('apiconfig.ca_upload')['use_upload'];
        switch ($switch) {
            case 'oss_aliyun':
                $this->ioss = new OssInspur();
                break;
            case 'oss_baidu':
                $this->ioss = new OssInspur();
                break;
            case 'oss_tencent':
                $this->ioss = new OssInspur();
                break;
            case 'oss_inspur':
                $this->ioss = new OssInspur();
                break;
            default:
                $this->ioss = new OssInspur();
                break;
        }
        return $this->ioss;
    }
}