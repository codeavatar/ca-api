<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\map;


class MapClient
{
    private $imap = null;

    /**
     * 获取定位区域信息
     * @param $longitude 经度
     * @param $latitude 纬度
     * @return entity\MapEntity
     * @throws \Exception
     */
    public function getLocation($longitude, $latitude): MapEntity
    {
        return self::init()->location($longitude, $latitude);
    }

    /**
     * 获取某地址的座标信息
     * @param $address
     * @return entity\MapEntity
     */
    public function getCoordinate($address): MapEntity
    {
        return self::init()->coordinate($address);
    }

    //实例对象
    private function init(): IMap
    {
        if (isset($this->imap)) {
            return $this->imap;
        }

        $switch = config('apiconfig.ca_map')['use_map'];
        switch ($switch) {
            case 'gaode':
                $this->imap = new GaodeMap();
                break;
            case 'baidu':
                $this->imap = new BaiduMap();
                break;
            default:
                $this->imap = new GaodeMap();
                break;
        }
        return $this->imap;
    }
}