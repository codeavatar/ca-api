<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\map;


interface IMap
{
    public function location($longitude,$latitude): MapEntity;
    public function coordinate($address): MapEntity;
}