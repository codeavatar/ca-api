<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\map;


class BaiduMap implements IMap
{
    //访问Key
    private $accessKey;

    public function __construct()
    {
        $this->accessKey = config('apiconfig.ca_map')['baidu_accesskey'];
    }

    public function location($longitude, $latitude): MapEntity
    {
        // TODO: Implement location() method.
    }

    public function coordinate($address): MapEntity
    {
        // TODO: Implement coordinate() method.
    }
}