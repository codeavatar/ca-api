<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\map;

use toolkit\entity\CaCurlEntity;
use toolkit\tool\CaCurlTool;

class GaodeMap implements IMap
{
    //访问Key
    private $accessKey;

    public function __construct()
    {
        $this->accessKey = config('apiconfig.ca_map')['gaode_accesskey'];
    }


    public function location($longitude, $latitude): MapEntity
    {
        // TODO: Implement location() method.
        $api = 'https://restapi.amap.com/v3/geocode/regeo';
        $curl = new CaCurlTool();
        //返回LibCurlModel的数据模型
        $responseData = $curl->curl($api, array('key' => $this->accessKey, 'location' => $longitude . ',' . $latitude), 'GET');
//        $responseData  = new CaCurlEntity(); //用于提示
        $info = json_decode($responseData->getBody());
        $mapEntity = new MapEntity(); //封装数据
        if ('10000' === $info->infocode) {
            $mapEntity->setCode('200');
            $mapEntity->setCodeInfo($info->info);
            $tmpAddressComponent = $info->regeocode->addressComponent;
            $mapEntity->setProvince($tmpAddressComponent->province);
            $mapEntity->setCity($tmpAddressComponent->city);
            $mapEntity->setCityCode($tmpAddressComponent->citycode);
            $mapEntity->setDistrict($tmpAddressComponent->district);
            $mapEntity->setDistrictCode($tmpAddressComponent->adcode);
            $mapEntity->setType(MapType::MAP_GAODE);
            $mapEntity->setRawData(json_encode($info->regeocode));
        } else {
            $mapEntity->setCode('-1');
            $mapEntity->setCodeInfo('[' . $info->infocode . ']' . $info->info);
        }
        return $mapEntity;
    }

    public function coordinate($address): MapEntity
    {
        // TODO: Implement coordinate() method.
        $api = 'https://restapi.amap.com/v3/geocode/geo';
        $curl = new CaCurlTool();
        $responseData = $curl->curl($api, array('key' => $this->accessKey, 'address' => $address), 'GET');
        $info = json_decode($responseData);
        $mapEntity = new MapEntity(); //封装数据
        if ('10000' === $info->infocode) {
            $mapEntity->setCode('200');
            $mapEntity->setCodeInfo($info->info);
            $tmpGeocodeItem = $info->geocodes[0];
            $mapEntity->setProvince($tmpGeocodeItem->province);
            $mapEntity->setCity($tmpGeocodeItem->city);
            $mapEntity->setCityCode($tmpGeocodeItem->citycode);
            $mapEntity->setDistrict($tmpGeocodeItem->district);
            $mapEntity->setDistrictCode($tmpGeocodeItem->adcode);
            $mapEntity->setType(MapType::MAP_GAODE);
            $mapEntity->setLocationCoordinate($tmpGeocodeItem->location);
            $mapEntity->setRawData(json_encode($info->geocodes[0]));
        } else {
            $mapEntity->setCode('-1');
            $mapEntity->setCodeInfo('[' . $info->infocode . ']' . $info->info);
        }
        return $mapEntity;
    }
}