<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\sms;


use constval\ConstRedis;
use think\facade\Cache;
use toolkit\tool\CaStringTool;

class SmsClient
{
    private $isms = null;

    public function send($tophone, $msg = null): SmsEntity
    {
        if (empty($msg)) $msg = self::template(self::getCode($tophone));
        return self::init()->send($tophone, $msg);
    }

    /**
     * 短信模版
     * @param $validcode
     * @param $expiretime 有效时间（单位分钟）
     * @return string
     */
    public function template($validcode, $expiretime = 0): string
    {
        if ($expiretime <= 0) $expiretime = config('apiconfig.ca_sms')['validcode_expiretime'];
        return self::init()->template($validcode, ($expiretime/60));
    }

    /**
     * 校验
     *
     * @param $tophone
     * @param $code
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function checkCode($tophone, $code): bool
    {
        if(strval($code) === strval(self::getCode($tophone)) || $code === config('apiconfig.ca_sms')['bypass_verify']){
            $tmpkey = ConstRedis::PREFIX_SMS . $tophone;
            Cache::store('redis')->delete($tmpkey);
            return true;
        }
        return false;
    }

    /**
     * @param $tophone
     * @param int $codelen 验证码长度（默认配置）
     * @param int $expiretime 有效时间（单位秒）
     * @return int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCode($tophone, $codelen = 0, $expiretime = 0): int
    {
        $tmpkey = ConstRedis::PREFIX_SMS . $tophone;
        $code = Cache::store('redis')->get($tmpkey);
        if (empty($code)) {
            if ($codelen <= 0) $codelen = config('apiconfig.ca_sms')['validcode_length'];
            if ($expiretime <= 0) $expiretime = config('apiconfig.ca_sms')['validcode_expiretime'];
            $tool = new CaStringTool();
            $code = $tool->getRandomNum($codelen);
            Cache::store('redis')->set($tmpkey, strval($code), $expiretime);
        }
        return $code;
    }

    //实例对象
    private function init(): ISms
    {
        if (isset($this->isms)) {
            return $this->isms;
        }

        $switch = config('apiconfig.ca_sms')['use_sms'];
        switch ($switch) {
            case 'cly':
                $this->isms = new ClySms();
                break;
            case 'aliyun':
                $this->isms = new AliSms();
                break;
            default:
                $this->isms = new ClySms();
                break;
        }
        return $this->isms;
    }
}