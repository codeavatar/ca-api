<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\sms;


use AlibabaCloud\Client\AlibabaCloud;
use constval\ConstCode;

class AliSms implements ISms
{

    public function send($tophone, $msg): SmsEntity
    {
        // TODO: Implement send() method.
        $aliyunInfo = config('apiconfig.ca_sms')['aliyun_info'];

        AlibabaCloud::accessKeyClient($aliyunInfo['access_key_id'], $aliyunInfo['access_key_secret'])
            ->regionId($aliyunInfo['region'])
            ->asDefaultClient();

        //统一结果
        $entity = new SmsEntity();

        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $tophone,
                        'SignName' => $aliyunInfo['sign_name'],
                        'TemplateCode' => $aliyunInfo['template_code'],
                        'TemplateParam' => json_encode(['code'=>$msg]),
                    ],
                ])
                ->request();
            //响应结果
            $sms_result = $result->toArray();
            $entity->setCode('ok' === strtolower($sms_result['Code'])?ConstCode::CODE_SUCCESS:ConstCode::CODE_FAIL);
            $entity->setMsg($sms_result['Message']."[{$sms_result['Code']}]");
        } catch (ClientException $e) {
            $entity->setCode(ConstCode::CODE_FAIL);
            $entity->setMsg($e->getErrorMessage);
        } catch (ServerException $e) {
            $entity->setCode(ConstCode::CODE_FAIL);
            $entity->setMsg($e->getErrorMessage);
        }
        return $entity;
    }

    public function template($validcode, $expiretime): string
    {
        // TODO: Implement template() method.
    }
}