<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\sms;


interface ISms
{
    public function send($tophone,$msg): SmsEntity;
    public function template($validcode, $expiretime): string;
}