<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\sms;

use constval\ConstCode;

class ClySms implements ISms
{
    private $gateway;
    private $account;
    private $password;
    private $codemsg;

    function __construct()
    {
        //诚立业SMS
        $this->gateway = 'http://sms-cly.cn/smsSend.do?';
        $this->account = config('apiconfig.ca_sms')['cly_account'];
        $this->password = md5($this->account . '' . md5(config('apiconfig.ca_sms')['cly_password']));
        $this->codemsg = array('code' => '成功', 'code-0' => '失败', 'code-1' => '用户名或者密码不正确', 'code-2' => '必填选项为空', 'code-3' => '短信内容0个字节', 'code-5' => '余额不够',
            'code-10' => '用户被禁用', 'code-11' => '短信内容超过500字', 'code-12' => '无扩展权限（ext字段需填空）', 'code-13' => 'IP校验错误', 'code-14' => '内容解析异常',
            'code-990' => '未知错误');
    }

    public function send($tophone, $msg): SmsEntity
    {
        // TODO: Implement send() method.
        if(config('apiconfig.ca_sms')['is_open']){
            //屏蔽发送短信
            return new SmsEntity(ConstCode::CODE_SUCCESS,'发送成功');
        }

        if ((strlen($tophone) != 11) || empty($msg)) {
            return new SmsEntity(ConstCode::CODE_FAIL,'手机号或短消息错误');
        }

        $param = http_build_query(
            array(
                'username' => $this->account,
                'password' => $this->password,
                'mobile' => $tophone,
                'content' => $msg
            )
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->gateway);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $result = curl_exec($ch);
        curl_close($ch);
        return new SmsEntity((($result>0)?ConstCode::CODE_SUCCESS:ConstCode::CODE_FAIL),$this->codemsg['code' . ($result > 0 ? '' : $result)]);
    }

    public function template($validcode, $expiretime): string
    {
        // TODO: Implement template() method.
        return "尊敬的用户，您的短信验证码为：{$validcode}（{$expiretime}分钟内有效）祝您生活愉快!";
    }
}