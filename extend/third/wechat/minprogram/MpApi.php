<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\wechat\minprogram;


class MpApi
{
    private $api_config=[];

    //构造函数
    public function __construct()
    {
        $this->api_config = [
            'jscode2session'=>'https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={js_code}&grant_type=authorization_code|GET'
        ];
    }
    //所有配置
    public function getConfig(){
        return $this->api_config;
    }
    //读取api地址
    public function getApiStr($api_key){
        $api = $this->api_config[$api_key];
        return self::getAppIdAndSecret($api);
    }

    //私有方法
    private function getAppIdAndSecret($api){
        $app_id_secret = config('apiconfig.ca_third')['wechat'];
        return $this->apiParamHandler($api,['appid'=>$app_id_secret['wx_mp_appid'],'secret'=>$app_id_secret['wx_mp_appsecret']]);
    }

    //api参数处理器
    private function apiParamHandler($api_url, $param)
    {
        foreach ($param as $key => $val) {
            $api_url = str_replace('{' . $key . '}', $val, $api_url);
        }
        return $api_url;
    }
}