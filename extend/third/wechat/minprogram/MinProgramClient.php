<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\wechat\minprogram;


use toolkit\tool\CaCurlTool;

class MinProgramClient
{

    //登录凭证校验
    public function getCode2Session($js_code)
    {
        $post_data = ['js_code' => $js_code];
        $http_grp = self::getHttpApi('jscode2session',$post_data);

        $curl = new CaCurlTool();
        $responseData = $curl->curl($http_grp[0], $post_data, $http_grp[1]);
        $resultArray = json_decode($responseData->getBody(), true);
        return $resultArray;
    }

    //微信数据安全验签
    public function chkDataSignature($param_signature,$param_raw_data,$session_key){
        return ($param_signature === sha1($param_raw_data.$session_key));
    }

    ////////////
    /// 私有函数
    ////////////

    //api参数处理器
    private function apiParamHandler($api_url, $param)
    {
        foreach ($param as $key => $val) {
            $api_url = str_replace('{' . $key . '}', $val, $api_url);
        }
        return $api_url;
    }

    //解析api结构
    private function getHttpApi($api_key, $param)
    {
        $mp_api = new MpApi();
        $http_api = $mp_api->getApiStr($api_key);
        $http_grp = explode('|', $http_api);
        $http_grp[0] = self::apiParamHandler($http_grp[0], $param);
        return $http_grp;
    }
}