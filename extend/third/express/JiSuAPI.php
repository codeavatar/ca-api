<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\express;

use toolkit\entity\CaCurlEntity;
use toolkit\tool\CaCurlTool;

class JiSuAPI implements IExpress
{

    //访问Key
    private $accessKey;

    public function __construct()
    {
        $this->accessKey = config('apiconfig.ca_express')['jisu_accesskey'];
    }

    public function getProcess($number): ExpressEntity
    {
        // TODO: Implement getProcess() method.
        $api = self::getApi();
        $curl = new CaCurlTool();
        $responseData = new CaCurlEntity();
        $responseData = $curl->curl($api, array('type' => 'auto', 'number' => $number), 'POST');
        $jsonData = json_decode($responseData->getBody(), true);
        $entity = new ExpressEntity();
        $entity->setCode(($jsonData['status'] == 0) ? 200 : $jsonData['status']);
        $entity->setMsg($jsonData['msg']);
        if ($entity->getCode() == 200) {
            $list = [];
            //统一存储格式，其他平台同理
            foreach ($jsonData['result']['list'] as $item) {
                $list[] = array('time' => $item['time'], 'info' =>$item['status']);
            }
            $entity->setList($list);
        }
        return $entity;
    }

    private function getApi(){
        return 'https://api.jisuapi.com/express/query?appkey=' . $this->accessKey;
    }
}