<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace third\express;

class ExpressClient
{
    private $iexpress;

    public function getProcess($number) : ExpressEntity
    {
        return self::init()->getProcess($number);
    }

    //实例对象
    private function init(): IExpress
    {
        if(isset($this->iexpress)){
           return $this->iexpress;
        }

        $switch = config('apiconfig.ca_express')['use_express'];
        switch ($switch){
            case 'jisu':
                $this->iexpress = new JiSuAPI();
                break;
            default:
                $this->iexpress = new JiSuAPI();
                break;
        }
        return $this->iexpress;
    }
}