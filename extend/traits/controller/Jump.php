<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace traits\controller;

use constval\ConstCode;
use constval\ConstKey;
use entity\CaApiEntity;
use think\facade\View;

/**
 * 用法：
 * class index
 * {
 *     use \traits\controller\Jump;
 *     public function index(){
 *         $this->error();
 *         $this->redirect();
 *     }
 * }
 */
trait Jump
{

    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    //成功
    protected function success($data = '', $msg = '访问成功', $code = ConstCode::CODE_SUCCESS)
    {
        return json(CaApiEntity::data($code, $msg, $data));
    }

    //成功
    protected function success_list($list, $pageIndex, $pageTotal=0, $pageTotalRow=0, $data = '', $msg = '访问成功', $code = ConstCode::CODE_SUCCESS)
    {
        return json(CaApiEntity::listdata($code, $msg, $list, $pageIndex, $pageTotal, $pageTotalRow, $data));
    }

    //失败
    protected function error($data = '', $msg = '访问失败', $code = ConstCode::CODE_FAIL)
    {
        return json(CaApiEntity::data($code, $msg, $data));
    }

    //失败（含令牌）
    protected function error_token($data = '', $msg = '访问失败', $code = ConstCode::CODE_FAIL)
    {
        if (empty($data)) {
            $data['token'] = token();
        } else if (is_array($data)) {
            $data = array_merge($data,['token'=>token()]);
        } else if (is_string($data)) {
            $tmpData = ['str' => $data, 'token' => token()];
            $data = $tmpData;
        }
        return json(CaApiEntity::data($code, $msg, $data));
    }

    //模版变量
    protected function assign($name, $value = null)
    {
        View::assign($name, $value);
    }

    //模版渲染
    protected function fetch($template = '', array $vars = [])
    {
        return View::fetch($template, $vars);
    }
}

