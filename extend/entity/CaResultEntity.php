<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace entity;


use constval\ConstKey;

class CaResultEntity
{
    //data结构
    public static function data($code, $msg, $data = '')
    {
        return ['code' => $code, 'msg' => $msg, 'data' => (empty($data) ? '' : $data)];
    }
}