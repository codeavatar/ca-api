<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace entity;

use constval\ConstCode;
use constval\ConstKey;

/**
 * Class CaApiEntity 结果模型
 * @package app\common\model
 */
class CaApiEntity
{
    //data结构
    public static function data($code, $msg, $data = '')
    {
        return [ConstKey::KEY_CODE => $code, ConstKey::KEY_MSG => $msg, ConstKey::KEY_DATA => (empty($data) ? '' : $data)];
    }

    //分页结构
    public static function list($code, $msg, $list, $pageIndex, $pageTotal, $pageTotalRow)
    {
        return [ConstKey::KEY_CODE => $code, ConstKey::KEY_MSG => $msg, ConstKey::KEY_DATA => [
            ConstKey::KEY_PAGE_LIST => $list, ConstKey::KEY_PAGE_INDEX => $pageIndex, ConstKey::KEY_PAGE_TOTAL => $pageTotal, ConstKey::KEY_PAGE_TOTALRow => $pageTotalRow
        ]];
    }

    //分页+data结构
    public static function listdata($code, $msg, $list, $pageIndex, $pageTotal, $pageTotalRow, $data)
    {
        $resData = CaApiEntity::list($code, $msg, $list, $pageIndex, $pageTotal, $pageTotalRow);
        if (!empty($data)) $resData[ConstKey::KEY_DATA][ConstKey::KEY_PAGE_DATA] = $data;
        return $resData;
    }

    //固定输出
    public static function jsondata($data = '', $msg = '', $code = ConstCode::CODE_FAIL)
    {
        if (empty($msg)) $msg = ConstCode::MESSAGE_FAIL;
        return json(CaApiEntity::data($code, $msg, (empty($data) ? '' : $data)));
    }

    //自动切换输出(开发时为实际提示，生产时为统一“无权限”提醒)
    public static function jsondata_auto($realdata, $uniformdata = null)
    {
        if (null == $uniformdata) $uniformdata = CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_POWER, ConstCode::MESSAGE_ERROR_INVALIDATED_POWER);
        return json(app_dev() ? $realdata : $uniformdata);
    }
}