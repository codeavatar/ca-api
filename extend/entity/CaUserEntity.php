<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace entity;

/**
 * Class CaUserEntity 用户登录的公用数据
 * @package app\common\model
 */
class CaUserEntity
{
    private $userId;
    private $userToken;
    private $longitude;
    private $latitude;
    private $provinceId;
    private $cityId;
    private $districtId;
    private $countyId;

    public function __construct()
    {
        $this->setUserId(fwValPost(FwKeyNameModel::KEY_USERID));
        $this->setUserToken(fwValPost(FwKeyNameModel::KEY_USERTOKEN));
        $this->setLongitude(fwValPost(FwKeyNameModel::KEY_USERLONGITUDE));
        $this->setLatitude(fwValPost(FwKeyNameModel::KEY_USERLATITUDE));
        $this->setProvinceId(fwValPost(FwKeyNameModel::KEY_USERPROVINCEID));
        $this->setCityId(fwValPost(FwKeyNameModel::KEY_USERCITYID));
        $this->setDistrictId(fwValPost(FwKeyNameModel::KEY_USERDISTRICTID));
        $this->setCountyId(fwValPost(FwKeyNameModel::KEY_USERCOUNTYID));
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->userToken;
    }

    /**
     * @param mixed $userToken
     */
    public function setUserToken($userToken)
    {
        $this->userToken = $userToken;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * @param mixed $provinceId
     */
    public function setProvinceId($provinceId)
    {
        $this->provinceId = $provinceId;
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param mixed $cityId
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return mixed
     */
    public function getDistrictId()
    {
        return $this->districtId;
    }

    /**
     * @param mixed $districtId
     */
    public function setDistrictId($districtId)
    {
        $this->districtId = $districtId;
    }

    /**
     * @return mixed
     */
    public function getCountyId()
    {
        return $this->countyId;
    }

    /**
     * @param mixed $countyId
     */
    public function setCountyId($countyId)
    {
        $this->countyId = $countyId;
    }


}