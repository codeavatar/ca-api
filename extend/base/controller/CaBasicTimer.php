<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace base\controller;


use app\BaseController;
use app\middleware\TimerCheck;

class CaBasicTimer extends BaseController
{
    /**
     * 控制器中间件
     * @var string[]
     */
    protected $middleware = [
        TimerCheck::class
    ];


}