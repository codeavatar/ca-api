<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace base\controller;


use app\BaseController;
use app\middleware\RbacCheck;

class CaBasicWap extends BaseController
{
    //控制器中间件
    protected $middleware = [
        RbacCheck::class
    ];

    //页面标题
    public $title;

    //默认操作数据表
    public $table;

    //操作标识
    protected $action;

    /**
     * 表单默认操作
     * @param Query $dbQuery 数据库查询对象
     * @param string $tplFile 显示模板名字
     * @param string $pkField 更新主键规则(仅支持单一主键，反之必须指定)
     * @param array $where 查询规则
     * @param array $extendData 扩展数据
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    protected function wap_form($dbQuery = null, $tplFile = '', $pkField = '', $where = [], $extendData = [])
    {
        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);
        $pk = empty($pkField) ? ($db->getPk() ? $db->getPk() : 'id') : $pkField;
        $pkValue = $this->request->request($pk, isset($where[$pk]) ? $where[$pk] : (isset($extendData[$pk]) ? $extendData[$pk] : null));
        // 非POST请求, 获取数据并显示表单页面
        if (!$this->request->isPost()) {
            $vo = ($pkValue !== null) ? array_merge((array)$db->where($pk, $pkValue)->where($where)->find(), $extendData) : $extendData;
            //@TODO
            if (false !== $this->_ca_callback('_ca_form_filter', $vo, [])) {
                empty($this->title) || View::assign('title', $this->title);
                return View::fetch($tplFile, ['vo' => $vo]);
            }
            return $vo;
        }
        // POST请求, 数据自动存库
        $post = $this->request->post();
        foreach ($post as $k => $v) {
            if (!is_array($v)) $post[$k] = trim($v);
        }
        $data = array_merge($post, $extendData);
        //@TODO
        if (false !== $this->_ca_callback('_ca_form_filter', $data, [])) {
            //自动更新时间
            if ($pkValue !== null) {
                if (in_array('update_time', $db->getTableFields())) {
                    $data['update_time'] = date('Y-m-d H:i:s');
                }
            }
            $result = DataService::save($db, $data, $pk, $where);
            //@TODO
            if (false !== $this->_ca_callback('_ca_form_result', $result, $data)) {
                if ($result !== false) {
                    return $this->success('数据保存成功!');
                }
                return $this->error('数据保存失败, 请稍候再试!');
            }
        }
    }

    /**
     * 列表集成处理方法
     * @param Query $dbQuery 数据库查询对象
     * @param bool $isPage 是启用分页
     * @param bool $isDisplay 是否直接输出显示
     * @param bool $total 总记录数
     * @param array $result 结果集
     * @return array|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    protected function wap_list($dbQuery = null, $isPage = true, $isDisplay = true, $total = false, $result = [])
    {
        $db = is_null($dbQuery) ? Db::name($this->table) : (is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery);
        // 列表排序默认处理
        if ($this->request->isPost() && $this->request->post('action') === 'resort') {
            foreach ($this->request->post() as $key => $value) {
                if (preg_match('/^_\d{1,}$/', $key) && preg_match('/^\d{1,}$/', $value)) {
                    list($where, $update) = [['id' => trim($key, '_')], ['sort' => $value]];
                    if (false === Db::table($db->getTable())->where($where)->update($update)) {
                        return $this->error('列表排序失败, 请稍候再试');
                    }
                }
            }
            return $this->success('列表排序成功, 正在刷新列表');
        }
        // 列表数据查询与显示
//        if (null === $db->getOptions('order')) {
//            in_array('sort', $db->getTableFields($db->getTable())) && $db->order('sort asc');
//        }
        if ($isPage) {
            $rows = intval($this->request->get('rows', cookie('page-rows')));
            cookie('page-rows', $rows = $rows >= 10 ? $rows : 20);
            // 分页数据处理
            $query = $this->request->get();
            $page = $db->paginate(['query'     => $query,'list_rows' => $rows],$total);
            if (($totalNum = $page->total()) > 0) {
                list($rowHTML, $curPage, $maxNum) = [[], $page->currentPage(), $page->lastPage()];
                foreach ([10, 20, 50, 100, 150, 200] as $num) {
                    list($query['rows'], $query['page']) = [$num, '1'];
                    $url = url('@core') . '#' . $this->request->baseUrl() . '?' . urldecode(http_build_query($query));
                    $rowHTML[] = "<option ca-url='{$url}' " . ($rows === $num ? 'selected' : '') . " value='{$num}'>{$num}</option>";
                }
                list($pattern, $replacement) = [['|href="(.*?)"|', '|pagination|'], ['ca-open="$1"', 'pagination pull-right']];
                $html = "<span class='ca-pagination-trigger nowrap'>共 {$totalNum} 条记录，每页显示 <select lay-ignore>" . join('', $rowHTML) . "</select> 条，共 {$maxNum} 页当前显示第 {$curPage} 页。</span>";
                list($result['total'], $result['list'], $result['page']) = [$totalNum, $page->all(), $html . preg_replace($pattern, $replacement, $page->render())];
            } else {
                Log::error($totalNum);
                list($result['total'], $result['list'], $result['page']) = [$totalNum, $page->all(), $page->render()];
            }
        } else {
            $result['list'] = $db->select()->toArray();
        }
        //@TODO
        if (false !== $this->_ca_callback('_ca_list_filter', $result['list'], []) && $isDisplay) {
            !empty($this->title) && View::assign('title', $this->title);
            return View::fetch('', $result);
        }
        return $result;
    }

    //单项操作
    protected function wap_action($msg='')
    {
        if(empty($msg)) $msg = input('post.result/s','操作');

        if (DataService::update($this->table)) {
            return $this->success($msg . "成功！");
        }
        return $this->error($msg . "失败，请稍候再试！");
    }

    //RBAC检测
    protected function wap_rbac($action)
    {
        $rbacuri = self::_ca_rbac_uri();

        if (is_array($action)) {
            foreach ($action as $v) {
                if (sysrbac($v)) return true;
            }
            return false;
        }

        if (is_string($action)) {
            if (false !== strpos($action, '|')) {
                $actions = explode('|', $action);
                foreach ($actions as $v) {
                    if (sysrbac($rbacuri . '/' . $v)) {
                        return true;
                    }
                }
                return false;
            }
            return sysrbac($rbacuri . '/' . $action);
        }
    }

    protected function wap_rbac_uri()
    {
        $rbacuri = \think\facade\View::__get('rbacuri');
        return (empty($rbacuri) ? '' : $rbacuri);
    }
}