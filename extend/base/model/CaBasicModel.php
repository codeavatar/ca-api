<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace base\model;


use base\entity\CaPaginationEntity;
use think\helper\Str;
use think\Model;

/**
 * 基础模型
 * Class BasicModel
 * @package model
 */
abstract class CaBasicModel extends Model
{
    protected $prefix = '';

    public function __construct(array $data = [])
    {
        $this->prefix = config('database.connections.mysql.prefix');
        parent::__construct($data);
    }

    //获取数据表
    protected function getDbName($name)
    {
        if(false === strpos($name,'_')){
            $name = Str::snake($name);
        }
        return (!Str::startsWith($name, $this->prefix)) ? $this->prefix . $name : $name;
    }

    //分页列表
    public function getPageList(CaPaginationEntity $entity){

        $query = $this->field($entity->getField())->where($entity->getWhere())->order($entity->getOrderby());
        //原生条件
        if(!empty($entity->getWhereRaw())){
            foreach ($entity->getWhereRaw() as $item){
                $query->whereRaw($item);
            }
        }
        //字符串条件
        if(!empty($entity->getWhereStr())){
            $query->where($entity->getWhereStr());
        }

        if($entity->isIsPagination()){
            $totalRows = $query->count();
            $totalPages = ($totalRows%$entity->getPageSize() > 0 ? intval($totalRows/$entity->getPageSize())+1 : intval($totalRows/$entity->getPageSize()));
            $collection = $query->page($entity->getPageIndex(), $entity->getPageSize())->select();
            return [
                'list'=>$this->getSelectToArray($collection),
                'total_rows'=>$totalRows,
                'total_pages'=>$totalPages
            ];
        }
        if(-1 !== $entity->getLimit()){
            $query->limit($entity->getLimit());
        }
        $collection = $query->select();
        return $this->getSelectToArray($collection);
    }

    //关联表分页结果集
    public function getDbPageList($query,$page_index,$page_size){
        //分页数据
        $totalRows = $query->count();
        $totalPages = ($totalRows%$page_size > 0 ? intval($totalRows/$page_size)+1 : intval($totalRows/$page_size));
        $collection = $query->page($page_index,$page_size)->select();
        //处理数据
        $tmp_list = ($collection->isEmpty()) ? [] : $collection->toArray();
        return [
            'list'=>$tmp_list,
            'total_rows'=>$totalRows,
            'total_pages'=>$totalPages
        ];
    }

    //模型数据对象转普通数组
    public function getSelectToArray($collection){
        return ($collection->isEmpty()) ? [] : $collection->toArray();
    }
}