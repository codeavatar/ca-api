<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace base\entity;


use think\helper\Str;

class CaBasicEntity
{
    //同步数据
    public function synchronizedData($data){
        foreach ($data as $key=>$val){
            call_user_func(array($this,$this->setEntityMethodName($key)),$this->dataNullFilter($key, $val));
        }
    }

    //转数组（属性必须为public类型）
    public function toArray(){
        return (json_decode(json_encode($this,JSON_FORCE_OBJECT),true));
    }

    //转JsonObject字符串（属性必须为public类型）
    public function toJsonObject(){
        return json_encode($this,JSON_FORCE_OBJECT);
    }

    //转字符串（属性必须为public类型）
    public function toString(){
        return strval(json_encode($this,JSON_FORCE_OBJECT));
    }

    //对象自动转字符串（属性必须为public类型）
    public function __toString(){
        return strval(json_encode($this,JSON_FORCE_OBJECT));
    }

    /**
     * 私有函数
     */

    private function setEntityMethodName($key){
        return 'set'.(false === strpos($key,'_') ? Str::title($key) : Str::camel($key));
    }

    private function getEntityMethodName($key){
        return 'get'.(false === strpos($key,'_') ? Str::title($key) : Str::camel($key));
    }

    //null值过滤器
    private function dataNullFilter($key,$val){
        if(is_null($val)){
            $getVal = call_user_func(array($this,$this->getEntityMethodName($key)));
            if(is_string($getVal)){
                return '';
            }
            if(is_array($getVal)){
                return [];
            }
            if(is_int($getVal) || is_long($getVal)){
                return 0;
            }
            if(is_bool($getVal)){
                return false;
            }
            if(is_float($getVal) || is_double($getVal)){
                return 0;
            }
            return '';
        }
        return $val;
    }
}