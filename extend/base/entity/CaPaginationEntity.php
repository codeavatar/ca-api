<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace base\entity;

class CaPaginationEntity extends CaBasicEntity
{
    private $field = '*';
    private $where = [];
    private $whereRaw = [];//['raw1','raw2']
    private $whereStr = '';
    private $orderby = 'id DESC';
    private $page_index = 0;
    private $page_size = 10;
    private $is_pagination = false;
    private $limit = -1;

    public function __construct($page_index=0,$page_size=10)
    {
        $this->page_index = $page_index;
        $this->page_size = $page_size;
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField(string $field): void
    {
        $this->field = $field;
    }

    /**
     * @return array
     */
    public function getWhere(): array
    {
        return $this->where;
    }

    /**
     * @param array $where
     */
    public function setWhere(array $where): void
    {
        $this->where = $where;
    }

    /**
     * @return array
     */
    public function getWhereRaw(): array
    {
        return $this->whereRaw;
    }

    /**
     * @param array $whereRaw
     */
    public function setWhereRaw(array $whereRaw): void
    {
        $this->whereRaw = $whereRaw;
    }

    /**
     * @return string
     */
    public function getWhereStr(): string
    {
        return $this->whereStr;
    }

    /**
     * @param string $whereStr
     */
    public function setWhereStr(string $whereStr): void
    {
        $this->whereStr = $whereStr;
    }

    /**
     * @return string
     */
    public function getOrderby(): string
    {
        return $this->orderby;
    }

    /**
     * @param string $orderby
     */
    public function setOrderby(string $orderby): void
    {
        $this->orderby = $orderby;
    }

    /**
     * @return int
     */
    public function getPageIndex(): int
    {
        return $this->page_index;
    }

    /**
     * @param int $page_index
     */
    public function setPageIndex(int $page_index): void
    {
        $this->page_index = $page_index;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->page_size;
    }

    /**
     * @param int $page_size
     */
    public function setPageSize(int $page_size): void
    {
        $this->page_size = $page_size;
    }

    /**
     * @return bool
     */
    public function isIsPagination(): bool
    {
        return $this->is_pagination;
    }

    /**
     * @param bool $is_pagination
     */
    public function setIsPagination(bool $is_pagination): void
    {
        $this->is_pagination = $is_pagination;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }


}