<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types=1);

namespace base\validate;

use think\Validate;

/**
 * 框架基础验证器
 * Class BasicValidate
 * @package validate
 */
abstract class CaBasicValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [];

    //+--------------------------------
    //+可以使用以下方法，也可使用Validate内置
    //+--------------------------------

    public function addRule($key, $val)
    {
        self::rule($key, $val);
    }

    public function removeRule($key)
    {
        self::remove($key);
    }

    public function addMessage($key, $val)
    {
        self::message([$key, $val]);
    }

    public function removeMessage($key)
    {
        unset($this->message[$key]);
    }

    public function addRuleMessage(array $rule, array $msg)
    {
        self::addRule($rule[0], $rule[1]);
        self::addMessage($msg[0], $msg[1]);
    }

    public function isPassed($data): bool
    {
        return self::rule($this->rule)->message($this->message)->check($data);
    }

    public function errorMsg()
    {
        return self::getError();
    }
}
