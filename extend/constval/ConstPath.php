<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;

class ConstPath
{
    //本地附件根目录
    public static function getRootPath()
    {
        $use_type = config('apiconfig.ca_upload')['use_upload'];
        $is_local = (strpos($use_type, 'local.') !== false);
        $local_tag = str_replace('local.', '', $use_type);
        return ($is_local ? config("filesystem.disks.{$local_tag}")['root'] : '');
    }

    //创建指定目录
    public static function getNewPath($path)
    {
        return rtrim($path, '/');
    }

    //获取精简路径
    public static function getShortPath($filename){
        $root_path = self::getRootPath();
        if (false !== strpos($filename, $root_path)) {
            $filename = substr($filename, (strpos($filename, $root_path)+strlen($root_path)));
        }
        return $filename;
    }

    //获取完整路径
    public static function getFullPath($filename){
        $root_path = self::getRootPath();
        if (false === strpos($filename, $root_path)) {
            $filename =  $root_path .$filename;
        }
        return $filename;
    }

    //路径过滤器
    public static function pathFilter($path, $is_slash = false){
        if($is_slash){
            return str_replace('\/', DIRECTORY_SEPARATOR, $path);
        }
        return str_replace('\\', '/', $path); //不能使用 DIRECTORY_SEPARATOR 该变量
    }

    //网络附件根目录
    public static function getBaseFileUrl(){
        return ca_http_file();
    }

    //网络全路径
    public static function getFullUrl($filename){
        return self::getBaseFileUrl().$filename;
    }
}