<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;


class ConstRegExp
{
    const REGEXP_NO_SPACE = '^[\S]{6,12}$';//不能含有空格
    const REGEXP_BETWEEN_NO_UNDERLINE = '(^\_)|(\__)|(\_+$)'; //首尾不能出现下划线_
    const REGEXP_USERNAME = '^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$';//用户名不能有特殊字符
    const REGEXP_DECIMAL = '^(0\.(?!0+$)\d{1,2}|^[1-9][0-9]{0,8}(\.\d{0,2})?|0|0.00)$';//整数最大8位，小数最大2位
    const REGEXP_EMAIL = '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$';
    const REGEXP_CELLPHONE = '^1[3-9][0-9]{9}$';
    const REGEXP_HTML_IMG = '<[img|IMG].*?src=[\'|"](.*?(?:[\.gif|\.jpg]))[\'|"].*?[\/]?>';
}