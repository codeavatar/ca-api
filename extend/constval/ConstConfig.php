<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;


class ConstConfig
{
    //+----------------------------------------------------
    // apiapp 配置
    //+----------------------------------------------------

    //开发状态
    public static function isDev(): bool
    {
        return config('apiapp.app_dev');
    }

    //混淆字符
    public static function getConfuseStr(): string
    {
        return config('apiapp.app_pwd_confuse_str');
    }

    //+----------------------------------------------------
    // apiconfig 配置
    //+----------------------------------------------------

    //md5密钥
    public static function getMd5SecretKey(): string
    {
        return config('apiconfig.ca_api')['md5_secret_key'];
    }


    //token长度
    public static function getTokenLen(): int
    {
        return config('apiconfig.ca_limit')['token_length'];
    }


}