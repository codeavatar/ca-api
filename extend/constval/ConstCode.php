<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;

/**
 * Class ConstCode 结果状态编号
 * @package constval
 */
class ConstCode
{
    //默认代码
    const CODE_RELOGIN = -111;
    const CODE_LOGIN = -1;
    const CODE_SUCCESS = 0;
    const CODE_FAIL = 1;
    const MESSAGE_RELOGIN = '您已在其他设备登录，请重新登录';
    const MESSAGE_LOGIN = '请登录';
    const MESSAGE_SUCCESS = '操作完成';
    const MESSAGE_FAIL = '操作失败';

    //失败代码
    const CODE_ERROR_INVALIDATED_METHOD = 101;
    const CODE_ERROR_INVALIDATED_PARAM = 102;
    const CODE_ERROR_INVALIDATED_TIMESTAMP = 103;
    const CODE_ERROR_INVALIDATED_SIGN = 104;
    const CODE_ERROR_INVALIDATED_TOKEN = 105;
    const CODE_ERROR_INVALIDATED_IP = 106;
    const CODE_ERROR_INVALIDATED_TOKEN_LENGTH = 107;
    const CODE_ERROR_INVALIDATED_POWER = 888;
    const CODE_ERROR_NONE = 999;

    const MESSAGE_ERROR_INVALIDATED_METHOD = '必须以POST形式访问API';
    const MESSAGE_ERROR_INVALIDATED_PARAM = '平台、设备、版本、随机字符串、随机数字、时间戳、签名为必要参数，且长度合理';
    const MESSAGE_ERROR_INVALIDATED_TIMESTAMP = '时间戳无效';
    const MESSAGE_ERROR_INVALIDATED_SIGN = '签名无效';
    const MESSAGE_ERROR_INVALIDATED_TOKEN = 'Token已过期';
    const MESSAGE_ERROR_INVALIDATED_IP = 'IP无效';
    const MESSAGE_ERROR_INVALIDATED_TOKEN_LENGTH = 'Token长度不足';
    const MESSAGE_ERROR_INVALIDATED_POWER = '无权限';
    const MESSAGE_ERROR_NONE = '未知错误';
}