<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;

/**
 * Class CaKeyModel ca键名
 * @package app\common\model
 */
class ConstKey
{
    //API键
    const KEY_CODE = 'caCode';
    const KEY_MSG = 'caMsg';
    const KEY_DATA = 'caData';
    const KEY_PAGE_DATA = 'caPageData';
    const KEY_PAGE_LIST = 'caPageList';
    const KEY_PAGE_INDEX = 'caPageIndex';
    const KEY_PAGE_TOTAL = 'caPageTotal';
    const KEY_PAGE_TOTALRow = 'caPageTotalRow';
    //api访问必须公用的参数(还有签名字符串fwSign)
    const KEY_PLATFORM = 'caPlatform';
    const KEY_DEVICE = 'caDevice';
    const KEY_VERSION = 'caVersion';
    const KEY_RNDSTR = 'caRndstr';
    const KEY_RNDNUM = 'caRndnum';
    const KEY_TIMESTAMP = 'caTimestamp';
    const KEY_SIGN = 'caSign';
    const KEY_USERID = 'caUserId';
    const KEY_USERTOKEN = 'caUserToken';
    const KEY_USERTYPE = 'caUserType';
    const KEY_USERLONGITUDE = 'caUserLongitude';
    const KEY_USERLATITUDE = 'caUserLatitude';
    const KEY_USERPROVINCEID = 'caUserProvinceId';
    const KEY_USERCITYID = 'caUserCityId';
    const KEY_USERDISTRICTID = 'caUserDistrictId';
    const KEY_USERCOUNTYID = 'caUserCountyId';
    const KEY_TEAMID = 'caTeamId';
    const KEY_TEAMTOKEN = 'caTeamToken';
    //特殊后缀
    const KEY_SUFFIX_JSONSTR = '_cajson';
    const KEY_SUFFIX_ATTACHMENT = '_cafile';
}