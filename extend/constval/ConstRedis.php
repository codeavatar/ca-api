<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace constval;


class ConstRedis
{
    //前缀
    const PREFIX_TIMER = 'api.timer_';
    const PREFIX_SMS = 'api.sms_';
    const PREFIX_LOCK = 'api.lock_';

    //标签
    const TAG_TIMER = 'timer';

    //Timer键
    const TIMER_REJECT_IPS = 'timer_reject_ips';
}