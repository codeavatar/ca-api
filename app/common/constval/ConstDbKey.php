<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\common\constval;


class ConstDbKey
{
    //单页键名
    const AGREEMENT_USER = 'user_agreement'; //用户协议
    const AGREEMENT_RPIVATE = 'private_agreement'; //注册协议

}