<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\common\constval;


class ConstAppVal
{
    ///用户角色
    const USER_ROLE_STUDENT = 1;
    const USER_ROLE_TEACHER = 2;


}