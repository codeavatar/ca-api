<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\controller;

use third\express\ExpressClient;
use third\express\ExpressEntity;
use third\map\MapClient;
use third\map\MapEntity;
use third\payment\entity\PayCheckEntity;
use third\payment\entity\PayEntity;
use third\payment\PayClient;
use third\payment\PayType;
use third\payment\wechatpay\WechatNotifyCallback;
use third\qrcode\QRcode;
use third\sms\SmsClient;
use third\sms\SmsEntity;

class Third
{
    //短信
    public function sms()
    {
        $tophone = '10000000000000';//仅支持11位手机号

        $type = self::ptype();
        $init = new SmsClient();
        switch ($type) {
            case 1:
                $entity = new SmsEntity();
                $entity = $init->send($tophone); //使用默认配置
//                $msg = $init->template($init->getCode($tophone,6,120),120); //使用默认模版，自定义有效时间与验证码
//                $entity = $init->send($tophone);
//                $init->checkCode($tophone,132456);
                ca_debug($entity);
                break;
            case 2:
                $code = self::ptype('code/d');
                ca_debug(($init->checkCode($tophone,$code))?'通过':'错误');
                break;
        }
    }

    //二维码
    public function qrcode()
    {

        require_once root_path() . 'extend\third\qrcode\phpqrcode.php';

        //1.配置与说明

        $data = '测试生成1';//内容
        $level = 'L';// 纠错级别：L、M、Q、H
        $size = 10;//元素尺寸
        $margin = 1;//边距
        $outfile = 'qrcode/erweima.png';
        $saveandprint = true;// true直接输出屏幕  false 保存到文件中
        $back_color = 0xFFFFFF;//白色底色
        $fore_color = 0x000000;//黑色二维码色 若传参数要hexdec处理，如 $fore_color = str_replace('#','0x',$fore_color); $fore_color = hexdec('0xCCCCCC');

        // 可在 phpqrcode.php 文件中修改以下配置
        // define('QR_FIND_BEST_MASK', true); // true 每次生成码都会变换掩码 ， false 时只要内容不变，生成图案不变
        // define('QR_PNG_MAXIMUM_SIZE', 1024);//生成最大图片尺寸，若要更大的尺寸，可以自己修改，根据自身需求和服务器性能决定

        $type = self::ptype();
        if ($type === 1) {
            //生成png图片
            QRcode::png($data, $outfile, $level, $size, $margin, $saveandprint);
            //覆盖LOGO
            //加上erweima.png会在根目录生成这个名字的二维码图片
            //带有logo的二维码beginning
            $logo = public_path() . '\qrcode\logo.png';
            $qr = $outfile;
            if ($logo !== FALSE) {
                $qr = imagecreatefromstring(file_get_contents($qr));
                $logo = imagecreatefromstring(file_get_contents($logo));
                $qr_width = imagesx($qr);
                $qr_height = imagesy($qr);
                $logo_width = imagesx($logo);
                $logo_height = imagesy($logo);
                $logo_qr_width = $qr_width / 5;
                $scale = $logo_width / $logo_qr_width;
                $logo_qr_height = $logo_height / $scale;
                $from_width = ($qr_width - $logo_qr_width) / 2;
                imagecopyresampled($qr, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
            }
            imagepng($qr, $outfile);
            //缓存
//            ob_start();
            $outfile = ca_http_root() . $outfile;
            echo "<img src='{$outfile}' style='width:100px;'>";
//            $imageString = base64_encode(ob_get_contents());
//            ob_end_clean();
//            $src = "data:image/jpg;base64,".$imageString;
//            echo $src;
//            ob_flush();
        } else if ($type === 2) {
            $outfile = 'qrcode/erweima.jpg';
            $outfile = false;
            //生成raw图片
            $img_data = QRcode::raw($data,$outfile);
            halt($img_data);
        } else if ($type === 3) {
            //保存到文本 1表示黑色点  0表示白色点
            $outfile = 'qrcode/erweima.text';
            $outfile = false;//不设置 outfile  返回数组
            $text = QRcode::text($data, $outfile, $level, $size, $margin);
            print_R($text);
        }
    }

    //支付异部通知 (需实际测试)
    public function payment_notify()
    {
        $type = self::ptype();

        if ($type === 1) {
            //微信回调
            $notify = new WechatNotifyCallback(array($this, 'wechat_handler'));
            $notify->Handle(null, false);
            return;
        }

        //支付宝回调
        $initChk = new PayCheckEntity();
        //不使用以下获取参数，原因为会被转义
        //$return_info = $this->request->post();
        //直接原生获取POST参数
        $return_info = $_POST;
        $initChk->setPostParam($return_info);
        $init = new PayClient(PayType::PAY_ALIPAY);
        if ((!empty($return_info)) && $init->checkSign($initChk)) {
            //处理回调通知
            $data['total_fee'] = $return_info['total_amount'] * 100;
            $data['out_trade_no'] = $return_info['out_trade_no'];

            $doAction = false;
            if ($return_info['passback_params'] == 'action') {
                //执行逻辑
                $doAction = true;
            }
            die($doAction ? 'TRADE_SUCCESS' : 'TRADE_FAIL');
        }
    }

    //微信执行方法
    public function wechat_handler($data): bool
    {
        $doAction = false;
        if ($data['attach'] == 'action') {
            //执行代码
            $doAction = true;
        }
        return $doAction;
    }

    //支付
    public function payment()
    {
        $isWechatPay = true;
        $init = new PayClient(($isWechatPay ? PayType::PAY_WECHAT : PayType::PAY_ALIPAY));
        //封装支付数据
        $payEntity = new PayEntity();
        $payEntity->setTradeNo('交易订单号');
        $payEntity->setSubject('交易主题说明');
        $payEntity->setBody('交易详情说明');
        $payEntity->setAmount(1);//交易金额（浮点类型）
        $payEntity->setPassbackParam('action'); //回传参数（原封不动）
        $payData = $init->getPayInfo($payEntity);
        ca_debug($payData);
    }

    //地图
    public function map()
    {
        $init = new MapClient();
        $entity = new MapEntity();
        $entity = $init->getLocation(114.501153, 36.610899);
        ca_debug($entity);
    }

    //物流
    public function express()
    {
        $init = new ExpressClient();
        $entity = new ExpressEntity();
        $entity = $init->getProcess('SF1980666268697');
        ca_debug($entity);
    }

    //操作类型
    private function ptype($pkey = 'type/d', $default = 1)
    {
        return request()->post($pkey, $default);
    }
}