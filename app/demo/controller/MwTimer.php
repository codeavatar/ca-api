<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\controller;


use app\middleware\TimerCheck;
use base\controller\CaBasicTimer;

class MwTimer extends CaBasicTimer
{
    /**
     * 控制器中间件
     * @var string[]
     */
    protected $middleware = [TimerCheck::class];

    public function index(){
        //定时器逻辑代码
        ca_debug('定时器运行中...');
    }

}