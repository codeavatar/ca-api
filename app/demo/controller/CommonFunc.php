<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\controller;


class CommonFunc
{
    public function index(){
        $data = [];
        $data[] = 'app_dev() >> '.app_dev();
        $data[] = 'app_version() >> '.app_version();
        $data[] = 'ca_unique_path() >> '.ca_unique_path();
        $data[] = 'ca_unique_path(true) >> '.ca_unique_path(true);
        $data[] = 'ca_mca() >> '.ca_mca();
        $data[] = 'ca_url() >> '.ca_url();
        $data[] = 'catp_input("get.key") >> '.catp_input('get.key','',10);
        $data[] = 'ca_ip() >> '.ca_ip();
        $data[] = 'ca_order_sn() >> '.ca_order_sn();
        $data[] = 'ca_http_root() >> '.ca_http_root();
        $data[] = 'ca_http_file() >> '.ca_http_file();
        ca_debug($data);
    }
}