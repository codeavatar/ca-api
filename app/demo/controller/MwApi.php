<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\controller;


use base\controller\CaBasicApi;
use entity\CaUploadEntity;

class MwApi extends CaBasicApi
{
    //签名检测
    public function index(){
        //API逻辑代码
        ca_debug('API运行中...');
    }

    //上传文件
    public function upload(){
        $upload_entity = new CaUploadEntity();
        $upload_entity = parent::caFileUpload('arr1,arr2,file1,file2,file3,arr3,file4');
        ca_debug($upload_entity->getData());
    }

    //文件路径配置 (上传文件)
    public function fullpath(){
        $data = [
            'jpg/20210114/52bc57dac310de706c2a54d3fc4bce9d.jpg',
            'jpg/20210114/06d0a25a062244182a7a863194da5e9d.jpg',
            'jpg/20210114/b6153b99824aeda9270ff5585dd1ab96.jpg',
            'jpg/20210114/76a109c21f7a9c61de320661c6c33aeb.jpg',
            'jpg/20210114/55fc71c0332701ab2c5f8e0cf1f6819c.jpg'
        ];
//        $data = 'jpg/20210114/52bc57dac310de706c2a54d3fc4bce9d.jpg';
        parent::caFilePath($data);
        ca_debug($data);
    }
}