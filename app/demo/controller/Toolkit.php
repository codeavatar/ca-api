<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\controller;


use constval\ConstKey;
use toolkit\entity\CaCurlEntity;
use think\Log;
use toolkit\tool\CaCurlTool;
use toolkit\tool\CaDatetimeTool;
use toolkit\tool\CaEncrypTool;
use toolkit\tool\CaLogTool;
use toolkit\tool\CaSignTool;
use toolkit\tool\CaStringTool;
use toolkit\tool\CaTimeTool;
use toolkit\tool\CaTreeTool;

class Toolkit
{
    //树型结构
    public function tree(){
        $type = request()->post('type/d',1);
        $treedata = [
            ['id'=>1,'pid'=>'0','title'=>'1号'],['id'=>2,'pid'=>'0','title'=>'2号'],['id'=>3,'pid'=>'1','title'=>'3号'],['id'=>4,'pid'=>'2','title'=>'4号'],
            ['id'=>5,'pid'=>'1','title'=>'5号'],['id'=>6,'pid'=>'2','title'=>'6号'],['id'=>7,'pid'=>'1','title'=>'7号'],['id'=>8,'pid'=>'2','title'=>'8号'],
            ['id'=>10,'pid'=>'5','title'=>'10号'],['id'=>11,'pid'=>'4','title'=>'11号'],['id'=>12,'pid'=>'7','title'=>'12号'],['id'=>13,'pid'=>'4','title'=>'13号'],
        ];
        $tree_array = ($type == 1)?CaTreeTool::arrayToTree($treedata): CaTreeTool::arrayToTreeTable($treedata);
        ca_debug($tree_array);
    }

    //计时器
    public function timer(){
        $init = new CaTimeTool();
        $init->start();
        $n=0;
        //普通计算机最大1亿运算量，否则太慢
        for ($i=0; $i<100000000;$i++){
            $n++;
        }
        $init->stop();
        $init->outprint('timer');
        die('输出到runtime日志文件中了');
    }

    //字符串
    public function string(){
        $init = new CaStringTool();
        $data = array();
        $data[] = '=======================================【静态方法】==============================================';
        $data[] = '测试字符串 >> '. 'Hello world';
        $data[] = '检测前缀Hello >> '. (CaStringTool::startsWith('Hello world','Hello')?'是':'否');
        $data[] = '检测后缀world >> '. (CaStringTool::endsWith('Hello world','world')?'是':'否');
        $data[] = '检测包含ll >> '. (CaStringTool::contains('Hello world','ll')?'是':'否');
        $data[] = '下划线转驼峰(首字母小写) >> '. CaStringTool::camel('hello_world');
        $data[] = '驼峰转下划线 >> '. CaStringTool::snake('helloWorld');
        $data[] = '下划线转驼峰(首字母大写) >> '. CaStringTool::studly('hello_world');
        $data[] = '转为首字母大写的标题格式 >> '. CaStringTool::title('hello world');
        $data[] = '截取字符串 >> '. CaStringTool::substr('hello world',1,4);
        $data[] = '获取字符串的长度 >> '. CaStringTool::length('hello world');
        $data[] = '字符串转大写 >> '. CaStringTool::upper('hello world');
        $data[] = '字符串转小写 >> '. CaStringTool::lower('hello world');
        $data[] = '获取指定长度的随机字母数字组合的字符串 >> '. CaStringTool::random(50);//type:0-5
        $data[] = '=======================================【动态方法】==============================================';
        $data[] = '获取毫秒 >> '.$init->getMillisecond();
        $data[] = '是否为空 >> '.($init->checkEmpty('')?'是':'否');
        $data[] = '获取唯一时间戳 >> '.$init->getUniqueTimestamp();
        $data[] = '获取唯一字符串 >> '.$init->getUniqueStr();
        $data[] = '获取唯一订单号 >> '.$init->getUniqueSN();
        $data[] = '随机字符串 >> '.$init->getRandomStr(100);
        $data[] = '截取字符串 >> '.$init->getSubstr('abcdefghijklmnopqrstuvwxyz',2,5);
        CaLogTool::writelog($data);
        ca_outpu_arr($data);
    }

    //加密
    public function encrypt(){
        $init = new CaEncrypTool();
        $str = '123';
        $secret_key = '01234567890123456'; //仅限16位长度

        $data = [];
        $ciphertext = $init->aes_encrypt($str,$secret_key);
        $data[] = '密文 >> ' . $ciphertext;
        $data[] = '明文 >> ' . $init->aes_decrypt($ciphertext,$secret_key);
        CaLogTool::writelog($data);
        ca_outpu_arr($data);
    }

    //时间
    public function datetime(){
        $init = new CaDatetimeTool();
        $data = array();
        $data[] = '当前时区 >> '.$init->getTimeArea().PHP_EOL;$init->setReset();
        $data[] = '时间 >> '.$init->getNewDatetimeStr().'|||'.'日期 >> '.$init->getNewDatestr().PHP_EOL;$init->setReset();
        $data[] = '2021-01-09至2021-01-11差几天 >> '.$init->setDiffDatestr('2021-01-11','2021-01-09')->getDiffDays().PHP_EOL;$init->setReset();
        $data[] = '2021-01-09至2021-01-11差多少秒 >> '.$init->setDiffDatestr('2021-01-11','2021-01-09')->getDiffSeconds().PHP_EOL;$init->setReset();
        $data[] = '2021-01-09加5天 >> '.$init->setSymbol(true)->setDay(5)->getNewDatestr('2021-01-09').PHP_EOL;$init->setReset();
        $data[] = '当前时间加3天 >> '.$init->setSymbol(true)->setDay(3)->getNewDatestr().PHP_EOL;$init->setReset();
        $data[] = '当前时间加3天【另一种】 >> '.$init->setDiffstr('+')->setDay(3)->getNewDatestr().PHP_EOL;$init->setReset();
        $data[] = '当前是否为闰年 >> '.($init->isLeapYear()?'闰年':'平年').PHP_EOL;$init->setReset();
        $data[] = '当前年-月-日 时：分：秒：微秒 >> '.$init->getYear().'-'.$init->getMonth().'-'.$init->getMonthDay().' '.$init->getHour().'：'.$init->getMinite().'：'.$init->getSeconds().'：'.$init->getUSeconds().PHP_EOL;$init->setReset();
        $data[] = '年中第N天 >> '.$init->getYearDay().PHP_EOL;$init->setReset();
        $data[] = '当月共N天 >> '.$init->getMonthDays().PHP_EOL;$init->setReset();
        $data[] = '月中的第N天 >> '.$init->getMonthDay(null,false).PHP_EOL;$init->setReset();
        $data[] = '周中的第N天（周日为第一天） >> '.$init->getWeekDay(null,false).'（'.$init->getWeekDayText($init->getWeekDay(null,false), false).'）'.PHP_EOL;$init->setReset();
        $data[] = '周中的第N天（周一为第一天） >> '.$init->getWeekDay(null,true).'（'.$init->getWeekDayText($init->getWeekDay(null,true), true).'）'.PHP_EOL;$init->setReset();
        $data[] = '时间格式一 >> '.$init->setFormatstr('Y-m-d')->getNewDatestr().PHP_EOL;$init->setReset();
        $data[] = '时间格式二 >> '.$init->setFormatstr('Y-m-d H:i:s')->getNewDatetimeStr().PHP_EOL;
        CaLogTool::writelog($data);
        ca_outpu_arr($data);
    }

    //日志
    public function log()
    {
        $logs = ['日志1', '日志2']; //$logs = 'string';
        CaLogTool::writelog($logs); //或者（ca_log($logs);）
        $logs[] = '新增的日志';
        CaLogTool::writelog($logs, Log::WARNING); //或者（ca_log($logs,Log::WARNING);）
        die('输出到runtime日志文件中了');
    }

    //签名
    public function sign()
    {
        $params = array('key1' => 'v1', 'key2' => 'v2');
        $type = request()->post('type/d',1);
        $data = [];
        if (1 == $type) {
            //MD5签名代码
            $init = new CaSignTool(true); //开启md5签名
            $data[] = 'MD5签名字符串::' . $init->md5Sign($params); //md5签名字符串 c475f27b0bf4e43d3e4bf270e2a9e1b4
            $params[ConstKey::KEY_SIGN] = '727e18f3ba1149ae309bf2ce288e3684';
            $data[] = 'MD5验签结果：：' . ($init->md5Check($params)?'通过':'未通过');
        } else if (2 == $type) {
            //RSA签名代码
            $init = new CaSignTool(); //默认使用RSA签名
            $data[] = 'RSA签名字符串::' . $init->rsaSign($params);
            //生成的签名字符串
//            iwuw/Y/yW29ba9WN4ytEC3tJq2PNVUu7DzfXqXhN5I1FTlJfa1fS17TQ2r1sYZipnlFQyrqvnX8G5y7v22nUTWiIVruW6K4Vnrbw1LLhZCPw6PzS6eKf3gtnaBA8VGN7o6h0zIqx/iJiJorN471+5Yrs0Wl8Ne1TsCSws10eDdk=
            $params[ConstKey::KEY_SIGN] = 'iwuw/Y/yW29ba9WN4ytEC3tJq2PNVUu7DzfXqXhN5I1FTlJfa1fS17TQ2r1sYZipnlFQyrqvnX8G5y7v22nUTWiIVruW6K4Vnrbw1LLhZCPw6PzS6eKf3gtnaBA8VGN7o6h0zIqx/iJiJorN471+5Yrs0Wl8Ne1TsCSws10eDdk=';
            $data[] = 'RSA验签结果：：' . ($init->rsaCheckV1($params)?'通过':'未通过'); //验签结果
        } else if (3 == $type) {
            $init = new CaSignTool(false, null, null, true);
            $data[] = 'RSA2签名字符串::' . ($init->rsaSign($params, 'RSA2')?'通过':'未通过');
            //RSA2签名字符串
//            ECBQYOyorLaEXpdu+56T911VXgHFfX/Dw3zHEwgeXvJqf0kRwWzd6OrbKodBFmOcxkw5gODIOffr2q0Je7OuxikT09rHFakunPbOwXuubY1BaYaEKgAUdXWopcQ/4efECJdiBzF5dmF8jHA8ShCJ+tWCo2/O0T5JT/aDGuwFBw9aHGcBd4FxxH+ijhe2Ff877BZNP48x2D29iRUkmUWpXe0xEoRMpuALp+c8O7MkJgz75qE78z6u9NF+EikOzdg6ASbqgxFfbePD3zPd0xRf+Kq+Th42a2qw4n/NNJw5WatTRrP5fh4W1io4ljl7wlB082OjvUkve2fUh+ne8wHAug==
            $params[ConstKey::KEY_SIGN] = 'ECBQYOyorLaEXpdu+56T911VXgHFfX/Dw3zHEwgeXvJqf0kRwWzd6OrbKodBFmOcxkw5gODIOffr2q0Je7OuxikT09rHFakunPbOwXuubY1BaYaEKgAUdXWopcQ/4efECJdiBzF5dmF8jHA8ShCJ+tWCo2/O0T5JT/aDGuwFBw9aHGcBd4FxxH+ijhe2Ff877BZNP48x2D29iRUkmUWpXe0xEoRMpuALp+c8O7MkJgz75qE78z6u9NF+EikOzdg6ASbqgxFfbePD3zPd0xRf+Kq+Th42a2qw4n/NNJw5WatTRrP5fh4W1io4ljl7wlB082OjvUkve2fUh+ne8wHAug==';
            $data[] = 'RSA2验签结果：：v1方法校验结果》' . ($init->rsaCheckV1($params, 'RSA2')?'通过':'未通过') . '|||v2方法校验结果》' . ($init->rsaCheckV2($params, 'RSA2')?'通过':'未通过');
        }
        ca_outpu_arr($data);
    }

    //模拟请求
    public function curl()
    {
        $curl = new CaCurlEntity();
        $init = new CaCurlTool();
//        //默认是POST请求
//        $init->curl('http://www.domin.com');
//        //POST请求传参数(支持附件)
//        $curl = $init->curl('http://www.shuoyunnet.com', array('p1' => 0, 'p2' => 'abc'));
        //显示头部信息 (默认不显示)
        $curl = $init->curl('http://www.baidu.com', null, 'POST', true);
//        //GET请求
//        $curl = $init->curl('http://www.domin.com', null, 'GET');
//        //显示头部信息 (默认不显示)
//        $curl = $init->curl('http://www.domin.com', null, 'GET', true);

//        echo $curl->getCode();   //响应状态码
//        echo $curl->getParams(); //响应头部信息
//        echo $curl->getBody();  //响应内容
        echo $curl->printCurlInfo(); //响应性能分析
    }
}