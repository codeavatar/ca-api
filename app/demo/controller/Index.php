<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types = 1);

namespace app\demo\controller;

class Index
{
    public function index()
    {
        return '您好！这是一个[demo]示例应用';
    }
}
