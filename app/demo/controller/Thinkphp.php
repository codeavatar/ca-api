<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/


namespace app\demo\controller;


use toolkit\tool\CaLogTool;

class Thinkphp
{
    //请求常用方法
    public function request(){
        $data = [];
        $data[] = 'url() >> '. request()->url();
        $data[] = 'url(true) >> '. request()->url(true);
        $data[] = 'domain() >> '. request()->domain();
        $data[] = 'domain(true) >> '. request()->domain(true);
        $data[] = 'rootDomain() >> '. request()->rootDomain();
        $data[] = 'baseUrl() >> '. request()->baseUrl();
        $data[] = 'baseUrl(true) >> '. request()->baseUrl(true);

        $data[] = 'root() >> '. request()->root();
        $data[] = 'root(true) >> '. request()->root(true);
        $data[] = 'rootUrl() >> '. request()->rootUrl();
        $data[] = 'baseFile() >> '. request()->baseFile();
        $data[] = 'baseFile(true) >> '. request()->baseFile(true);

        $data[] = 'pathinfo() >> '. request()->pathinfo();
        $data[] = 'ext() >> '. request()->ext();
        $data[] = 'time() >> '. request()->time();
        $data[] = 'time(true) >> '. request()->time(true);
        $data[] = 'type() >> '. request()->type();
        $data[] = 'method() >> '. request()->method();
        $data[] = 'isPost() >> '. (request()->isPost()?'是':'否'); //isGet、isPost、isPut、isDelete、isHead、isPatch、isOptions、isCli、isCgi
        $data[] = 'isAjax() >> '. (request()->isAjax()?'是':'否'); //isSsl、isJson、isAjax、isPjax
        $data[] = 'ip() >> '. request()->ip();
        $data[] = 'ip2bin() >> '. request()->ip2bin(request()->ip());
        $data[] = 'isValidIP() >> '. (request()->isValidIP(request()->ip())?'是':'否');
        $data[] = 'param() >> '. json_encode(request()->param());
        $data[] = 'isMobile() >> '. (request()->isMobile()?'是':'否');
        $data[] = 'scheme() >> '. request()->scheme();
        $data[] = 'query() >> '. request()->query();
        $data[] = 'controller() >> '. request()->controller();
        $data[] = 'action() >> '. request()->action();
        $data[] = 'getContent() >> '. request()->getContent();
        $data[] = 'getInput() >> '. request()->getInput();
        CaLogTool::writelog($data);
        ca_debug($data);
    }

    //路径
    public function path(){
        $data = [];
        $data[] = "app('http')->getName() >> ". app('http')->getName();
        $data[] = "request()->controller() >> ". request()->controller();
        $data[] = "request()->action() >> ". request()->action();
        $data[] = "app_path() >> ". app_path();
        $data[] = "base_path() >> ". base_path();
        $data[] = "config_path() >> ". config_path();
        $data[] = "public_path() >> ". public_path();
        $data[] = "root_path() >> ". root_path();
        $data[] = "runtime_path() >> ". runtime_path();
        CaLogTool::writelog($data);
        ca_debug($data);
    }

    //下载
    public function download(){
        $web_file_path = public_path('storage/').'jpg/20210101/52bc57dac310de706c2a54d3fc4bce9d.png';
        return download($web_file_path,'download.png');
    }
}