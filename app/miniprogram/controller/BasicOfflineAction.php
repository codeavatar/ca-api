<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\TeacherModel;
use think\exception\ValidateException;

//离线操作
class BasicOfflineAction extends BaseOffline
{
    //发送短信
    public function sendSms()
    {
        $param = [];
        $param['mobilephone'] = catp_input('post.mobilephone/d', -1,11);
        $param['action'] = catp_input('post.action/s', '',20);
        $data['usertype'] = catp_input('post.usertype/d', -1);
        //合理数据过滤
        if($data['usertype'] == -1) return $this->error(null,'身份标识错误！');

        //数据安全检测
        $rule = [
            'mobilephone' => 'require|mobile|length:11',
            'action' => 'require|alpha|length:2,20',
        ];
        $message = [
            'mobilephone.require' => '请输入手机号码！',
            'mobilephone.mobile' => '手机号码格式错误！',
            'mobilephone.length' => '手机号码必须是11位数字！',
            'action.require' => '请输入功能标识！',
            'action.alpha' => '功能标识必须是纯字母！',
            'action.length' => '功能标识长度必须2至20位之间！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        if(!in_array($param['action'],[ConstMpVal::SMS_FIND_PWD])) return $this->error(null,'操作代码错误！');

        //逻辑安全检测
        if(parent::isTeacher($data['usertype'])){
            //执行安全代码
        }else{
            //执行安全代码
        }

        //执行短信发送
        if(sms_send($param['mobilephone'], $param['action'])){
            return $this->success(null,'发送成功！');
        }
        return $this->error(null,'发送失败！');
    }

    //重置密码
    public function findPwd()
    {
        $param = [];
        $param['mobilephone'] = catp_input('post.mobilephone/d', -1,11);
        $param['validcode'] = catp_input('post.validcode/d', 0, 6);
        $param['userpwd'] = catp_input('post.userpwd/s', '', 20);
        $param['userconfirmpwd'] = catp_input('post.userconfirmpwd/s', '', 20);
        $data['usertype'] = catp_input('post.usertype/d', -1);
        //合理数据过滤
        if($data['usertype'] == -1) return $this->error(null,'身份标识错误！');

        //数据安全检测
        $rule = [
            'mobilephone' => 'require|mobile|length:11',
            'validcode' => 'require|number|max:6',
            'userpwd' => 'require|length:8,12',
            'userconfirmpwd'=>'require|confirm:userpwd'
        ];
        $message = [
            'mobilephone.require' => '请输入手机号码！',
            'mobilephone.mobile' => '手机号码格式错误！',
            'mobilephone.length' => '手机号码必须是11位数字！',
            'validcode.require' => '输入验证码！',
            'validcode.number' => '验证码必须是数字！',
            'validcode.max' => '验证码长度必须小于等于6位！',
            'userpwd.require' => '请输入新密码！',
            'userpwd.length' => '新密码长度必须8至12位之间！',
            'userconfirmpwd.require' => '请输入确认密码！',
            'userconfirmpwd.confirm' => '两次输入的密码不一致！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        //过滤未使用键值对
        unset($param['action']);

        //老师
        if(parent::isTeacher($data['usertype'])){
            $model = new TeacherModel();
            if($info = $model->getInfoByFilter(['mobilephone'=>$param['mobilephone']],'teacher_id,school_id')){
                //校验短信
                if(sms_chkcode($param['mobilephone'],$param['validcode'],ConstMpVal::SMS_FIND_PWD)){
                    if($model->setResetPwd($info['teacher_id'],$info['school_id'],$param['userpwd'])){
                        return $this->success(null,'新密码设置成功，请登录！');
                    }
                    return $this->success(null,'新密码设置失败！');
                }
                return $this->error(null,'短信验证码错误！');
            }
            return $this->error(null,'您输入的手机号码错误！');
        }
        //学生
        $model = new StudentModel();
        if($info = $model->getInfoByFilter(['mobilephone'=>$param['mobilephone']],'student_id,school_id')){
            //校验短信
            if(sms_chkcode($param['mobilephone'],$param['validcode'],ConstMpVal::SMS_FIND_PWD)) {
                if ($model->setResetPwd($info['student_id'], $info['school_id'], $param['userpwd'])) {
                    return $this->success(null, '新密码设置成功，请登录！');
                }
                return $this->success(null, '新密码设置失败！');
            }
            return $this->error(null,'短信验证码错误！');
        }
        return $this->error(null,'您输入的手机号码错误！');
    }
}