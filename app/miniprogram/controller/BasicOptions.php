<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;

use app\miniprogram\model\MajorModel;

class BasicOptions extends BaseOnline
{
    public function getLevel(){
        $data = [
            'level'=>[
                ['txt'=>'初级','val'=>1],['txt'=>'中级','val'=>2],['txt'=>'高级','val'=>3]
            ],
        ];
        return $this->success($data);
    }
}