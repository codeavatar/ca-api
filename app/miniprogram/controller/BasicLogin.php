<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\entity\WechatInfoEntity;
use app\miniprogram\logic\AuthLogic;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\TeacherModel;
use constval\ConstConfig;
use constval\ConstKey;
use think\exception\ValidateException;
use think\Log;
use third\wechat\minprogram\MinProgramClient;
use toolkit\tool\CaLogTool;
use toolkit\tool\CaStringTool;

class BasicLogin extends BaseOffline
{
    //账号登录（个人）
    public function personalLogin()
    {
        $data = [];
        $data['username'] = catp_input('post.username/d', '');
        $data['userpwd'] = catp_input('post.userpwd/s', '');
        $data['usertype'] = catp_input('post.usertype/d', -1);
        //合理数据过滤
        if($data['usertype'] == -1) return $this->error(null,'身份标识错误！');

        //数据安全检测
        $rule = [
          'username'=>'require|mobile',
          'userpwd'=>'require|length:8,12'
        ];
        $message = [
            'username.require' => '请输入手机号码！',
            'username.mobile' => '请正确输入11位手机号码！',
            'userpwd.require' => '请输入密码！',
            'username.length' => '请输入8至12位长度的密码！',
        ];
        try {
            $this->validate($data,$rule,$message);
        }catch(ValidateException $e){
            return $this->error(null,$e->getError());
        }

        //逻辑处理
        $filter = [
            'mobilephone'=>trim($data['username'])
        ];
        if($data['usertype'] === ConstMpVal::AUTH_TYPE_STUDENT){
            $stuModel = new StudentModel();
            if($info = $stuModel->getInfoByFilter($filter)){
                //校验学生密码
                if(password_verify($data['userpwd'], $info['student_password'])){
                    $str = new CaStringTool();
                    $tmp_token = $str->getRandomStr(ConstConfig::getTokenLen());
                    $loginLogic = new AuthLogic();
                    if ($loginLogic->setAuth($info['student_id'], $tmp_token, ConstMpVal::AUTH_TYPE_STUDENT, $info['school_id'])) {
                        return $this->success([
                            'auth_id' => $info['student_id'],
                            'auth_type' => ConstMpVal::AUTH_TYPE_STUDENT,
                            'auth_token' => $tmp_token,
                            'bind_wechat'=>(empty($info['wechat_openid'])),
                        ], '登录成功，请继续操作');
                    }
                    return $this->error(null, '请检查Redis是否启用');
                }
            }
        }else if($data['usertype'] === ConstMpVal::AUTH_TYPE_TEACHER){
            $teaModel = new TeacherModel();
            if($info = $teaModel->getInfoByFilter($filter)){
                //校验老师密码
                if(password_verify($data['userpwd'], $info['teacher_password'])){
                    $str = new CaStringTool();
                    $tmp_token = $str->getRandomStr(ConstConfig::getTokenLen());
                    $loginLogic = new AuthLogic();
                    if ($loginLogic->setAuth($info['teacher_id'], $tmp_token, ConstMpVal::AUTH_TYPE_TEACHER, $info['school_id'])) {
                        return $this->success([
                            'auth_id' => $info['teacher_id'],
                            'auth_type' => ConstMpVal::AUTH_TYPE_TEACHER,
                            'auth_token' => $tmp_token,
                            'bind_wechat'=>(empty($info['wechat_openid'])),
                        ], '登录成功，请继续操作');
                    }
                    return $this->error(null, '请检查Redis是否启用');
                }
            }
        }

        return $this->error(null, '帐号或密码错误！');
    }

    //微信登录（个人）
    public function personalWxlogin()
    {
        $param = [];
        $param['action'] = catp_input('post.action/s', ''); //操作（选项：wxlogin》微信登录；bindlogin》绑定登录；）

        if(!in_array($param['action'],['wxlogin','bindlogin'])) return $this->error(null,'操作类型错误！');

        //微信数据
        $param['js_code'] = catp_input('post.js_code/s', '');
        $param['raw_data'] = catp_input('post.raw_data/s', '',0,'urldecode');
        $param['signature'] = catp_input('post.signature/s', '');
        //绑定数据
        $param['auth_id'] = catp_input('post.auth_id/d', -1);
        $param['auth_type'] = catp_input('post.auth_type/d', -1);
        $param['auth_token'] = catp_input('post.auth_token/s', '',200);

        //数据安全检测
        $rule = [
            'js_code' => 'require',
            'raw_data' => 'require',
            'signature' => 'require',
        ];
        $message = [
            'js_code.require' => '请输入微信js_code字符串！',
            'raw_data.require' => '请输入微信rawData数据字符串！',
            'signature.require' => '请输入微信signature签名字符串！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        //绑定登录
        if('bindlogin' == $param['action']){
            if(-1 == $param['auth_id']) return $this->error(null,'身份编号错误！');
            if(!in_array($param['auth_type'],[ConstMpVal::AUTH_TYPE_STUDENT,ConstMpVal::AUTH_TYPE_TEACHER])) return $this->error(null,'身份类型错误！');
            if(strlen($param['auth_token'])<32) return $this->error(null,'身份Token错误！');
        }
        //微信验签，格式：sha1( rawData + sessionkey )
        $mp_client = new MinProgramClient();
        $respData = $mp_client->getCode2Session($param['js_code']);
        if(!empty($respData)){
            //存在openid键，表示成功
            if(array_key_exists('openid',$respData)){
                //微信数据验签
                if($param['signature'] !== sha1($param['raw_data'].$respData['session_key'])){
                    return $this->error(null,'微信数据未通过验签！');
                }

                if('wxlogin' == $param['action']){
                    //先筛查老师，再查学生
                    $teaModel = new TeacherModel();
                    if($info = $teaModel->getInfoByFilter(['wx_openid'=>$respData['openid']])){
                        $str = new CaStringTool();
                        $tmp_token = $str->getRandomStr(ConstConfig::getTokenLen());
                        $loginLogic = new AuthLogic();
                        if ($loginLogic->setAuth($info['teacher_id'], $tmp_token, ConstMpVal::AUTH_TYPE_TEACHER, $info['school_id'])) {
                            //更新微信信息（昵称、头像、性别）
                            $raw_data_json = json_decode($param['raw_data'],true);
                            $entity = new WechatInfoEntity();
                            $entity->setWechatName($raw_data_json['nickName']);
                            $entity->setWechatAvatar($raw_data_json['avatarUrl']);
                            $teaModel->setWechatInfo($info['teacher_id'],$info['school_id'],$entity);
                            //响应身份数据
                            return $this->success([
                                'auth_id' => $info['teacher_id'],
                                'auth_type' => ConstMpVal::AUTH_TYPE_TEACHER,
                                'auth_token' => $tmp_token
                            ], '登录成功，请继续操作');
                        }
                        return $this->error(null, '请检查Redis是否启用');
                    }
                    //筛查学生
                    $stuModel = new StudentModel();
                    if($info = $stuModel->getInfoByFilter(['wx_openid'=>$respData['openid']])){
                        $str = new CaStringTool();
                        $tmp_token = $str->getRandomStr(ConstConfig::getTokenLen());
                        $loginLogic = new AuthLogic();
                        if ($loginLogic->setAuth($info['student_id'], $tmp_token, ConstMpVal::AUTH_TYPE_STUDENT, $info['school_id'])) {
                            //更新微信信息（昵称、头像、性别）
                            $raw_data_json = json_decode($param['raw_data'],true);
                            $entity = new WechatInfoEntity();
                            $entity->setWechatName($raw_data_json['nickName']);
                            $entity->setWechatAvatar($raw_data_json['avatarUrl']);
                            $stuModel->setWechatInfo($info['student_id'],$info['school_id'],$entity);
                            //响应身份数据
                            return $this->success([
                                'auth_id' => $info['student_id'],
                                'auth_type' => ConstMpVal::AUTH_TYPE_STUDENT,
                                'auth_token' => $tmp_token
                            ], '登录成功，请继续操作');
                        }
                        return $this->error(null, '请检查Redis是否启用');
                    }
                    //未绑定该微信
                    return $this->error(null, '该微信未绑定帐号！请使用帐号与密码登录！');
                }else if('bindlogin' == $param['action']){
                    $authlogic = new AuthLogic();
                    if($authlogic->checkToken($param['auth_id'],$param['auth_token'],$param['auth_type'])){
                        //数据模型
                        $model = ((ConstMpVal::AUTH_TYPE_TEACHER == $param['auth_type'])?new TeacherModel():new StudentModel());
                        //过滤重复绑定
                        if($model->isHasBindOfWxopenid($respData['openid'])){
                            return $this->error(null,'该微信已绑其他帐号，请更换微信号！');
                        }
                        $raw_data_json = json_decode($param['raw_data'],true);
                        $entity = new WechatInfoEntity();
                        $entity->setWechatName($raw_data_json['nickName']);
                        $entity->setWechatAvatar($raw_data_json['avatarUrl']);
                        $entity->setWechatSex($raw_data_json['gender']);
                        $entity->setWechatOpenid($respData['openid']);
                        $entity->setWechatUnionid($respData['unionid']);
                        //身份数据
                        $school_id = $authlogic->getAuth($param['auth_id'],$param['auth_type'],$authlogic->getPropertySchoolId());
                        if($model->setWechatInfo($param['auth_id'],$school_id,$entity)){
                            return $this->success([
                                'auth_id' => $param['auth_id'],
                                'auth_type' => $param['auth_type'],
                                'auth_token' => $param['auth_token']
                            ], '登录成功，请继续操作');
                        }
                        return $this->success(null,'微信绑定失败！');
                    }
                    return $this->error(null, '身份信息已过期，请重新登录！');
                }
            }else if(array_key_exists('errcode',$respData)){
                if(0 !== intval($respData['errcode'])){
                    return $this->error(null,$respData['errmsg']);
                }else{
                    return $this->error(null,'微信响应数据异常！');
                }
            }
        }
        return $this->error(null,'微信通道异常,请检查！');
    }

    //自动登录（个人）
    public function personalAutoLogin()
    {
        $user_id = catp_input('post.' . ConstKey::KEY_USERID . '/d', 0);
        $user_token = catp_input('post.' . ConstKey::KEY_USERTOKEN . '/s', '', ConstConfig::getTokenLen());
        $user_type = catp_input('post.' . ConstKey::KEY_USERTYPE . '/d', -1);
        $logic = new AuthLogic();
        if ($logic->checkToken($user_id, $user_token, $user_type)) {
            $str = new CaStringTool();
            $tmp_token = $str->getRandomStr(ConstConfig::getTokenLen());
            $school_id = $logic->getAuth($user_id,$user_type,$logic->getPropertySchoolId());
            if ($logic->setAuth($user_id, $tmp_token, $user_type, $school_id)) {
                return $this->success([
                    'auth_id' => $user_id,
                    'auth_type' => $user_type,
                    'auth_token' => $tmp_token
                ], '登录成功，请继续操作');
            }
            return $this->error(null, '请检查Redis是否启用');
        }
        return $this->error();
    }

    //退出（个人）
    public function personalExit()
    {
        $user_id = catp_input('post.' . ConstKey::KEY_USERID . '/d', 0);
        $user_token = catp_input('post.' . ConstKey::KEY_USERTOKEN . '/s', '', ConstConfig::getTokenLen());
        $user_type = catp_input('post.' . ConstKey::KEY_USERTYPE . '/d', -1);
        $logic = new AuthLogic();
        if ($logic->checkToken($user_id, $user_token, $user_type)) {
            return ($logic->clearAuth($user_id,$user_type) ? $this->success(null, '已退出登录') : $this->error(null, '退出登录异常'));
        }
        return $this->success(null, '您未登录');
    }
}