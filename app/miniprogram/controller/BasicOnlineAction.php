<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\constval\ConstMpVal;
use think\exception\ValidateException;

//在线操作
class BasicOnlineAction extends BaseOnline
{
    //发送短信
    public function sendSms()
    {
        $param = [];
        $param['mobilephone'] = catp_input('post.mobilephone/d', -1,11);
        $param['action'] = catp_input('post.action/s', '',20);

        //数据安全检测
        $rule = [
            'mobilephone' => 'require|mobile|length:11',
            'action' => 'require|alpha|length:2,20',
        ];
        $message = [
            'mobilephone.require' => '请输入手机号码！',
            'mobilephone.mobile' => '手机号码格式错误！',
            'mobilephone.length' => '手机号码必须是11位数字！',
            'action.require' => '请输入功能标识！',
            'action.alpha' => '功能标识必须是纯字母！',
            'action.length' => '功能标识长度必须2至20位之间！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        if(!in_array($param['action'],[ConstMpVal::SMS_RESET_PWD])) return $this->error(null,'操作代码错误！');

        //逻辑安全检测
        if(parent::isTeacher()){
            //执行安全代码
        }else{
            //执行安全代码
        }

        //执行短信发送
        if(sms_send($param['mobilephone'], $param['action'])){
           return $this->success(null,'发送成功！');
        }
        return $this->error(null,'发送失败！');
    }
}