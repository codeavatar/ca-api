<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\model\BannerModel;
use app\miniprogram\model\CourseModel;
use app\miniprogram\model\ExpertModel;
use app\miniprogram\model\LessonModel;
use app\miniprogram\model\LivesModel;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\TeacherModel;
use app\miniprogram\model\TrainingCourseModel;
use app\miniprogram\model\TrainingLessonModel;
use app\miniprogram\model\UploadModel;
use base\entity\CaPaginationEntity;

class NavHome extends BaseOnline
{
    //首页
    public function index(){
        $data = [];
        return $this->success($data);
    }

    //学生列表
    public function studentList(){
        $page_entity = new CaPaginationEntity($this->caPageIndex,$this->caPageSize);
        $page_entity->setOrderby('xxx ASC,aaa DESC');
        $page_entity->setField('realname,avatar');
        $page_entity->setIsPagination(true);
        $page_entity->setWhere([
            ['is_show','=',1]
        ]);
        $model = new StudentModel();
        $page = $model->getPageList($page_entity);
        web_http_filter_array('avatar',$page['list']);
        return $this->success_list($page['list'], $this->caPageIndex, $page['total_pages'], $page['total_rows']);
    }

    //教师列表
    public function teacherList()
    {
        $param = [];
        $param['keyword'] = catp_input('post.keyword/s', '', 20);

        $model = new TeacherModel();
        $page = $model->getRelativeList($this->caUserId, $this->caPageIndex, $this->caPageSize,$param['keyword']);
        return $this->success_list($page['list'], $this->caPageIndex, $page['total_pages'], $page['total_rows']);
    }
}