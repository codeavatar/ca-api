<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\demo\validate\StudentValidate;
use app\miniprogram\entity\StudentEntity;
use app\miniprogram\entity\WebPersonalInfoEntity;
use app\miniprogram\entity\WebTeamEntity;
use app\miniprogram\entity\WebTeamInfoEntity;
use app\miniprogram\logic\WebPersonalLogic;
use app\miniprogram\logic\WebTeamLogic;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\WebPersonalInfoModel;
use app\miniprogram\model\WebTeamModel;
use app\miniprogram\validate\WebPersonalInfoValidate;
use app\miniprogram\validate\WebTeamInfoValidate;
use app\miniprogram\validate\WebTeamValidate;

class BasicRegister extends BaseOnline
{
    //认证（个人）
    public function personalRegister(){
        //参数
        $data = [];
        $data['personal_id'] = $this->caUserId;
        $data['avatar_url'] = catp_input('post.avatar_url/s', '',100);
        $data['email'] = catp_input('post.email/s', '',50);
        $data['realname'] = catp_input('post.realname/s', '',10);
        $data['identity_card'] = catp_input('post.identity_card/s', '',18);
        $data['sex'] = catp_input('post.sex/d', 0,1);
        $data['birthday'] = catp_input('post.birthday/s', '',10);
        $data['politics_id'] = catp_input('post.politics_id/d', 0,9);
        $data['nation_id'] = catp_input('post.nation_id/d', 0,9);
        $data['cellphone'] = catp_input('post.cellphone/s', '',11);
        $data['city_id'] = catp_input('post.city_id/d', 0,9);
        $data['live_address'] = catp_input('post.live_address/s', '',50);
        $data['qualification_id'] = catp_input('post.qualification_id/d', 0,9);
        $data['employment_id'] = catp_input('post.employment_id/d', 0,9);
        $data['service_ids'] = catp_input('post.service_ids/s', 0,50);
        $data['service_profession_id'] = catp_input('post.service_profession_id/d', 0,9);
        $data['personal_system_number'] = WebPersonalLogic::getUniqueId();

        //安全检测
        $validate = new StudentValidate();
        if (!$validate->check($data)) return $this->error(null,$validate->getError());

        //封装数据
        $entity = new StudentEntity();
        $entity->synchronizedData($data);

        //申请认证
        $model = new StudentModel();
        if($model->setInfoByEntity($entity)){
            return  $this->success(null,'申请成功，待认证！');
        }
        return $this->error(null,'申请失败！');
    }
}