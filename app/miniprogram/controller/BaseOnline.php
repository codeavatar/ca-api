<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\middleware\AuthCheck;
use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\logic\AuthLogic;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\TeacherModel;
use base\controller\CaBasicApi;
use constval\ConstKey;

class BaseOnline extends CaBasicApi
{
    /**
     * 控制器中间件
     * @var string[]
     */
    protected $middleware = [
        AuthCheck::class
    ];

    protected $caUserId;
    protected $caUserType;
    protected $caSchoolId;

    protected function initialize()
    {
        parent::initialize();
        $this->caUserId = catp_input('post.'.ConstKey::KEY_USERID.'/d',0);
        $this->caUserType = catp_input('post.'.ConstKey::KEY_USERTYPE.'/d',0);
        //缓存中的数据
        $authlogic = new AuthLogic();
        $this->caSchoolId = $authlogic->getAuth($this->caUserId,$this->caUserType,$authlogic->getPropertySchoolId());
    }

    //检测老师
    protected function isTeacher(){
        //缓存中的数据
        $authlogic = new AuthLogic();
        $tmpUserType = $authlogic->getAuth($this->caUserId,$this->caUserType,$authlogic->getPropertyType());
        return (intval($tmpUserType) === ConstMpVal::AUTH_TYPE_TEACHER);
    }

    //检测学生
    protected function isStudent(){
        //缓存中的数据
        $authlogic = new AuthLogic();
        $tmpUserType = $authlogic->getAuth($this->caUserId,$this->caUserType,$authlogic->getPropertyType());
        return (intval($tmpUserType) === ConstMpVal::AUTH_TYPE_STUDENT);
    }

    //角色标识
    protected function getAuthType(){
        //缓存中的数据
        $authlogic = new AuthLogic();
        return $authlogic->getAuth($this->caUserId,$this->caUserType,$authlogic->getPropertyType());
    }
}