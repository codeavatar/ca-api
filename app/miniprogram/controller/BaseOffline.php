<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\constval\ConstMpVal;
use base\controller\CaBasicApi;

class BaseOffline extends CaBasicApi
{
    //检测老师
    protected function isTeacher($usertype){
        return (intval($usertype) === ConstMpVal::AUTH_TYPE_TEACHER);
    }

    //检测学生
    protected function isStudent($usertype){
        return (intval($usertype) === ConstMpVal::AUTH_TYPE_STUDENT);
    }
}