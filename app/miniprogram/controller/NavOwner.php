<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;


use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\entity\StudentEntity;
use app\miniprogram\entity\WechatInfoEntity;
use app\miniprogram\logic\AuthLogic;
use app\miniprogram\model\ClassModel;
use app\miniprogram\model\CourseModel;
use app\miniprogram\model\ExpertModel;
use app\miniprogram\model\GzexpertModel;
use app\miniprogram\model\MessageModel;
use app\miniprogram\model\MsgReadlogModel;
use app\miniprogram\model\SchoolModel;
use app\miniprogram\model\StudentModel;
use app\miniprogram\model\TeacherModel;
use app\miniprogram\model\TrainingCourseModel;
use app\miniprogram\model\ZtlxstModel;
use base\entity\CaPaginationEntity;
use think\exception\ValidateException;
use third\wechat\minprogram\MinProgramClient;

class NavOwner extends BaseOnline
{
    //入口
    public function index()
    {
        $data = [
            'avatar' => '', 'realname' => '', 'remark' => '', 'noreadnum' => 0,'user_id'=>$this->caUserId
        ];
        return $this->success($data);
    }

    //联系我们
    public function contactus()
    {
        $platform = config('mpconfig.platform');
        $data = [
            'app_email' => $platform['app_email'],
            'app_phone' => $platform['app_phone'],
            'app_site' => $platform['app_site']
        ];
        return $this->success($data);
    }

    //关于我们
    public function about()
    {
        $platform = config('mpconfig.platform');
        $data = [
            'app_mp_version' => $this->caVersion,
            'app_site' => $platform['app_site'],
            'app_weburl' => [
                'app_operating_instruction' => BasicWap::operatingInstruction(),
                'app_law_description' => BasicWap::lawDescription(),
                'app_platform_agreement' => BasicWap::platformAgreement(),
            ]
        ];
        return $this->success($data);
    }

    //个人资料
    public function personalInfo()
    {
        $param = [];
        $param['action'] = catp_input('post.action/d', -1);
        //合理数据过滤
        if (!in_array($param['action'], [ConstMpVal::ACTION_GETINFO, ConstMpVal::ACTION_SETINFO])) return $this->error(null, '操作标识错误！');

        if ($param['action'] == ConstMpVal::ACTION_GETINFO) {
            if(ConstMpVal::AUTH_TYPE_STUDENT == $this->caUserType){
                $model = new StudentModel();
                if ($info = $model->getInfoByFilter(['student_id' => $this->caUserId])) {
                    $data = [
                        'cellphone'=>$info['student_mobile'],
                        'avatar_url' => web_http_filter($info['avatarurl']),
                        'realname' => $info['student_name'],
                        'student_sn' => $info['student_idcard'],
                        'at_school' => $info->school->school_name,
                        'school_signin' => '--年--月',
                        'link_address' => $info['student_address'],
                        'link_email' => $info['student_email'],
                    ];
                    return $this->success($data);
                }
            }else if(ConstMpVal::AUTH_TYPE_TEACHER == $this->caUserType){
                $model = new TeacherModel();
                if ($info = $model->getInfoByFilter(['teacher_id' => $this->caUserId])) {
                    $data = [
                        'cellphone'=>$info['teacher_mobile'],
                        'avatar_url' => web_http_filter($info['teacher_avatar']),
                        'realname' => $info['teacher_name'],
                        'student_sn' => $info['teacher_idcard'],
                        'at_school' => $info->school->school_name,
                        'school_signin' => '--年--月',
                        'link_address' => $info['teacher_address'],
                        'link_email' => $info['teacher_email'],
                    ];
                    return $this->success($data);
                }
            }

        } else if ($param['action'] == ConstMpVal::ACTION_SETINFO) {
            $param['student_address'] = catp_input('post.address/s', '', 50);
            $param['student_email'] = catp_input('post.email/s', '', 50);
            //数据安全检测
            $rule = [
                'student_address' => 'require|max:50',
                'student_email' => 'require|email|max:50'
            ];
            $message = [
                'student_address.require' => '请输入联系地址！',
                'student_address.max' => '联系地址长度必须小于50位！',
                'student_email.require' => '请输入邮箱地址！',
                'student_email.email' => '邮箱地址格式错误！',
                'student_email.max' => '邮箱地址长度必须小于50位！',
            ];

            try {
                $this->validate($param, $rule, $message);
            } catch (ValidateException $e) {
                return $this->error(null, $e->getError());
            }
            //过滤未使用键值对
            unset($param['action']);

            //封装数据
            $entity = new StudentEntity();
            $entity->setChannel(ConstMpVal::CHANNEL_STUDENT_PERSONAL_INFO);
            $entity->setStudentId($this->caUserId);
            $entity->synchronizedData($param);
            if(ConstMpVal::AUTH_TYPE_STUDENT == $this->caUserType) {
                $model = new StudentModel();
                if ($model->setInfoByEntity($entity)) {
                    return $this->success(null, '设置保存成功！');
                }
            }else if(ConstMpVal::AUTH_TYPE_STUDENT == $this->caUserType) {
                $model = new TeacherModel();
                if ($model->setInfoByEntity($entity)) {
                    return $this->success(null, '设置保存成功！');
                }
            }
            return $this->error(null, '设置保存失败！');
        }
        return $this->error('目标数据不存在或已删除！');
    }

    //收藏管理
    public function favoriteList()
    {
        $data = [];
        $data['tabIdx'] = catp_input('post.tabIdx/d', '');
        //数据检测
        if(!in_array($data['tabIdx'],[0,1])) return $this->error(null,'请求参数错误！');

        if(0 == $data['tabIdx']){
            $model = new CourseModel();
            $page = $model->getFavoriteList($this->caUserId, $this->caPageIndex, $this->caPageSize);
        }else if(1 == $data['tabIdx']){
            $model = new TrainingCourseModel();
            $page = $model->getFavoriteList($this->caUserId, $this->caPageIndex, $this->caPageSize);
        }
        return $this->success_list($page['list'], $this->caPageIndex, $page['total_pages'], $page['total_rows']);
    }

    //我的消息
    public function messageList()
    {
        $model = new MessageModel();
        $page_entity = new CaPaginationEntity($this->caPageIndex, $this->caPageSize);
        $page_entity->setField('msg_id,from_type as msg_type,msg_title,create_time,from_id');
        $page_entity->setOrderby('msg_id DESC');
        $page_entity->setIsPagination(true);
        $page = $model->getPageList($page_entity);
        return $this->success_list($page['list'], $this->caPageIndex, $page['total_pages'], $page['total_rows']);
    }

    //消息详情
    public function messageDetail()
    {
        $param = [];
        $param['msg_id'] = catp_input('post.msg_id/d', -1);
        //合理数据过滤
        if (-1 == $param['msg_id']) return $this->error(null, '参数错误！');
        $model = new MessageModel();
        if($info = $model->field('msg_id,msg_title,from_type as msg_type,msg_content,create_time')->where(['msg_id'=>$param['msg_id']])->find()){
//            $info['create_time'] = date('Y-m-d H:i:s', $info['create_time']);
            $info['msg_content'] = web_http_filter_richtext($info['msg_content']);
            //记录已读标记
            $model_msglog = new MsgReadlogModel();
            $model_msglog->setData($info['msg_id'],$this->caUserId,$this->caUserType);
            return $this->success($info);
        }
        return $this->error(null,'目标数据不存在或已删除！');
    }

    //更改密码
    public function changeUserpwd()
    {
        $param = [];
        $param['mobilephone'] = catp_input('post.mobilephone/d', -1,11);
        $param['validcode'] = catp_input('post.validcode/d', 0, 6);
        $param['userpwd'] = catp_input('post.userpwd/s', '', 20);

        //数据安全检测
        $rule = [
            'mobilephone' => 'require|mobile|length:11',
            'validcode' => 'require|number|max:6',
            'userpwd' => 'require|length:8,12'
        ];
        $message = [
            'mobilephone.require' => '请输入手机号码！',
            'mobilephone.mobile' => '手机号码格式错误！',
            'mobilephone.length' => '手机号码必须是11位数字！',
            'validcode.require' => '输入验证码！',
            'validcode.number' => '验证码必须是数字！',
            'validcode.max' => '验证码长度必须小于等于6位！',
            'userpwd.require' => '请输入新密码！',
            'userpwd.length' => '新密码长度必须8至12位之间！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        //过滤未使用键值对
        unset($param['action']);

        //老师
        if(parent::isTeacher()){
            $model = new TeacherModel();
            if(!$model->isChkMyMobilephone($this->caUserId,$param['mobilephone']))
                return $this->error(null,'您输入的手机号码错误！');
            //校验短信
            if(sms_chkcode($param['mobilephone'],$param['validcode'],ConstMpVal::SMS_RESET_PWD)){
                if($model->setResetPwd($this->caUserId,$this->caSchoolId,$param['userpwd'])){

                    return $this->success(null,'修改密码成功！');
                }
                return $this->success(null,'修改密码失败！');
            }
            return $this->error(null,'短信验证码错误！');
        }
        //学生
        $model = new StudentModel();

        if(!$model->isChkMyMobilephone($this->caUserId,$param['mobilephone']))
            return $this->error(null,'您输入的手机号码错误！');

        //校验短信
        if(sms_chkcode($param['mobilephone'],$param['validcode'],ConstMpVal::SMS_RESET_PWD)) {
            if ($model->setResetPwd($this->caUserId, $this->caSchoolId, $param['userpwd'])) {
                return $this->success(null, '修改密码成功！');
            }
            return $this->success(null, '修改密码失败！');
        }
        return $this->error(null,'短信验证码错误！');
    }

    //更换微信
    public function changeBindWechat()
    {
        $param = [];
        $param['js_code'] = catp_input('post.js_code/s', '');
        $param['raw_data'] = catp_input('post.raw_data/s', '',0,'urldecode');
        $param['signature'] = catp_input('post.signature/s', '');

        //数据安全检测
        $rule = [
            'js_code' => 'require',
            'raw_data' => 'require',
            'signature' => 'require',
        ];
        $message = [
            'js_code.require' => '请输入微信js_code字符串！',
            'raw_data.require' => '请输入微信rawData数据字符串！',
            'signature.require' => '请输入微信signature签名字符串！',
        ];

        try {
            $this->validate($param, $rule, $message);
        } catch (ValidateException $e) {
            return $this->error(null, $e->getError());
        }
        //微信验签，格式：sha1( rawData + sessionkey )
        $mp_client = new MinProgramClient();
        $respData = $mp_client->getCode2Session($param['js_code']);
        if(!empty($respData)){
            //存在openid键，表示成功
            if(array_key_exists('openid',$respData)){
                //微信数据验签
                if($param['signature'] !== sha1($param['raw_data'].$respData['session_key'])){
                    return $this->error(null,'微信数据未通过验签！');
                }
                //数据模型
                $model = (parent::isTeacher()?new TeacherModel():new StudentModel());
                //过滤重复绑定
                if($model->isHasBindOfWxopenid($respData['openid'])){
                    return $this->error(null,'该微信已绑其他帐号，请更换微信号！');
                }

                $raw_data_json = json_decode($param['raw_data'],true);
                $entity = new WechatInfoEntity();
                $entity->setWechatName($raw_data_json['nickName']);
                $entity->setWechatAvatar($raw_data_json['avatarUrl']);
                $entity->setWechatSex($raw_data_json['gender']);
                $entity->setWechatOpenid($respData['openid']);
                $entity->setWechatUnionid($respData['unionid']);
                if($model->setWechatInfo($this->caUserId,$this->caSchoolId,$entity)){
                    return $this->success(null,'更换微信绑定成功！');
                }
                return $this->success(null,'更换微信绑定失败！');
            }else if(array_key_exists('errcode',$respData)){
                if(0 !== intval($respData['errcode'])){
                    return $this->error(null,$respData['errmsg']);
                }else{
                    return $this->error(null,'微信响应数据异常！');
                }
            }
        }
        return $this->error(null,'微信通道异常,请检查！');
    }

    //安全退出
    public function appExit()
    {
        $logic = new AuthLogic();
        $logic->clearAuth($this->caUserId,$this->caUserType);
        return $this->success(null,'安全退出成功！');
    }
}