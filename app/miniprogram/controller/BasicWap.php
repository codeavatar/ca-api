<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\controller;

use app\BaseController;
use app\miniprogram\constval\ConstMpDbKey;

class BasicWap extends BaseController
{
    //功能介绍
    public static function operatingInstruction(){
        return self::urlBuilder('template_agreement',['keyname'=>ConstMpDbKey::CONFIG_WAP_INSTRUCTION]);
    }

    //法律声明
    public static function lawDescription(){
        return self::urlBuilder('template_agreement',['keyname'=>ConstMpDbKey::CONFIG_WAP_LAW]);
    }

    //平台协议
    public static function platformAgreement(){
        return self::urlBuilder('template_agreement',['keyname'=>ConstMpDbKey::CONFIG_WAP_PLATFORM]);
    }

    ////////////
    /// 私有函数
    ////////////

    //生成网址
    private static function urlBuilder($action_name,$vars=[]){
        return url("wapsite/WapPage/{$action_name}",$vars,true,true)->build();
    }
}