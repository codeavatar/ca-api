<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\model;


use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\entity\StudentEntity;
use app\miniprogram\entity\WechatInfoEntity;
use base\model\CaBasicModel;

class StudentModel extends CaBasicModel
{
    protected $name = 'student';
    protected $pk = 'id';

    //+---------------------------------------
    //+ 关联方法
    //+---------------------------------------

    public function school()
    {
        return $this->hasOne(SchoolModel::class, 'school_id', 'school_id');
    }

    //+---------------------------------------
    //+ 操作方法
    //+---------------------------------------

    //读取数据
    public function getDataById($id,$fields='*'){
        return $this->field($fields)->find(intval($id));
    }

    //读取数据
    public function getInfoByFilter($filter){
        $query = $this->with(['school'=>function($query){
            $query->field('school_id,school_name');
        }])->field('*');

        if(!empty($filter['mobilephone']))
            $query->where('student_mobile=:student_mobile',['student_mobile'=>[$filter['mobilephone'],\PDO::PARAM_STR]]);

        if(!empty($filter['student_id']))
            $query->where('student_id=:student_id',['student_id'=>[$filter['student_id'],\PDO::PARAM_INT]]);

        if(!empty($filter['wx_openid']))
            $query->where('wechat_openid=:wechat_openid',['wechat_openid'=>[$filter['wx_openid'],\PDO::PARAM_STR]]);

        return $query->fetchSql(false)->find();
    }
    //设置信息
    public function setInfoByEntity(StudentEntity $entity){
        $model = $this->find($entity->getStudentId());
        if($entity->getChannel() === ConstMpVal::CHANNEL_STUDENT_PERSONAL_INFO){
            $model->student_address = $entity->getStudentAddress();
            $model->student_email = $entity->getStudentEmail();
        }
        return $model->save();
    }
    //重置密码
    public function setResetPwd($user_id, $school_id, $userpwd)
    {
        $model = $this->where('student_id=:student_id AND school_id=:school_id',
            ['student_id' => [$user_id, \PDO::PARAM_INT], 'school_id' => [$school_id, \PDO::PARAM_INT]])->find();
        $model->student_password = password_hash($userpwd,PASSWORD_DEFAULT);
        return $model->save();
    }

    //绑定微信
    public function setWechatInfo($user_id, $school_id, WechatInfoEntity $entity)
    {
        $model = $this->where('student_id=:student_id AND school_id=:school_id',
            ['student_id' => [$user_id, \PDO::PARAM_INT], 'school_id' => [$school_id, \PDO::PARAM_INT]])->find();
        if(!empty($entity->getWechatName()))
            $model->wechat_name = $entity->getWechatName();

        if(!empty($entity->getWechatAvatar()))
            $model->wechat_headimg = $entity->getWechatAvatar();

        if(!empty($entity->getWechatOpenid()))
            $model->wechat_openid = $entity->getWechatOpenid();

        if(!empty($entity->getWechatUnionid()))
            $model->wechat_unionid = $entity->getWechatUnionid();

        return $model->save();
    }
    //检测该openId是否已绑定了
    public function isHasBindOfWxopenid($openid){
        return ($this->where('wechat_openid=:wechat_openid',['wechat_openid' => [$openid, \PDO::PARAM_STR]])->count()>0);
    }
}