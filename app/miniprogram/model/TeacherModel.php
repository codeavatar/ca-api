<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\model;


use app\miniprogram\constval\ConstMpVal;
use app\miniprogram\entity\StudentEntity;
use app\miniprogram\entity\WechatInfoEntity;
use base\model\CaBasicModel;
use think\facade\Db;

class TeacherModel extends CaBasicModel
{
    protected $name = 'teacher';
    protected $pk = 'id';

    //+---------------------------------------
    //+ 关联方法
    //+---------------------------------------

    public function school()
    {
        return $this->hasOne(SchoolModel::class, 'school_id', 'school_id');
    }

    //+---------------------------------------
    //+ 操作方法
    //+---------------------------------------

    //关联数据列表
    public function getRelativeList($student_id, $page_index, $page_size, $keyword)
    {
        $query = Db::name($this->name)->alias('main')->field('main.id as paper_id,exam.id,exam.name as title,exam.kao_time as begintime,exam.jie_time as endtime,main.fenshu as score')
            ->join('exam exam', 'exam.id=main.exam_id', 'left')
            ->where('main.student_id=:student_id', ['student_id' => [$student_id, \PDO::PARAM_INT]]);
        if (!empty($keyword)) $query->where([['exam.name', 'LIKE', '%' . ca_sqlinject_filter($keyword) . '%']]);
        $query->order('exam.id DESC');
        //分页数据
        $totalRows = $query->count();
        $totalPages = ($totalRows%$page_size > 0 ? intval($totalRows/$page_size)+1 : intval($totalRows/$page_size));
        $collection = $query->page($page_index,$page_size)->select();
        //处理数据
        $tmp_list = ($collection->isEmpty()) ? [] : $collection->toArray();
        return [
            'list'=>$tmp_list,
            'total_rows'=>$totalRows,
            'total_pages'=>$totalPages
        ];
    }

    //设置信息
    public function setInfoByEntity(StudentEntity $entity){
        $model = $this->find($entity->getStudentId());
        if($entity->getChannel() === ConstMpVal::CHANNEL_STUDENT_PERSONAL_INFO){
            $model->teacher_address = $entity->getStudentAddress();
            $model->teacher_email = $entity->getStudentEmail();
        }
        return $model->save();
    }

    //读取数据
    public function getDataById($id,$fields='*'){
        return $this->field($fields)->find(intval($id));
    }

    //读取数据
    public function getInfoById($teacher_id, $fields = '*')
    {
        return $this->field($fields)->where('teacher_id=:teacher_id', ['teacher_id' => [$teacher_id, \PDO::PARAM_INT]])->find();
    }

    //读取数据
    public function getInfoByFilter($filter,$fields='*')
    {
        $query = $this->with(['school'=>function($query){
            $query->field('school_id,school_name');
        }])->field($fields);

        if (!empty($filter['mobilephone']))
            $query->where('teacher_mobile=:teacher_mobile', ['teacher_mobile' => [$filter['mobilephone'], \PDO::PARAM_STR]]);

        if (!empty($filter['teacher_id']))
            $query->where(['teacher_id' => intval($filter['teacher_id'])]);

        if(!empty($filter['wx_openid']))
            $query->where('wechat_openid=:wechat_openid',['wechat_openid'=>[$filter['wx_openid'],\PDO::PARAM_STR]]);

        return $query->fetchSql(false)->find();
    }

    //重置密码
    public function setResetPwd($user_id, $school_id, $userpwd)
    {
        $model = $this->where('teacher_id=:teacher_id AND school_id=:school_id',
            ['teacher_id' => [$user_id, \PDO::PARAM_INT], 'school_id' => [$school_id, \PDO::PARAM_INT]])->find();
        $model->teacher_password = password_hash($userpwd,PASSWORD_DEFAULT);
        return $model->save();
    }

    //验证码手机号
    public function isChkMyMobilephone($user_id, $mobilephone){
        if($model = $this->where('teacher_id=:teacher_id', ['teacher_id' => [$user_id, \PDO::PARAM_INT]])->find()){
            return (trim($model->teacher_mobile)===$mobilephone);
        }
        return false;
    }

    //检测手机号是否存在
    public function isExistsOfMobilephone($mobilephone){
        return ($model = $this->where('teacher_mobile=:teacher_mobile',
            ['teacher_mobile' => [$mobilephone, \PDO::PARAM_STR]])->count() > 0);
    }

    //绑定微信
    public function setWechatInfo($user_id, $school_id, WechatInfoEntity $entity)
    {
        $model = $this->where('teacher_id=:teacher_id AND school_id=:school_id',
            ['teacher_id' => [$user_id, \PDO::PARAM_INT], 'school_id' => [$school_id, \PDO::PARAM_INT]])->find();
        if(!empty($entity->getWechatName()))
            $model->wechat_name = $entity->getWechatName();

        if(!empty($entity->getWechatAvatar()))
            $model->wechat_headimg = $entity->getWechatAvatar();

        if(!empty($entity->getWechatOpenid()))
            $model->wechat_openid = $entity->getWechatOpenid();

        if(!empty($entity->getWechatUnionid()))
            $model->wechat_unionid = $entity->getWechatUnionid();
        return $model->save();
    }

    //检测该openId是否已绑定了
    public function isHasBindOfWxopenid($openid){
        return ($this->where('wechat_openid=:wechat_openid',['wechat_openid' => [$openid, \PDO::PARAM_STR]])->count()>0);
    }

    //读取多个老师编号
    public function getTeacherIdsByClassId($class_id)
    {
        return $this->whereRaw('FIND_IN_SET('.intval($class_id).',class_ids)')->value('teacher_id');
    }

    //读取老师列表（使用多个老师ID）
    public function getTeacherListByIds($teacher_ids, $fields = '*')
    {
        return $this->field($fields)->whereIn('teacher_id',$teacher_ids)->select();
    }
}