<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\demo\validate;


use base\validate\CaBasicValidate;

/**
 * 内置规则说明：https://www.kancloud.cn/manual/thinkphp6_0/1037629
 *
 * Class PersonalInfoValidate
 * @package app\miniprogram\validate
 */
class StudentValidate extends CaBasicValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'personal_id' => 'require|integer|unique:WebPersonalInfo',
        'personal_system_number' => 'require|number|max:20|unique:WebPersonalInfo',
        'realname' => 'require|chs|max:10',
        'sex' => 'require|integer|max:1',
        'avatar_url' => 'require|url|max:200',
        'identity_card' => 'require|idCard',
        'birthday' => 'require|date|max:10',
        'cellphone' => 'require|mobile|max:11',
        'email' => 'require|email|max:50',
        'city_id' => 'require|integer',
        'live_address' => 'require|chsAlphaNum|max:50',
        'politics_id' => 'require|integer',
        'nation_id' => 'require|integer',
        'qualification_id' => 'require|integer',
        'employment_id' => 'require|integer',
        'service_id' => 'require|requireCallback:service_check_require|max:50',
        'service_profession_id' => 'require|integer',
        'remark' => 'max:200'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'personal_id.require' => '请输入个人编号！',
        'personal_id.integer' => '个人编号错误！',
        'personal_id.unique' => '该个人编号已存在！',
        'personal_system_number.require' => '请输入唯一编号！',
        'personal_system_number.number' => '唯一编号必须是纯数字！',
        'personal_system_number.max' => '唯一编号长度最大28位数字！',
        'personal_system_number.unique' => '唯一编号已存在！',
        'realname.require' => '请输入真实姓名！',
        'realname.chs' => '真实姓名必须是汉字！',
        'realname.max' => '真实姓名长度最大10位汉字！',
        'sex.require' => '请选择性别！',
        'sex.integer' => '性别必须是整数！',
        'sex.max' => '性别长度最大1位整数！',
        'avatar_url.require' => '请选择头像！',
        'avatar_url.url' => '头像必须是有效URL地址！',
        'avatar_url.max' => '头像地址长度最大200位有效URL地址！',
        'identity_card.require' => '请输入身份证号码！',
        'identity_card.idCard' => '身份证号码必须是有效身份证！',
        'birthday.require' => '请选择出生日期！',
        'birthday.date' => '出生日期必须是有效的日期！',
        'birthday.max' => '出生日期长度最大100位有效日期！',
        'cellphone.require' => '请输入手机号！',
        'cellphone.mobile' => '手机号必须是11位有效的手机号！',
        'cellphone.max' => '手机号长度最大11位有效数字！',
        'email.require' => '请输入邮箱！',
        'email.email' => '邮箱必须是有效的email地址！',
        'email.max' => '邮箱长度最大50位有效字符！',
        'city_id.require' => '请选择居住区域！',
        'city_id.integer' => '居住区域必须是整数！',
        'live_address.require' => '请输入家庭住址！',
        'live_address.chsAlphaNum' => '家庭住址必须是汉字、字母和数字！',
        'live_address.max' => '家庭住址长度最大50位有效字符！',
        'politics_id.require' => '请选择政治面貌！',
        'politics_id.integer' => '政治面貌必须是整数！',
        'nation_id.require' => '请选择民族！',
        'nation_id.integer' => '民族必须是整数！',
        'qualification_id.require' => '请选择最高学历！',
        'qualification_id.integer' => '最高学历必须是整数！',
        'employment_id.require' => '请选择从业状况！',
        'employment_id.integer' => '从业状况必须是整数！',
        'service_id.require' => '请选择服务类别！',
        'service_id.requireCallback' => '最多选择4个服务类别！',
        'service_id.max' => '服务类别长度最大50位有效字符！',
        'service_profession_id.require' => '请选择服务领域或行业！',
        'service_profession_id.integer' => '服务领域或行业必须是整数！',
        'remark.max' => '备注长度最大200位有效字符！',
    ];

    /**
     * 定义场景信息
     * 格式：'场景名'    =>    【'字段名1','字段名2','字段名3'】
     *
     * @var array
     */
    protected $scene = [];

    /**
     * 服务类别检测方法
     *
     * @param $value 当前字段的值
     * @param $data 所有的数据
     */
    public function service_check_require($value, $data){
        $values = explode(',',$value);
        return (count($values) > 4);
    }
}