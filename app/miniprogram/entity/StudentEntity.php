<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\entity;


use base\entity\CaBasicEntity;

class StudentEntity extends CaBasicEntity
{
    private $channel = -1;
    private $student_id = 0;
    private $student_address = '';
    private $student_email = '';

    public function __construct()
    {
    }

    ////////////
    /// 自动生成
    ////////////

    /**
     * @return int
     */
    public function getChannel(): int
    {
        return $this->channel;
    }

    /**
     * @param int $channel
     */
    public function setChannel(int $channel): void
    {
        $this->channel = $channel;
    }

    /**
     * @return int
     */
    public function getStudentId(): int
    {
        return $this->student_id;
    }

    /**
     * @param int $student_id
     */
    public function setStudentId(int $student_id): void
    {
        $this->student_id = $student_id;
    }

    /**
     * @return string
     */
    public function getStudentAddress(): string
    {
        return $this->student_address;
    }

    /**
     * @param string $student_address
     */
    public function setStudentAddress(string $student_address): void
    {
        $this->student_address = $student_address;
    }

    /**
     * @return string
     */
    public function getStudentEmail(): string
    {
        return $this->student_email;
    }

    /**
     * @param string $student_email
     */
    public function setStudentEmail(string $student_email): void
    {
        $this->student_email = $student_email;
    }



}