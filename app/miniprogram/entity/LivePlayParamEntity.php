<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\entity;


use base\entity\CaBasicEntity;

class LivePlayParamEntity  extends CaBasicEntity
{
    public $classid='';
    public $session_id='';
    public $token='';
    public $dsp=1;
    public $disable_ppt_animate=1;

    /**
     * @return string
     */
    public function getClassid(): string
    {
        return $this->classid;
    }

    /**
     * @param string $classid
     */
    public function setClassid(string $classid): void
    {
        $this->classid = $classid;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->session_id;
    }

    /**
     * @param string $session_id
     */
    public function setSessionId(string $session_id): void
    {
        $this->session_id = $session_id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getDsp(): int
    {
        return $this->dsp;
    }

    /**
     * @param int $dsp
     */
    public function setDsp(int $dsp): void
    {
        $this->dsp = $dsp;
    }

    /**
     * @return int
     */
    public function getDisablePptAnimate(): int
    {
        return $this->disable_ppt_animate;
    }

    /**
     * @param int $disable_ppt_animate
     */
    public function setDisablePptAnimate(int $disable_ppt_animate): void
    {
        $this->disable_ppt_animate = $disable_ppt_animate;
    }


}