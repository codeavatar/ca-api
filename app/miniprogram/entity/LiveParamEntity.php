<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\entity;


use base\entity\CaBasicEntity;

class LiveParamEntity  extends CaBasicEntity
{
    private $room_id='';
    private $user_number=0;
    private $user_name='';
    private $user_role=0; //0:学生 1:老师 2:管理员
    private $user_avatar='';
    private $group_id=0;
    //来自百家云帐号
    private $partner_key='';

    /**
     * 自动生成
     */

    /**
     * @return string
     */
    public function getRoomId(): string
    {
        return $this->room_id;
    }

    /**
     * @param string $room_id
     */
    public function setRoomId(string $room_id): void
    {
        $this->room_id = $room_id;
    }

    /**
     * @return int
     */
    public function getUserNumber(): int
    {
        return $this->user_number;
    }

    /**
     * @param int $user_number
     */
    public function setUserNumber(int $user_number): void
    {
        $this->user_number = $user_number;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->user_name;
    }

    /**
     * @param string $user_name
     */
    public function setUserName(string $user_name): void
    {
        $this->user_name = $user_name;
    }

    /**
     * @return int
     */
    public function getUserRole(): int
    {
        return $this->user_role;
    }

    /**
     * @param int $user_role
     */
    public function setUserRole(int $user_role): void
    {
        $this->user_role = $user_role;
    }

    /**
     * @return string
     */
    public function getUserAvatar(): string
    {
        return $this->user_avatar;
    }

    /**
     * @param string $user_avatar
     */
    public function setUserAvatar(string $user_avatar): void
    {
        $this->user_avatar = $user_avatar;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId(int $group_id): void
    {
        $this->group_id = $group_id;
    }

    /**
     * @return string
     */
    public function getPartnerKey(): string
    {
        return $this->partner_key;
    }

    /**
     * @param string $partner_key
     */
    public function setPartnerKey(string $partner_key): void
    {
        $this->partner_key = $partner_key;
    }
}