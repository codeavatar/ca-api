<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\entity;


class WechatInfoEntity
{
    private $wechat_name='';
    private $wechat_sex=0;//微信性别：0》未知；1》男；2》女；
    private $wechat_avatar='';
    private $wechat_openid='';
    private $wechat_unionid='';

    public function __construct()
    {
    }

    ///
    /// 自动生成
    ///

    /**
     * @return string
     */
    public function getWechatName(): string
    {
        return $this->wechat_name;
    }

    /**
     * @param string $wechat_name
     */
    public function setWechatName(string $wechat_name): void
    {
        $this->wechat_name = $wechat_name;
    }

    /**
     * @return int
     */
    public function getWechatSex(): int
    {
        return $this->wechat_sex;
    }

    /**
     * @param int $wechat_sex
     */
    public function setWechatSex(int $wechat_sex): void
    {
        $this->wechat_sex = $wechat_sex;
    }

    /**
     * @return string
     */
    public function getWechatAvatar(): string
    {
        return $this->wechat_avatar;
    }

    /**
     * @param string $wechat_avatar
     */
    public function setWechatAvatar(string $wechat_avatar): void
    {
        $this->wechat_avatar = $wechat_avatar;
    }

    /**
     * @return string
     */
    public function getWechatOpenid(): string
    {
        return $this->wechat_openid;
    }

    /**
     * @param string $wechat_openid
     */
    public function setWechatOpenid(string $wechat_openid): void
    {
        $this->wechat_openid = $wechat_openid;
    }

    /**
     * @return string
     */
    public function getWechatUnionid(): string
    {
        return $this->wechat_unionid;
    }

    /**
     * @param string $wechat_unionid
     */
    public function setWechatUnionid(string $wechat_unionid): void
    {
        $this->wechat_unionid = $wechat_unionid;
    }

}