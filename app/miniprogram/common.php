<?php
// 这是系统自动生成的公共文件
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/
//发送短信（固定内容）
use constval\ConstCode;
use third\sms\SmsClient;

//时间格式转化
function time_formater($seconds){
    if($seconds < 1) return '--';
    if($seconds < 60) return $seconds.'秒';

    $hour = intval($seconds/3600);
    $minute = intval(($seconds%3600)/60);
    return ($hour>0?$hour.'小时':'').($minute>0?$minute.'分钟':'');
}

//富文本框图片地址补全
function web_http_filter_richtext($richtext){
    if(empty($richtext) || (!is_string($richtext))) return '';

    if(false !== strpos($richtext,'<img src=')){
        return str_replace('src="/uploads/','src="'.config('mpconfig.attachment_http').'/uploads/',$richtext);
    }
    return $richtext;
}
//补全网址
function web_http_filter($path){
    if(empty($path) || (!is_string($path))) return config('mpconfig.attachment_http');

    if(\think\helper\Str::startsWith($path,'http')){
        return $path;
    }
    return config('mpconfig.attachment_http').$path;
}
//批量处理网址
function web_http_filter_array($column,&$data_array2){
    if(!is_array($data_array2)) return;

    foreach ($data_array2 as &$item){
        $item[$column] = web_http_filter($item[$column]);
    }
}

//数字转中文
function num_to_zhformat($num){
    if($num > 100000000){//亿
        $num = round($num/100000000,2).'亿';
    }else if($num > 10000){//万
        $num = round($num/10000,2).'万';
    }
    return $num;
}

function sms_send($mobilephone, $action = '')
{
    $init = new SmsClient();
    $entity = new SmsEntity();
    $entity = $init->send($mobilephone, $init->getCode($mobilephone.$action));
    return (ConstCode::CODE_SUCCESS == $entity->getCode());
}

//发送短信（自定义内容）
function sms_send_msg($mobilephone, $sms_msg, $action = '')
{
    return true;
}

//验证短信码
function sms_chkcode($mobilephone, $code, $action = '')
{
    $init = new SmsClient();
    return $init->checkCode($mobilephone.$action,$code);
}

//一维数组转换
function jsonarray_to_st1array($json_array){
    if(is_object($json_array)){
        $json_array = (array) $json_array;
    }

    if(is_array($json_array)){
        $tmp_array = [];
        foreach ($json_array as $key=>$val){
            $tmp_array[] = $val;
        }
        return $tmp_array;
    }
    return $json_array;
}

//过滤null
function null_filter($val){
    if(is_array($val)){
        //一维数组
        if(count($val)==count($val,1)){
            $keys = array_keys($val);
            foreach ($keys as $key){
                if(is_null($val[$key])){
                    $val[$key] = '';
                }
            }
        }
        return $val;
    }
    return empty($val)?'':$val;
}