<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/
declare (strict_types = 1);
namespace app\miniprogram\logic;


use think\facade\Cache;

class AuthLogic
{
    private $prefix = 'mp_auth_';

    public function setAuth($id, $token, $type, $school_id)
    {
        Cache::store('redis')->delete(self::getKey($id,$type));
        $hash = [
            $this->prefix.'id'=>$id,
            $this->prefix.'token'=>$token,
            $this->prefix.'type'=>$type,
            $this->prefix.'school_id'=>$school_id
        ];
        return Cache::store('redis')->set(self::getKey($id,$type), $hash, config('apiconfig.ca_limit')['token_timeout']);
    }

    public function checkToken($id, $token, $type){
        $hash = Cache::store('redis')->get(self::getKey($id,$type),[]);
        if(empty($hash)) return false;
        return (md5($hash[$this->prefix.'token']) === md5($token));
    }

    public function getAuth($id, $type, $key){
        $hash = Cache::store('redis')->get(self::getKey($id, $type));
        return $hash[$this->prefix.$key];
    }

    public function clearAuth($id, $type){
        return Cache::store('redis')->delete(self::getKey($id,$type));
    }

    ////////////
    /// 私有函数
    ////////////

    private function getKey($uid, $type){
        return $this->prefix.$type.'_'.$uid;
    }

    //属性名称
    public function getPropertyId(){
        return 'id';
    }
    public function getPropertySchoolId(){
        return 'school_id';
    }
    public function getPropertyToken(){
        return 'token';
    }
    public function getPropertyType(){
        return 'type';
    }
}