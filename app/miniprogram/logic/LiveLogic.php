<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\logic;


use app\miniprogram\entity\LiveParamEntity;
use app\miniprogram\entity\LivePlayParamEntity;
use app\miniprogram\entity\LivePlayTokenParamEntity;
use toolkit\entity\CaCurlEntity;
use toolkit\tool\CaCurlTool;

//官网文档： https://dev.baijiayun.com/wiki/detail/6

class LiveLogic
{
    private $live_partner_id = '';
    private $live_partner_key = '';
    private $live_root = '';
    //跳转URL
    private $live_url = '/web/room/enter';
    private $live_play_url = '/web/playback/index';
    //API地址
    private $api_live_token = '/openapi/playback/getPlayerToken';


    public function __construct()
    {
        $this->live_partner_id = config('mpconfig.live_partner_id');
        $this->live_partner_key = config('mpconfig.live_partner_key');
        //API地址组合
        $this->live_root = config('mpconfig.live_root_http');
        $this->live_url = $this->live_root.$this->live_url;
        $this->live_play_url = $this->live_root.$this->live_play_url;
        $this->api_live_token = $this->live_root.$this->api_live_token;
    }

    //进入直播间
    public function getLiveEnterLink(LiveParamEntity $entity){
        $param = [
            'room_id'=>$entity->getRoomId(),
            'user_number'=>$entity->getUserNumber(),
            'user_name'=>$entity->getUserName(),
            'user_role'=>$entity->getUserRole(),
            'user_avatar'=>$entity->getUserAvatar()
        ];
        if($entity->getGroupId() > 0){
            $param['group_id'] = $entity->getGroupId();
        }
        $param['sign']=$this->getSign($param);
        return $this->live_url.'?'.http_build_query($param);
    }

    //进入直播回放
    public function getLivePlayLink(LivePlayParamEntity $entity){
        $entity_token = new LivePlayTokenParamEntity();
        $entity_token->setPartnerId($this->live_partner_id);
        $entity_token->setRoomId($entity->getClassid());
        if($token_info = $this->getLivePlayToken($entity_token)){
//            $entity->setSessionId($token_info['video_id']);
            $entity->setToken($token_info['token']);
            $param = $entity->toArray();
            unset($param['session_id']);
            return $this->live_play_url.'?'.http_build_query($param);
        }
        return null;
    }

    //生成直播回放Token
    public function getLivePlayToken(LivePlayTokenParamEntity $entity){
        $entity->setExpiresIn(3600*3);
        $entity->setTimestamp(time());
        $param = $entity->toArray();
        //暂时删除
        unset($param['session_id']);
        $param['sign']=$this->getSign($param);
        $curl = new CaCurlTool();
        //返回LibCurlModel的数据模型
        $responseData  = new CaCurlEntity();//用于提示
        $responseData = $curl->curl($this->api_live_token, $param);
        $info = json_decode($responseData->getBody(),true);
        if($info['code']==0){
            return [
                'video_id'=>$info['data']['video_id'],
                'token'=>$info['data']['token'],
            ];
        }
        return null;
    }

    /**
     * 生成签名参数
     *
     * @param array $params 请求的参数
     * @param string $partner_key
     * @return string 生成的签名
     */
    public function getSign($params, $partner_key=null) {
        if(empty($partner_key)) $partner_key = $this->live_partner_key;

        ksort($params);//将参数按key进行排序
        $str = '';
        foreach ($params as $k => $val) {
            $str .= "{$k}={$val}&"; //拼接成 key1=value1&key2=value2&...&keyN=valueN& 的形式
        }
        $str .= "partner_key=" . $partner_key; //结尾再拼上 partner_key=$partner_key
        $sign = md5($str); //计算md5值
        return $sign;
    }
}