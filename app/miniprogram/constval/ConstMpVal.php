<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\constval;


class ConstMpVal
{
    //身份类型
    const AUTH_TYPE_STUDENT = 1;
    const AUTH_TYPE_TEACHER = 2;
    //短信标识
    const SMS_RESET_PWD = 'reset';
    const SMS_FIND_PWD = 'find';
}