<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\miniprogram\constval;


class ConstMpDbKey
{
    //Wap网页键名
    const CONFIG_WAP_INSTRUCTION = 'wap_instruction';
    const CONFIG_WAP_LAW = 'wap_law_description';
    const CONFIG_WAP_PLATFORM = 'wap_platform_agreement';
}