<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\index\controller;


use app\BaseController;

/**
 * 异部通知
 *
 * Class Notify
 * @package app\index\controller
 */
class Notify extends BaseController
{
    public function alipay(){
        return '支付宝异部通知';
    }

    public function wechatpay(){
        return '微信异部通知';
    }
}