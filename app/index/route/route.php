<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

use think\facade\Route;

//Route::post('callback/alipay','Notify/alipay')->ext('do');
//Route::post('callback/wxpay','Notify/wechatpay')->ext('do');
//================与下面等效============
Route::group('callback',function (){
    Route::post('alipay','Notify/alipay');
    Route::post('wxpay','Notify/wechatpay');
})->ext('do');



