<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\controller;

class Error
{
    public function __call($method, $args)
    {
        $controller = app('request')->controller();
//        $action = app('request')->action();
        if(strtolower($controller) != 'public'){
            return \redirect(url('index/index'));
        }
    }
}