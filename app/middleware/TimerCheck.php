<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types=1);

namespace app\middleware;


use constval\ConstCode;
use constval\ConstRedis;
use entity\CaApiEntity;
use think\facade\Cache;

class TimerCheck
{
    private $msg = [
        ConstCode::CODE_ERROR_INVALIDATED_IP => ConstCode::MESSAGE_ERROR_INVALIDATED_IP,
        ConstCode::CODE_ERROR_INVALIDATED_TOKEN_LENGTH => ConstCode::MESSAGE_ERROR_INVALIDATED_TOKEN_LENGTH,
        ConstCode::CODE_ERROR_INVALIDATED_TOKEN => ConstCode::MESSAGE_ERROR_INVALIDATED_TOKEN
    ];

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        if ($chkCode = self::chkauth($request)) {
            if ($chkCode !== 1000) {
                return CaApiEntity::jsondata_auto(CaApiEntity::data($chkCode, $this->msg[$chkCode]));
            }
            return $next($request);
        }
    }

    /**
     * 处理结束
     * (注意，在end方法里面不能有任何的响应输出。因为回调触发的时候请求响应输出已经完成了。)
     * @param \think\Response $response
     */
    public function end(\think\Response $response)
    {
        //
    }

    //检测身份
    private function chkauth($request)
    {
        //访问者的IP地址
        $accessIp = ca_ip();

        //检测IP黑名单
        $rejectIps = Cache::store('redis')->get(self::reject_ips_key());
        if (null != $rejectIps) {
            $rejectIpsArray = explode('|', $rejectIps);
            if (in_array($accessIp, $rejectIpsArray)) {
                return ConstCode::CODE_ERROR_INVALIDATED_IP;
            }
        }
        //检测IP白名单
        $ipsArray = config('apiconfig.ca_timer')['allow_ips'];
        if (!in_array($accessIp, $ipsArray)) {
            return ConstCode::CODE_ERROR_INVALIDATED_IP;
        }
        //检测Token
        $gToken = $request->get('token/s', '');
        $cToken = config('apiconfig.ca_timer')['token'];

        if (strlen($cToken) < config('apiconfig.ca_timer')['token_min_length']) {
            return ConstCode::CODE_ERROR_INVALIDATED_TOKEN_LENGTH;
        }

        if (md5(trim($gToken)) !== md5(trim($cToken))) {
            $tmpAccessIpKey = ConstRedis::PREFIX_TIMER . md5($accessIp); //计数器key
            $accessIpCounter = Cache::store('redis')->get($tmpAccessIpKey);
            if (false === $accessIpCounter) {
                Cache::store('redis')->set($tmpAccessIpKey, 1, config('apiconfig.ca_limit_setting')['redis_timeout']);
            } else if (config('apiconfig.ca_timer')['failed_times'] > 0) {
                if ($accessIpCounter >= config('apiconfig.ca_timer')['failed_times']) {
                    //加入黑名单
                    $rejectIps = Cache::store('redis')->get(self::reject_ips_key());
                    if (false !== $rejectIps) {
                        $rejectIpsArray = explode('|', $rejectIps);
                        $tmpRejectIpsArray = array_merge($rejectIpsArray, array($accessIp));
                        Cache::store('redis')->set(self::reject_ips_key(), implode('|', $tmpRejectIpsArray));
                    } else {
                        Cache::store('redis')->set(self::reject_ips_key(), $accessIp, config('apiconfig.ca_limit_setting')['redis_timeout']);
                    }
                    Cache::store('redis')->set($tmpAccessIpKey, null); //删除计数器
                } else {
                    Cache::store('redis')->inc($tmpAccessIpKey);
                }
            } else {
                Cache::store('redis')->set($tmpAccessIpKey, null);
            }
            return ConstCode::CODE_ERROR_INVALIDATED_TOKEN;
        }

        return 1000;
    }

    //黑名单键名
    private function reject_ips_key()
    {
        return ConstRedis::PREFIX_TIMER . ConstRedis::TIMER_REJECT_IPS;
    }
}
