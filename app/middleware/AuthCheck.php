<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types=1);

namespace app\middleware;

use app\miniprogram\logic\AuthLogic;
use constval\ConstCode;
use constval\ConstKey;
use entity\CaApiEntity;

class AuthCheck
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $userId = $request->post(ConstKey::KEY_USERID . '/d', 0);
        $userToken = $request->post(ConstKey::KEY_USERTOKEN . '/s', '');
        $userType = $request->post(ConstKey::KEY_USERTYPE . '/d', -1);

        $logic_user = new AuthLogic();
        if ($logic_user->checkToken($userId, $userToken, $userType)) {
            return $next($request);
        }
        return CaApiEntity::jsondata(null, ConstCode::MESSAGE_LOGIN, ConstCode::CODE_LOGIN);
    }

    /**
     * 处理结束
     * (注意，在end方法里面不能有任何的响应输出。因为回调触发的时候请求响应输出已经完成了。)
     * @param \think\Response $response
     */
    public function end(\think\Response $response)
    {
        //
    }
}
