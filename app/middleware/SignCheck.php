<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types=1);

namespace app\middleware;

use constval\ConstCode;
use constval\ConstKey;
use entity\CaApiEntity;
use toolkit\tool\CaSignTool;
use function Sodium\compare;

class SignCheck
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //数据提交方式
        if (!$request->isPost()) {
            return CaApiEntity::jsondata_auto(CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_METHOD, ConstCode::MESSAGE_ERROR_INVALIDATED_METHOD));
        }
        //安全处理
        if (config('apiconfig.ca_api')['open_checksign']) {
            $params = $request->post();
            if (self::checkParam($params)) {
                if (self::checkTimestamp($params)) {
                    self::jsonstrFormat($params);
                    if (self::checkSign($params)) {
                        return $next($request);
                    } else {
                        return CaApiEntity::jsondata_auto(CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_SIGN, ConstCode::MESSAGE_ERROR_INVALIDATED_SIGN));
                    }
                } else {
                    return CaApiEntity::jsondata_auto(CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_TIMESTAMP, ConstCode::MESSAGE_ERROR_INVALIDATED_TIMESTAMP));
                }
            } else {
                return CaApiEntity::jsondata_auto(CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_PARAM, ConstCode::MESSAGE_ERROR_INVALIDATED_PARAM));
            }
        } else {
            return $next($request);
        }
    }

    /**
     * 处理结束
     * (注意，在end方法里面不能有任何的响应输出。因为回调触发的时候请求响应输出已经完成了。)
     * @param \think\Response $response
     */
    public function end(\think\Response $response)
    {
        //
    }

    //验签
    private function checkSign($params)
    {
        $sign_type = config('apiconfig.ca_api')['sign_type'];
        $check_result = false;
        switch ($sign_type) {
            case 'md5':
                $init = new CaSignTool(true);
                $check_result = $init->md5Check($params);
                break;
            case 'rsa':
                $init = new CaSignTool();
                $check_result = $init->rsaCheckV1($params);
                break;
            case 'rsa2':
                $init = new CaSignTool(false, null, null, true);
                $check_result = $init->rsaCheckV2($params);
                break;
        }
        return $check_result;
    }

    //检测数据
    private function checkParam($params)
    {
        //待检测数据
        $chkdata = [
            ConstKey::KEY_PLATFORM => 3, ConstKey::KEY_DEVICE => 3, ConstKey::KEY_VERSION => 3,
            ConstKey::KEY_RNDSTR => config('apiconfig.ca_api')['carndstr_mini_length'],
            ConstKey::KEY_RNDNUM => config('apiconfig.ca_api')['carndnum_mini_length'],
            ConstKey::KEY_TIMESTAMP => config('apiconfig.ca_api')['catimestamp_mini_length'],
            ConstKey::KEY_SIGN => config('apiconfig.ca_api')['casign_mini_length'],
        ];

        $chkpassed = true;
        foreach ($chkdata as $key => $val) {
            if (empty($params[$key]) || (mb_strlen($params[$key]) < $val)) {
                $chkpassed = false;
                break;
            }
        }
        return $chkpassed;
    }

    //检测时间戳
    private function checkTimestamp($params)
    {
        $timestamp_timeout = intval(config('apiconfig.ca_limit')['catimestamp_timeout']);
        return ((time() - $params[ConstKey::KEY_TIMESTAMP]) < $timestamp_timeout);
    }

    //格式json字符串
    private function jsonstrFormat(&$params)
    {
        $suffix_json = ConstKey::KEY_SUFFIX_JSONSTR; //以此结尾的键进行实体转换
        $suffix_file = ConstKey::KEY_SUFFIX_ATTACHMENT; //以此结尾的键进行删除
        foreach ($params as $key => $val) {
            if (strpos($key, $suffix_json) !== false) {
                if (substr_compare($key, $suffix_json, -strlen($suffix_json)) === 0) {
                    //存在json结尾的键
                    $entityArr = array(['target' => '&quot;', 'replace' => '"']);
                    foreach ($entityArr as $item) {
                        $params[$key] = str_replace($item['target'], $item['replace'], $params[$key]);
                    }
                }
            } else if (strpos($key, $suffix_file) !== false) {
                if (substr_compare($key, $suffix_file, -strlen($suffix_file)) === 0) {
                    //存在file结尾的键
                    unset($params[$key]);
                }
            }
        }
    }
}
