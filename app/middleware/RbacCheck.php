<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types=1);

namespace app\middleware;

use constval\ConstCode;
use entity\CaApiEntity;
use think\facade\View;

/**
 * RBAC 核心文件
 *
 * Class Auth
 * @package app\middleware
 */
class RbacCheck
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        list($module, $controller, $action) = [app('http')->getName(), $request->controller(), $request->action()];
        $mca = "{$module}/{$controller}/{$action}";
        // 权限校验
        if (!sysrbac($mca)) {
            return CaApiEntity::data(ConstCode::CODE_ERROR_INVALIDATED_POWER, ConstCode::MESSAGE_ERROR_INVALIDATED_POWER);
        }

        // 模板常量声明
        View::assign(['rbacuri' => $mca]);
        return $next($request);
    }
}
