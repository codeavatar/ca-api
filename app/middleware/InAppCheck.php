<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

declare (strict_types = 1);

namespace app\middleware;

class InAppCheck
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        if (preg_match('~micromessenger~i', $request->header('user-agent'))) {
            $request->InApp = 'WeChat';
        } else if (preg_match('~alipay~i', $request->header('user-agent'))) {
            $request->InApp = 'Alipay';
        } else {
            $request->InApp = 'Other';
        }
        return $next($request);
    }

    /**
     * 处理结束
     * (注意，在end方法里面不能有任何的响应输出。因为回调触发的时候请求响应输出已经完成了。)
     * @param \think\Response $response
     */
    public function end(\think\Response $response)
    {
        //
    }
}
