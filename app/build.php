<?php
// 自动生成结构定义文件
return [
    // 需要自动创建的文件
    '__file__'   => ['common.php','event.php','middleware.php','README.md'],
    // 需要自动创建的目录
    '__dir__'    => ['controller', 'model', 'view', 'config', 'route', 'event', 'listener', 'validate', 'middleware'],
    // 需要自动创建的控制器
    'controller' => ['Index'],
    // 需要自动创建的模型
    'model'      => [],
    // 需要自动创建的模板
    'view'       => [],
];