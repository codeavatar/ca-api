<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\wapsite\controller;

use app\BaseController;
use app\miniprogram\model\ConfigModel;
use app\miniprogram\model\SystemConfigModel;
use app\wapsite\model\WebPageModel;

class BasicPage extends BaseController
{
    //获取网页详情
    protected function getPageInfo($keyname){
        $model = new WebPageModel();
        return $model->getInfoByKey($keyname);
    }
}