<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

namespace app\wapsite\controller;


use app\common\constval\ConstDbKey;
use app\miniprogram\model\CourseModel;
use app\miniprogram\model\ExpertModel;
use app\miniprogram\model\TrainingCourseModel;

//手机网页
class WapPage extends BasicPage
{
    //协议模版
    public function template_agreement($keyname){

        return $this->fetch('', ['info' => parent::getSystemConfigInfo($keyname,'value,info,desc')]);
    }
    //专家详情
    public function expert_info($id){
        $model = new ExpertModel();
        if($info = $model->getInfoById($id,'expert_id,expert_name,expert_content')){
            $this->assign('info',$info);
            return $this->fetch();
        }
        return view();
    }
    //平台帮助
    public function platform_help()
    {
        return view();
    }
}