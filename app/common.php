<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

// 应用公共文件

//+---------------------------------------------------------
//+===============框架核心==================================
//+---------------------------------------------------------

//RBAC节点权限验证
function sysrbac($node)
{
    return false;
}

//应用配置
function sysconf($name, $value = null)
{
    $config = [];
    if ($value !== null) {
        list($config, $data) = [[], ['name' => $name, 'value' => $value]];
        return \service\DataService::save('SystemConfig', $data, 'name');
    }
    if (empty($config)) {
        $config = \think\facade\Db::name('SystemConfig')->column('value','name');
    }
    return isset($config[$name]) ? $config[$name] : '';
}

//+---------------------------------------------------------
//+===============应用核心==================================
//+---------------------------------------------------------

//开发状态
function app_dev(){
    return config('apiapp.app_dev');
}

//开发版本
function app_version(){
    if(app_dev()){
        return date('YmdHis');
    }
    return config('apiapp.app_version');
}

//+---------------------------------------------------------
//+===============公共函数==================================
//+---------------------------------------------------------

//唯一标识（按操作路径）
function ca_unique_path($is_cipherext=false){
    return ($is_cipherext ? md5(request()->url(true)) : request()->url(true));
}

//站点地址
function ca_http_root(){
    return config('apiapp.app_url')['web_url'];
}

//附件地址
function ca_http_file(){
    return ca_http_root().'storage/';
}

//访问地址
function ca_mca(){
    list($module, $controller, $action) = [app('http')->getName(), request()->controller(), request()->action()];
    return "{$module}/{$controller}/{$action}";
}

//网址
function ca_url(string $url = '', array $vars = [], $suffix = true, $domain = false){
    return url($url,$vars,$suffix,$domain)->build();
}

//过滤null值
function ca_null_filter($arr, $rootIsArray = false)
{
    //过滤null
    if (null == $arr) {
        if ($rootIsArray) {
            return array();
        } else {
            return new stdClass();
        }
    }

    foreach ($arr as $key => &$val) {
        if (is_array($val)) {
            $val = ca_null_filter($val, true);
        } else {
            if ($val === NULL) {
                $arr[$key] = '';
            }
        }
    }
    return $arr;
}

//输入内容截取
function catp_input(string $key = '', $default = null, $maxlength = 0, $filter = 'htmlspecialchars'){
    $str = input($key,$default,$filter);
    if($maxlength !== 0){
        if(is_string($str)){
            if(strlen($str) > $maxlength){
                $str = substr($str,0,$maxlength);
            }
        }else if(is_array($str)){
            if(count($str) > $maxlength){
                $str = array_slice($str,0,$maxlength);
            }
        }else{
            $str = strval($str);
            $str = substr($str,0,$maxlength);
        }
    }
    return $str;
}

//请求IP
function ca_ip()
{
    return request()->ip();
}

//调试输出
function ca_debug($data){
    halt($data);
}

//日志输出
function ca_log($log, $level='warning'){
    if(is_array($log)){
        if(count($log) === count($log,COUNT_RECURSIVE)){
            //检测是一维数据
            $logstr = json_encode($log);
        }else{
            //检测是多维数据
            $logstr = $log;
        }
    }else if(is_object($log)){
        $logstr = json_encode($log);
    }else{
        $logstr = $log;
    }
    \toolkit\tool\CaLogTool::writelog($logstr, $level);
}

//订单编号
function ca_order_sn($prefix='',$len=0){
    $strtool = new \toolkit\tool\CaStringTool();
    if($len > 32){
        return $prefix.$strtool->getRandomStr($len-strlen($prefix));
    }
    return  $strtool->getUniqueSN($prefix);
}

//文本加星显示
function ca_keep_safestr($str, $start, $len, $symbol = '*')
{
    $strlen = mb_strlen($str);
    $appendStr = '';

    if ($strlen > $start + $len) {
        for ($i = 0; $i < $len; $i++) {
            $appendStr .= $symbol;
        }
        $tmpStr = str_replace(substr($str, $start, $len), $appendStr, $str);
    } else if ($strlen > $start) {
        for ($i = 0; $i < ($strlen - $start); $i++) {
            $appendStr .= $symbol;
        }
        $tmpStr = str_replace(substr($str, $start), $appendStr, $str);
    } else {
        $tmpStr = $str;
    }
    return $tmpStr;
}

// 截取字符串
function ca_substr($str, $start = 0, $length, $charset = "utf-8", $suffix = true)
{
    $strlen = strlen($str);
    $suffixStr = '';
    if ($strlen / 3 > $length) $suffixStr = '...';
    if (function_exists("mb_substr")) {
        if ($suffix)
            return mb_substr($str, $start, $length, $charset) . $suffixStr;
        else
            return mb_substr($str, $start, $length, $charset);
    } elseif (function_exists('iconv_substr')) {
        if ($suffix)
            return iconv_substr($str, $start, $length, $charset) . $suffixStr;
        else
            return iconv_substr($str, $start, $length, $charset);
    }
    $re['utf-8'] = "/[x01-x7f]|[xc2-xdf][x80-xbf]|[xe0-xef][x80-xbf]{2}|[xf0-xff][x80-xbf]{3}/";
    $re['gb2312'] = "/[x01-x7f]|[xb0-xf7][xa0-xfe]/";
    $re['gbk'] = "/[x01-x7f]|[x81-xfe][x40-xfe]/";
    $re['big5'] = "/[x01-x7f]|[x81-xfe]([x40-x7e]|xa1-xfe])/";
    preg_match_all($re[$charset], $str, $match);
    $slice = join("", array_slice($match[0], $start, $length));
    if ($suffix) return $slice . $suffixStr;
    return $slice;
}

//UTF8字符串加密
function ca_utf8_tobgk_encode($string)
{
    list($chars, $length) = ['', strlen($string = iconv('utf-8', 'gbk', $string))];
    for ($i = 0; $i < $length; $i++) {
        $chars .= str_pad(base_convert(ord($string[$i]), 10, 36), 2, 0, 0);
    }
    return $chars;
}

//UTF8字符串解密
function ca_utf8_tobgk_decode($string)
{
    $chars = '';
    foreach (str_split($string, 2) as $char) {
        $chars .= chr(intval(base_convert($char, 36, 10)));
    }
    return iconv('gbk', 'utf-8', $chars);
}

//过滤表情行号
function ca_filter_emoji($str)
{
    preg_match_all('/[\x{4e00}-\x{9fff}\d\w\s[:punct:]]+/u', $str, $result);
    return join('', $result[0]);
}

//防止SQL注入
function ca_sqlinject_filter($sqlval){
    $chars = ['\'',';','%','and','or','!'];
    foreach ($chars as $item){
        $sqlval = str_replace($item,'',$sqlval);
    }
    return $sqlval;
}