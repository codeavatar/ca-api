/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/
var jqext = {
    //自适应高度
    autoheight: function (idstr) {
        setTimeout(function () {
            var diff = 100;
            var winHeight = $(window).height();
            if ($(idstr).height() > winHeight) {
                $(idstr).parent().css('top',((diff-45)/2)+'px');
                $(idstr).height(winHeight-diff);
            }
        },200);
    },
    //弱提示
    layToast: function (msg, isSuccessed) {
        if (typeof isSuccessed == "undefined") {
            layer.msg(msg, {icon: 5, anim: 6});
        } else {
            if (isSuccessed) {
                layer.msg(msg, {icon: 1, anim: 0});
            } else {
                layer.msg(msg, {icon: 5, anim: 6});
            }
        }
    },
    //获取数据
    jqAjax: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            async: false,
            // dataType: "json",
            success: function (res) {
                if (res.code == 200) {
                    callback && callback(res);
                } else {
                    layer.msg(res.msg, {icon: 5, anim: 6});
                }
            }
        });
    },
    //自定义提示
    jqAjaxRes: function (url, data, callback) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            async: false,
            // dataType: "json",
            success: function (res) {
                callback && callback(res);
            }
        });
    },
    //可跳转
    jqAjaxJump: function (data, isJump, callback, errcallback) {
        $.ajax({
            url: data.form.action,
            type: "POST",
            data: data.field,
            async: false,
            dataType: "json",
            success: function (res) {
                if (res.code == 200) {
                    if (isJump) {
                        layer.msg(res.msg, {
                            icon: 1,//提示的样式
                            time: 2000 //2秒关闭（如果不配置，默认是3秒）//设置后不需要自己写定时关闭了，单位是毫
                        }, function () {
                            location.href = res.url;
                        });
                    } else {
                        callback && callback(res);
                    }
                } else {
                    //同默认必填提示一致。
                    layer.msg(res.msg, {icon: 5, anim: 6});
                    errcallback && errcallback(res);
                }
            }
        });
    }
};



