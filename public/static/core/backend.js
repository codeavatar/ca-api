/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

//jQuery后台初始化

if (typeof layui !== 'undefined') {
    var form = layui.form,
        layer = layui.layer,
        element = layui.element,
        tree = layui.tree,
        laydate = layui.laydate;
    if (typeof jQuery === 'undefined') {
        var $ = jQuery = layui.$;
    }
}

$(function () {
    // 当前页面Body对象
    var $body = $('body');

    /*! 消息组件实例 */
    $.msg = new function () {
        var self = this;
        this.shade = [0.02, '#000'];
        this.dialogIndexs = [];
        // 关闭消息框
        this.close = function (index) {
            return layer.close(index);
        };
        // 弹出警告消息框
        this.alert = function (msg, callback) {
            var index = layer.alert(msg, {end: callback, scrollbar: false});
            return this.dialogIndexs.push(index), index;
        };
        // 确认对话框
        this.confirm = function (msg, ok, no) {
            var index = layer.confirm(msg, {title: '操作确认', btn: ['确认', '取消']}, function () {
                typeof ok === 'function' && ok.call(this);
            }, function () {
                typeof no === 'function' && no.call(this);
                self.close(index);
            });
            return index;
        };
        // 显示成功类型的消息
        this.success = function (msg, time, callback) {
            var index = layer.msg(msg, {
                icon: 1,
                shade: this.shade,
                scrollbar: false,
                end: callback,
                time: (time || 2) * 1000,
                shadeClose: true
            });
            return this.dialogIndexs.push(index), index;
        };
        // 显示失败类型的消息
        this.error = function (msg, time, callback) {
            var index = layer.msg(msg, {
                icon: 5,
                shade: this.shade,
                scrollbar: false,
                time: (time || 3) * 1000,
                end: callback,
                anim: 6,
                shadeClose: true
            });
            return this.dialogIndexs.push(index), index;
        };
        // 状态消息提示
        this.tips = function (msg, time, callback) {
            var index = layer.msg(msg, {time: (time || 3) * 1000, shade: this.shade, end: callback, shadeClose: true});
            return this.dialogIndexs.push(index), index;
        };
        // 显示正在加载中的提示
        this.loading = function (msg, callback) {
            var index = msg ? layer.msg(msg, {
                icon: 16,
                scrollbar: false,
                shade: this.shade,
                time: 0,
                end: callback
            }) : layer.load(2, {time: 0, scrollbar: false, shade: this.shade, end: callback});
            return this.dialogIndexs.push(index), index;
        };
        // 自动处理显示Think返回的Json数据
        this.auto = function (data, time) {
            return (parseInt(data.code) === 200) ? self.success(data.msg, time, function () {
                !!data.url ? (window.location.href = data.url) : $.form.reload();
                for (var i in self.dialogIndexs) {
                    layer.close(self.dialogIndexs[i]);
                }
                self.dialogIndexs = [];
            }) : self.error(data.msg, 3, function () {
                //自动更新令牌
                backend.tpFormResetToken(data.data);
                !!data.url && (window.location.href = data.url);
            });
        };
    };

    /*! 表单自动化组件 */
    $.form = new function () {
        var self = this;
        // 默认异常提示消息
        this.errMsg = '{status}服务器繁忙，请稍候再试！';
        // 内容区域动态加载后初始化
        this.reInit = function ($container) {
            self.listen();
            $container.find('[lay-verify*="required"]').parent().prev('label').addClass('ca-label-required');
        };
        // 在内容区显示视图
        this.show = function (html) {
            var $container = $('.ca-framework-body').html(html);
            reinit.call(this), setTimeout(reinit, 500), setTimeout(reinit, 1000);

            function reinit() {
                $.form.reInit($container);
            }
        };
        // 以hash打开网页
        this.href = function (url, obj) {
            if (url !== '#') {
                window.location.href = '#' + $.menu.parseUri(url, obj);
            } else if (obj && obj.getAttribute('ca-menu-node')) {
                var node = obj.getAttribute('ca-menu-node');
                $('[ca-menu-node^="' + node + '-"][ca-open!="#"]:first').trigger('click');
            }
        };
        // 刷新当前页面
        this.reload = function () {
            window.onhashchange.call(this);
        };
        // 异步加载的数据
        this.load = function (url, data, type, callback, loading, tips, time) {
            var self = this, dialogIndex = 0;
            (loading !== false) && (dialogIndex = $.msg.loading(tips));
            //加载进度条
            (typeof Pace === 'object') && Pace.restart();
            $.ajax({
                type: type || 'GET', url: $.menu.parseUri(url), data: data || {},
                statusCode: {
                    404: function () {
                        $.msg.close(dialogIndex);
                        $.msg.tips(self.errMsg.replace('{status}', 'E404 - '));
                    },
                    500: function () {
                        $.msg.close(dialogIndex);
                        $.msg.tips(self.errMsg.replace('{status}', 'E500 - '));
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.msg.close(dialogIndex);
                    $.msg.tips(self.errMsg.replace('{status}', 'E' + textStatus + ' - '));
                },
                success: function (res) {
                    $.msg.close(dialogIndex);
                    if (typeof callback === 'function' && callback.call(self, res) === false) {
                        return false;
                    }
                    if (typeof (res) === 'object') {
                        return $.msg.auto(res, time || res.wait || undefined);
                    }
                    self.show(res);
                }
            });
        };
        // 加载HTML到目标位置
        this.open = function (url, data, callback, loading, tips) {
            this.load(url, data, 'get', function (res) {
                if (typeof (res) === 'object') {
                    return $.msg.auto(res);
                }
                self.show(res);
            }, loading, tips);
        };
        // 打开一个iframe窗口
        this.iframe = function (url, title, isFullScreen) {
            var width = $(document).width(), height = $(document).height();
            isFullScreen = (typeof isFullScreen != 'undefined') ? isFullScreen : false;
            return layer.open({
                title: title || '窗口',
                type: 2,
                area: (isFullScreen ? [width + 'px', height + 'px'] : ['800px', '530px']),
                fixed: true,
                maxmin: false,
                content: url
            });
        };
        // 加载HTML到弹出层
        this.modal = function (url, data, title, callback, loading, tips) {
            this.load(url, data, 'GET', function (res) {
                if (typeof (res) === 'object') {
                    return $.msg.auto(res);
                }
                var layerIndex = layer.open({
                    type: 1,
                    btn: false,
                    area: "800px",
                    content: res,
                    title: title || '',
                    success: function (dom, index) {
                        $(dom).find('[ca-close]').off('click').on('click', function () {
                            if ($(this).attr('ca-confirm')) {
                                var confirmIndex = $.msg.confirm($(this).attr('ca-confirm'), function () {
                                    layer.close(confirmIndex), layer.close(index);
                                });
                                return false;
                            }
                            layer.close(index);
                        });
                        $.form.reInit($(dom));
                    }
                });
                $.msg.dialogIndexs.push(layerIndex);
                return (typeof callback === 'function') && callback.call(this);
            }, loading, tips);
        };
        // 监听表单
        this.listen = function () {
            $('form[ca-form-auto]').map(function () {
                if ($(this).attr('ca-form-listen') !== 'true') {
                    var _this = $(this);
                    var callbackname = _this.attr('ca-callback');
                    _this.attr('ca-form-listen', 'true');
                    //设置必填项提示
                    $(this).find(':input').map(function () {
                        if ((typeof $(this).attr('lay-verify') !== 'undefined') && (typeof $(this).attr('lay-reqText') === 'undefined')) {
                            if ($(this).attr('lay-verify').indexOf('required') !== -1) {
                                $(this).attr('lay-reqText', $(this).attr('placeholder') || '必填项不能为空');
                            }
                        }
                    });
                    //启用表单按扭
                    $(this).find('[ca-form-loaded]').map(function () {
                        $(this).html(this.getAttribute('ca-form-loaded') || this.innerHTML);
                        $(this).removeAttr('ca-form-loaded').removeClass('layui-btn-disabled');
                    });
                    //监听提交
                    form.on('submit', function (data) {
                        var method = _this.attr('method') || 'POST';
                        var tips = _this.attr('ca-tips') || undefined;
                        var url = _this.attr('action') || window.location.href;
                        var callback = window[callbackname || '_default_callback'] || undefined;
                        var time = _this.attr('ca-time') || undefined;
                        $.form.load(url, data.field, method, callback, true, tips, time);
                        return false;
                    });
                    // form.on('submit(*)', function (data) {});
                    $(this).find('[lay-verify*="required"]').parent().prev('label').addClass('ca-label-required');
                }
            });
        }
    };

    /*! 后台菜单辅助插件 */
    $.menu = new function () {
        // 计算URL地址中有效的URI
        this.getUri = function (uri) {
            uri = uri || window.location.href;
            uri = (uri.indexOf(window.location.host) > -1 ? uri.split(window.location.host)[1] : uri).split('?')[0];
            return (uri.indexOf('#') !== -1 ? uri.split('#')[1] : uri);
        };
        // 通过URI查询最有可能的菜单NODE
        this.queryNode = function (url) {
            var node = location.href.replace(/.*spm=([\d\-m]+).*/ig, '$1');
            if (!/^m\-/.test(node)) {
                var $menu = $('[ca-menu-node][ca-open*="' + url.replace(/\.html$/ig, '') + '"]');
                return $menu.length ? $menu.get(0).getAttribute('ca-menu-node') : '';
            }
            return node;
        };
        // URL转URI
        this.parseUri = function (uri, obj) {
            var params = {};
            if (uri.indexOf('?') > -1) {
                var serach = uri.split('?')[1].split('&');
                for (var i in serach) {
                    if (serach[i].indexOf('=') > -1) {
                        var arr = serach[i].split('=');
                        try {
                            params[arr[0]] = window.decodeURIComponent(window.decodeURIComponent(arr[1].replace(/%2B/ig, ' ')));
                        } catch (e) {
                            console.log([e, uri, serach, arr]);
                        }
                    }
                }
            }
            uri = this.getUri(uri);
            params.spm = obj && obj.getAttribute('ca-menu-node') || this.queryNode(uri);
            delete params[""];
            var query = '?' + $.param(params);
            return uri + (query !== '?' ? query : '');
        };
        // 后台菜单动作初始化
        this.listen = function () {
            var self = this;
            // 左则二级菜单展示
            $('[ca-submenu-layout]>a').on('click', function () {
                $(this).parent().toggleClass('open');
                self.syncOpenStatus(1);
            });
            // 同步二级菜单展示状态
            this.syncOpenStatus = function (mode) {
                $('[ca-submenu-layout]').map(function () {
                    var node = $(this).attr('ca-submenu-layout');
                    if (mode === 1) {
                        var type = (this.className || '').indexOf('open') > -1 ? 2 : 1;
                        layui.data('menu', {key: node, value: type});
                    } else {
                        var type = layui.data('menu')[node] || 2;
                        (type === 2) && $(this).addClass('open');
                    }
                });
            };
            window.onhashchange = function () {
                var hash = window.location.hash || '';
                if (hash.length < 1) {
                    return $('[ca-menu-node][ca-open!="#"]:first').trigger('click');
                }
                $.form.load(hash);
                self.syncOpenStatus(2);
                // 菜单选择切换
                var node = self.queryNode(self.getUri());
                if (/^m\-/.test(node)) {
                    var $all = $('a[ca-menu-node]'), tmp = node.split('-'), tmpNode = tmp.shift();
                    while (tmp.length > 0) {
                        tmpNode = tmpNode + '-' + tmp.shift();
                        $all = $all.not($('a[ca-menu-node="' + tmpNode + '"]').addClass('active'));
                    }
                    $all.removeClass('active');
                    // 菜单模式切换
                    if (node.split('-').length > 2) {
                        var _tmp = node.split('-'), _node = _tmp.shift() + '-' + _tmp.shift();
                        $('[ca-menu-layout]').not($('[ca-menu-layout="' + _node + '"]').removeClass('hide')).addClass('hide');
                        $('[ca-menu-node="' + node + '"]').parent('div').parent('div').addClass('open');
                        $('body.ca-framework').removeClass('mini');
                    } else {
                        $('body.ca-framework').addClass('mini');
                    }
                    self.syncOpenStatus(1);
                }
            };
            // URI初始化动作
            window.onhashchange.call(this);
        };
    };

    // 上传单个图片
    $.fn.uploadOneImage = function () {
        var name = $(this).attr('name') || 'image';
        var type = $(this).data('type') || 'png,jpg';
        var $tpl = $('<a ca-file="one" ca-field="' + name + '" ca-type="' + type + '" class="uploadimage"></a>');
        $(this).attr('name', name).after($tpl).on('change', function () {
            !!this.value && $tpl.css('backgroundImage', 'url(' + this.value + ')');
        }).trigger('change');
    };

    // 上传多个图片
    $.fn.uploadMultipleImage = function () {
        var type = $(this).data('type') || 'png,jpg';
        var name = $(this).attr('name') || 'umt-image';
        var $tpl = $('<a ca-file="mut" ca-field="' + name + '" ca-type="' + type + '" class="uploadimage"></a>');
        $(this).attr('name', name).after($tpl).on('change', function () {
            var input = this, values = [], srcs = this.value.split('|');
            $(this).prevAll('.uploadimage').map(function () {
                values.push($(this).attr('ca-tips-image'));
            }), $(this).prevAll('.uploadimage').remove(), values.reverse();
            for (var i in srcs) {
                srcs[i] && values.push(srcs[i]);
            }
            this.value = values.join('|');
            for (var i in values) {
                var tpl = '<div class="uploadimage uploadimagemtl"><a class="layui-icon">&#x1006;</a></div>';
                var $tpl = $(tpl).attr('ca-tips-image', values[i]).css('backgroundImage', 'url(' + values[i] + ')');
                $tpl.data('input', input).data('srcs', values).data('index', i);
                $tpl.on('click', 'a', function (e) {
                    e.stopPropagation();
                    var $cur = $(this).parent();
                    var dialogIndex = $.msg.confirm('确定要移除这张图片吗？', function () {
                        var data = $cur.data('srcs'), tmp = [];
                        for (var i in data) {
                            i !== $cur.data('index') && tmp.push(data[i]);
                        }
                        $cur.data('input').value = tmp.join('|');
                        $cur.remove(), $.msg.close(dialogIndex);
                    });
                });
                $(this).before($tpl);
            }
        }).trigger('change');
    };

    /*! 注册 ca-load 事件行为 */
    $body.on('click', '[ca-load]', function () {
        var url = $(this).attr('ca-load'), tips = $(this).attr('ca-tips');
        if ($(this).attr('ca-confirm')) {
            return $.msg.confirm($(this).attr('ca-confirm'), _goLoad);
        }
        return _goLoad.call(this);

        function _goLoad() {
            $.form.load(url, {}, 'get', null, true, tips);
        }
    });

    /*! 注册 ca-serach 表单搜索行为 */
    $body.on('submit', 'form.form-search', function () {
        if(typeof $(this).attr('action') != 'undefined'){
            var url = $(this).attr('action').replace(/\&?page\=\d+/g, ''), split = url.indexOf('?') === -1 ? '?' : '&';
            if ((this.method || 'get').toLowerCase() === 'get') {
                return window.location.href = '#' + $.menu.parseUri(url + split + $(this).serialize());
            }
            $.form.load(url, this, 'post');
        }
    });

    /*! 注册 ca-modal 事件行为 */
    $body.on('click', '[ca-modal]', function () {
        return $.form.modal($(this).attr('ca-modal'), 'open_type=modal', $(this).attr('ca-title') || '编辑');
    });

    /*! 注册 ca-open 事件行为 */
    $body.on('click', '[ca-open]', function () {
        $.form.href($(this).attr('ca-open'), this);
    });

    /*! 注册 ca-iframe 事件行为 */
    $body.on('click', '[ca-iframe]', function () {
        $.form.iframe($(this).attr('ca-iframe'), $(this).attr('ca-title') || '窗口');
    });

    /*! 注册 ca-iframe-full 事件行为 */
    $body.on('click', '[ca-iframe-full]', function () {
        var index = $.form.iframe($(this).attr('ca-iframe-full'), $(this).attr('ca-title') || '窗口', true);
    });

    $body.on('click', '[ca-iframe-close]', function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    });

    /*! 注册 ca-back 事件行为 */
    $body.on('click', '[ca-back]', function () {
        window.history.back();
        // window.history.go(-1);
    });

    /*! 注册 ca-reload 事件行为 */
    $body.on('click', '[ca-reload]', function () {
        $.form.reload();
    });

    /*! 注册 ca-check-target 事件行为 */
    $body.on('click', '[ca-check-target]', function () {
        var checked = !!this.checked;
        $($(this).attr('ca-check-target')).map(function () {
            this.checked = checked;
        });
    });

    /*! 注册 ca-update 事件行为 */
    $body.on('click', '[ca-update]', function () {
        var id = $(this).attr('ca-update') || (function () {
            var data = [];
            return $($(this).attr('ca-list-target') || 'input.list-check-box').map(function () {
                (this.checked) && data.push(this.value);
            }), data.join(',');
        }).call(this);
        if (id.length < 1) {
            return $.msg.tips('请选择需要操作的数据！');
        }
        var action = $(this).attr('ca-action') || $(this).parents('[ca-location]').attr('ca-location');
        var value = $(this).attr('ca-value') || 0, field = $(this).attr('ca-field') || 'status';
        var msgtext = $(this).attr('ca-msg') || '确定要操作该数据吗？';
        var resulttext = $(this).attr('ca-result') || '操作';
        $.msg.confirm(msgtext, function () {
            $.form.load(action, {field: field, value: value, result: resulttext, id: id}, 'post');
        });
    });

    /*! 注册 ca-href 事件行为 */
    $body.on('click', '[ca-href]', function () {
        var href = $(this).attr('ca-href');
        (href && href.indexOf('#') !== 0) && (window.location.href = href);
    });

    /*! 注册 ca-page-href 事件行为 */
    $body.on('click', 'a[ca-page-href]', function () {
        window.location.href = '#' + $.menu.parseUri(this.href, this);
    });

    /*! 注册 ca-file 事件行为 */
    $body.on('click', '[ca-file]', function () {
        var method = $(this).attr('ca-file') === 'one' ? 'one' : 'mtl';
        var type = $(this).attr('ca-type') || 'jpg,png', field = $(this).attr('ca-field') || 'file';
        var title = $(this).attr('ca-title') || '文件上传', uptype = $(this).attr('ca-uptype') || '';
        var url = window.ROOT_URL + '/core/system_plugs/upfile.html?mode=' + method + '&uptype=' + uptype + '&type=' + type + '&field=' + field;
        $.form.iframe(url, title || '文件管理');
    });

    /*! 注册 ca-icon 事件行为 */
    $body.on('click', '[ca-icon]', function () {
        var field = $(this).attr('ca-icon') || $(this).attr('ca-field') || 'icon';
        var url = window.ROOT_URL + '/core/system_plugs/icon.html?field=' + field + '&' + Math.random();
        $.form.iframe(url, '图标选择');
    });

    /*! 注册 ca-tips-image 事件行为 */
    $body.on('click', '[ca-tips-image]', function () {
        var img = new Image(), index = $.msg.loading();
        var imgWidth = this.getAttribute('ca-width') || '480px';
        img.onload = function () {
            var $content = $(img).appendTo('body').css({background: '#fff', width: imgWidth, height: 'auto'});
            layer.open({
                type: 1, area: imgWidth, title: false, closeBtn: 1,
                skin: 'layui-layer-nobg', shadeClose: true, content: $content,
                end: function () {
                    $(img).remove();
                },
                success: function () {
                    $.msg.close(index);
                }
            });
        };
        img.onerror = function (e) {
            $.msg.close(index);
        };
        img.src = this.getAttribute('ca-tips-image') || this.src;
    });

    // /*! 注册 ca-tips-text 事件行为 */
    // $body.on('mouseenter', '[ca-tips-text]', function () {
    //     var text = $(this).attr('ca-tips-text'), placement = $(this).attr('ca-tips-placement') || 'auto';
    //     $(this).tooltip({title: text, placement: placement}).tooltip('show');
    // });

    /*! 注册 ca-phone-view 事件行为 */
    $body.on('click', '[ca-phone-view]', function () {
        var $container = $('<div class="mobile-preview pull-left"><div class="mobile-header">公众号</div><div class="mobile-body"><iframe id="phone-preview" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>').appendTo('body');
        $container.find('iframe').attr('src', this.getAttribute('ca-phone-view') || this.href);
        layer.style(layer.open({
            type: 1, scrollbar: !1, area: ['335px', '600px'], title: !1,
            closeBtn: 1, skin: 'layui-layer-nobg', shadeClose: !1, content: $container,
            end: function () {
                $container.remove();
            }
        }), {boxShadow: 'none'});
    });

    /*! 分页事件切换处理 */
    $body.on('change', '.ca-pagination-trigger select', function () {
        window.location.href = this.options[this.selectedIndex].getAttribute('ca-url');
    });

    /*! 初始化 */
    $.menu.listen();
    $.form.listen();
});

var layuiext = {

    msgToast: function (msg, isSuccessed) {
        if (typeof isSuccessed == "undefined") {
            layer.msg(msg, {icon: 5, anim: 6});
        } else {
            if (isSuccessed) {
                layer.msg(msg, {icon: 1, anim: 0});
            } else {
                layer.msg(msg, {icon: 5, anim: 6});
            }
        }
    },

    msgConfirm: function(msg, ok, no){
        var index = $.msg.confirm(msg, function () {
            typeof ok === 'function' && ok.call(this);
            $.msg.close(index);
        }, no);
        return index;
    },

    bindRangeDate: function (beginId, endId, timetype) {
        var nowTime = new Date();
        var startTime = laydate.render({
            elem: '#' + beginId,
            type: timetype || 'date', //year、month、date、time、datetime
            btns: ['clear','confirm'],
            max: 'nowTime',//默认最大值为当前日期
            done: function (value, date) {
                if(value.length < 1){
                    endTime.config.min= (nowTime.getFullYear()-100)+'-1-1';
                    return;
                }
                endTime.config.min = {
                    year: date.year,
                    month: date.month - 1,//关键
                    date: date.date,
                    hours: date.hours,
                    minutes: date.minutes,
                    seconds: date.seconds
                };
            }
        });
        var endTime = laydate.render({
            elem: '#' + endId,
            type: timetype || 'date',//year、month、date、time、datetime
            btns: ['clear','confirm'],
            min: 'nowTime',
            done: function (value, date) {
                if(value.length < 1){
                    endTime.config.max= (nowTime.getFullYear()+100)+'-1-1';
                    return;
                }
                startTime.config.max = {
                    year: date.year,
                    month: date.month - 1,//关键
                    date: date.date,
                    hours: date.hours,
                    minutes: date.minutes,
                    seconds: date.seconds
                }
            }
        });
    },
    frmRender: function () {
        //LayUI 表单动态样式渲染
        if (typeof window.element !== 'undefined')
            window.element.init();
        //LayUI 样式渲染
        if (typeof window.form !== 'undefined')
            window.form.render();
    },
    frmErrorTip: function (msg, tid) {
        if (typeof tid === 'string') {
            $('#' + tid).addClass('layui-form-danger').focus();
        }
        layer.msg(msg, {icon: 5, anim: 6});
    },
    frmSuccessTip: function (msg) {
        layer.msg(msg, {icon: 1, anim: 0});
    },
    frmVerify: function (verifyData) {
        form.verify(verifyData || {});
    },
    frmEvent: function (onkey, func) {
        form.on(onkey, func);
    },
    frmTrigger: function (frmId) {
        //未解决，需要深入研究
        layui.$('[lay-filter="' + frmId + '"]').trigger('click');
    },
    frmData: function (frmId, frmData) {
        form.val(frmId, frmData);
    },
    frmAjax: function (data, isJump, callback, errcallback) {
        $.ajax({
            url: data.form.action,
            type: "POST",
            data: data.field,
            async: false,
            dataType: "json",
            success: function (res) {
                if (res.code == 200) {
                    if (isJump) {
                        layer.msg(res.msg, {
                            icon: 1,//提示的样式
                            time: 2000 //2秒关闭（如果不配置，默认是3秒）//设置后不需要自己写定时关闭了，单位是毫
                        }, function () {
                            location.href = res.url;
                        });
                    } else {
                        callback && callback(res);
                    }
                } else {
                    //同默认必填提示一致。
                    this.frmErrorTip(res.msg);
                    errcallback && errcallback(res);
                }
            }
        });
    }
};

var backend = {
    tpFormResetToken: function (val, $tokenKey) {
        if (typeof $tokenKey == 'undefined' || $tokenKey == '') {
            $('input[name="__token__"]').val(val);
        }
    },
    initTool: function (requirekey, func) {
        if (typeof requirekey == 'string') {
            require([requirekey], func);
        } else if (typeof requirekey == 'array') {
            require(requirekey, func);
        }
    },
    initCkeditor: function (tid, options) {
        require(['ckeditor'], function () {
            CKEDITOR.replace(tid || 'description', options || {height: 300});
        });
    },
    setCkeditorData: function (html, tid) {
        if (typeof tid === "undefined") {
            CKEDITOR.document.getById('description').setHtml(html);
        } else {
            CKEDITOR.document.getById(tid).setHtml(html);
        }
    },
    getCkeditorData: function (tid) {
        if (typeof tid !== "undefined") {
            return eval('CKEDITOR.instances.' + tid + '.getData()');
        }
        return CKEDITOR.instances.description.getData();
    },
    syncCkeditorData: function () {
        //同步CKEditor数据
        if (typeof CKEDITOR === 'object' && typeof CKEDITOR.instances === 'object') {
            for (var instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }
    }
}