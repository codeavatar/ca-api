/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

require.config({
    waitSeconds:10,
    baseUrl:window.PLUG_URL,
    paths: {
        'ckeditor': 'ckeditor/v4.9.2/ckeditor',
        'datetimetool': 'tool/datetimetool',
    },
    //不符合AMD规范加载
    shim:{

    },
    deps:[]
});