// +----------------------------------------------------------------------
// | @Author: code avatar   @Year：2021
// +----------------------------------------------------------------------
// | @Email: codeavatar@aliyun.com
// +----------------------------------------------------------------------
var datetimetool = {
    YEAR: 'Y',
    MONTH: 'M',
    DATE: 'D',
    HOUR: 'H',
    MINUTE: 'I',
    SECOND: 'S',
    datestrPlus: function (datestr, type, num) {
        var timestamp = Date.parse(datestr); //毫秒
        timestamp += this.getMillisecondDifference(type, num);
        var newDate = new Date(timestamp);
        return this.getDatetimeStr(newDate);
    },
    datestrMinus: function () {
        var timestamp = Date.parse(datestr); //毫秒
        timestamp -= this.getMillisecondDifference(type, num);
        var newDate = new Date(timestamp);
        return this.getDatetimeStr(newDate);
    },
    compare: function (datestr1, datestr2) {
        var timestamp1 = Date.parse(datestr1);
        var timestamp2 = Date.parse(datestr2);
        var timestampdiff = timestamp1 - timestamp2;
        return (timestampdiff > 0) ? 1 : (timestampdiff == 0 ? 0 : -1);
    },
    //获取毫秒差
    getMillisecondDifference: function (type, num) {
        switch (type) {
            case this.YEAR:
                num *= 366 * 24 * 3600 * 1000;
                break;
            case this.MONTH:
                num *= 30 * 24 * 3600 * 1000;
                break;
            case this.DATE:
                num *= 24 * 3600 * 1000;
                break;
            case this.HOUR:
                num *= 3600 * 1000;
                break;
            case this.MINUTE:
                num *= 60 * 1000;
                break;
            case this.SECOND:
                num *= 1000;
                break;
            default:
                num *= 1000;
                break;
        }
        return num;
    },
    //统一输出格式
    getDatetimeStr: function (newDate) {
        return newDate.getFullYear() + '-' + this.getFullVal(newDate.getMonth()+1) + '-' + this.getFullVal(newDate.getDate()) + ' ' + this.getFullVal(newDate.getHours()) + ':' + this.getFullVal(newDate.getMinutes()) + ':' + this.getFullVal(newDate.getSeconds());
    },
    //小于9的前面补0
    getFullVal:function (val) {
        if(val > 9) return val;
        return '0'+val;
    }
};