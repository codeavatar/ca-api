<?php

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

return [
    // 默认缓存驱动
    'default' => env('cache.driver', 'file'),

    // 缓存连接方式配置
    'stores'  => [
        // 文件缓存
        'file' => [
            // 驱动方式
            'type'       => 'File',
            // 缓存保存目录
            'path'       => '',
            // 缓存前缀
            'prefix'     => '',
            // 缓存有效期 0表示永久缓存
            'expire'     => 0,
            // 缓存标签前缀
            'tag_prefix' => 'tag:',
            // 序列化机制 例如 ['serialize', 'unserialize']
            'serialize'  => [],
        ],
        // redis缓存
        'redis'   =>  [
            // 驱动方式
            'type'   => 'redis',
            // 服务器地址
            'host'       => env('cache.redis_host','127.0.0.1'),
            //端口
            'port' => env('cache.redis_port',6379),
            // 访问口令
            'password' => env('cache.redis_password','xxxxxxxxxxxxxxxxxxxxxxxxxxxx'),
            // 缓存前缀
            'prefix'     => env('cache.redis_prefix',''),
            // 缓存有效期 0表示永久缓存
            'expire'     => env('cache.redis_expire',0),
        ],
        // 更多的缓存连接
    ],
];
