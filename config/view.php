<?php
// +----------------------------------------------------------------------
// | 模板设置
// +----------------------------------------------------------------------

return [
    // 模板引擎类型使用Think
    'type'          => 'Think',
    // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写 3 保持操作方法
    'auto_rule'     => 1,
    // 模板目录名
    'view_dir_name' => 'view',
    // 模板后缀
    'view_suffix'   => 'html',
    // 模板文件名分隔符
    'view_depr'     => DIRECTORY_SEPARATOR,
    // 模板引擎普通标签开始标记
    'tpl_begin'     => '{',
    // 模板引擎普通标签结束标记
    'tpl_end'       => '}',
    // 标签库标签开始标记
    'taglib_begin'  => '{',
    // 标签库标签结束标记
    'taglib_end'    => '}',
    // 定义模板替换字符串
    'tpl_replace_string' =>[
        '__APP_VERSION__'   => app_dev(),
        '__HTTP_HOST__'     => app('request')->domain(),
        '__ROOT_PATH__'     => app('request')->root(false),
        '__STATIC_PATH__'   => app('request')->root(false).'/static',
        '__CORE_PATH__'     => app('request')->root(false).'/static/core',
        '__THEME_PATH__'    => app('request')->root(false).'/static/theme/default',
        '__PLUG_PATH__'     => app('request')->root(false).'/static/plug',
        '__UPLOAD_PATH__'   => app('request')->root(false).'/upload',
    ],
    // 是否开启模板编译缓存,设为false则每次都会重新编译(发布后建议开启)
    'tpl_cache'     => env('view.tpl_cache', true),
    // 模板渲染缓存
    'display_cache' => env('view.display_cache', true),
];
