<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

// +----------------------------------------------------------------------
// | caadmin 框架核心配置
// +----------------------------------------------------------------------

return [
    // +----------------------------------------------------------------------
    // | 框架安全设置
    // +----------------------------------------------------------------------
    //api控制器设置
    'ca_api' => [
        'open_checksign' => env('apiconfig.ca_api', ['open_checksign'=>true])['open_checksign'], //是否开启验证
        'sign_type' => 'md5', //md5,rsa,rsa2
        'rsa_private_key' => 'MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKZmwhkhedig3EsjJGjRYSxEcVw8EfxUuvi8UbjazqqjdwnP92zDLDWQw6P7jcov0u+hohFGA8g9WTIPm5m/BHCpMVYqcDnjV6TRZjjU6lUtSrmPhabMjlmr6WAQNSvrFgzlaasSTFQLbC3ag4W4afzrnZlwBMk8g+ehdd7PL1vtAgMBAAECgYB39bCMCXQb1au6hzUiU3+oOBc5FR0qObMoyipoB2Xh4kJY0pJe2D0wyJIhz6GiVuPMWUvLBByJ6KA61MGqDukVXWz05djuQY3lsDT8yTPsmw9si41kpto6obuNLP1WuTi6rGGGUsEzPh5db/qb9LZ9iZ1FmkxL/E988mIeUhq9CQJBAM9U+gQey7kHc+/xrYttoEl4G5UDmPraQzd+jmMJ1ZlQdVaXuNUNTyFTcnKtFztgn6Zm4pXyb/2mQmyREm+Ay58CQQDNdi1XPJyyQBzzeS1JaxN1TAvyMksW8ktWycchE/PlTCeCW0oMkcUOacXUL0XXyEbODsNLgIPUztXX4r8QmWzzAkEAo1efYX+hoLRucqtEvoQvs7IciNm8vGUYb7AVHZGx5oes6fu+cpt5rBfCIvabRvxoywBjox7NfFhwHHhuvcoKoQJBAMVNFOFPDVKHCsYruXdlYbwkGNytuzn/1qUzAwwasv2gkdg2ffdwDAF5RUYrVzCN3KCHwLbCZNZ5ARVdYwk3rE0CQQC7QPLNP52pB8Uj9vKD3qjVGeeHwReFVFQEuLBBgHUNXGvqomXKb+TZ443BMq4kFadfEp9Yl1wXRkX+pVm+7ZOJ',//RSA私钥，必须在一行
        'rsa_public_key' => 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmZsIZIXnYoNxLIyRo0WEsRHFcPBH8VLr4vFG42s6qo3cJz/dswyw1kMOj+43KL9LvoaIRRgPIPVkyD5uZvwRwqTFWKnA541ek0WY41OpVLUq5j4WmzI5Zq+lgEDUr6xYM5WmrEkxUC2wt2oOFuGn8652ZcATJPIPnoXXezy9b7QIDAQAB',//RSA公钥，必须在一行
        'rsa_private_key_file_path' => root_path() . 'notp' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'rsa' . DIRECTORY_SEPARATOR . 'rsa_private_key.pem',//RSA私钥证书地址
        'rsa_public_key_file_path' => root_path() . 'notp' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'rsa' . DIRECTORY_SEPARATOR . 'rsa_public_key.pem',//RSA公钥证书地址
        'md5_secret_key' => 'MIGfMA0GCSqGSIb3Ds01QEBAQUAA4ssdfxsvdvgHnOxsdn7LLILlKETd6BFRJ0GdsgS2Y3mn1wMQmyh9zEyWlz5p1zrahRahbXAfCfSqshSNfqOmAQzSHRVjCqjsAw1jyqrXaPdKBmr90DIpIxmIyKXv4GGAkPyJ/6FTFY99uhpiq0qadD/u2vx.?SzQsefWo0aTvP/65zi3ex3of7TcZ32oWvDAQAB', //MD5签名密钥
        'carndstr_mini_length' => 32,
        'carndnum_mini_length' => 20,
        'catimestamp_mini_length' => 10,
        'casign_mini_length' => 32,
    ],
    //定时器设置
    'ca_timer' => [
        //定时器token设置，推荐200位及以上，由大小写字线、下划线、除加号和空格以外的其他特殊符号，符号不级用于开始或结束。
        'token' => 'MIGfMA0GCSqGSIb3Ds01QEBAQUAA4GNADCBiQKBvsgQDIvgHnOn7LLILlKETd6BFRJs0GqsgS2Y3mn1wMQmyh9zEyWlz5p1zrahRahbXAfCfSqshSNfqOmAQzSHRVjCqjsAw1jyqrXaPdKBmr90DIpIxmIyKXv4GGAkPyJ/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3ex3of7TcZ32oWpwIDAQAB',
        'token_min_length' => 200,
        'allow_ips' => ['127.0.0.1','::1'],//ip白名单 （发布后::1去掉）
        'failed_times' => 5, //限制Token校验失败次数（超过限制请求的IP加入黑名单），0：不限
    ],
    //安全限制
    'ca_limit' => [
        'catimestamp_timeout' => 1*3600, //时间戳有效时长（单位秒）
        'redis_timeout' => 3600*24*7, //默认Redis超时，7天（单位秒）
        'token_timeout' => 3600*24*7, //Token 超时时间（单位秒）
        'token_length' => 100, //Token长度
        'request_locktime' => 2, //api单次请求锁定时长（单位秒）
        'action_times' => 10,//操作次数
        'action_locktime' => 10,//操作锁定时间（单位秒）
        'sys_config_time'=>3600*24*1, //1天（单位秒）
        'user_login_times'=>10, //登录失败10次锁定帐号
        'user_login_locked_time'=>3600*1,//1小时（单位秒）
    ],

    // +----------------------------------------------------------------------
    // | 框架第三方设置
    // +----------------------------------------------------------------------
    //短信
    'ca_sms' => [
        'is_open' => false, //屏蔽发送短信
        'use_sms' => 'aliyun', //类型，cly》诚立业；aliyun》阿里云；当前仅支持2个
        'cly_account' => 'xxxxxxxxxx', //SMS诚立业帐号
        'cly_password' => 'xxxxxxxxx', //SMS诚立业密码
        'aliyun_info'=>[
            'access_key_id'=>'xxxxxxxxxxx',
            'access_key_secret'=>'xxxxxxxxxxxxxxx',
            'template_code'=>'SMS_0000000',
            'sign_name'=>'短信签名',
            'region'=>'cn-hangzhou'
        ],
        'validcode_expiretime'=> 60*5,//单位秒
        'time_counter'=>120, //倒计时（单位秒）
        'validcode_length' => 4,
        'bypass_verify'=>'010101010101010',
    ],
    //支付
    'ca_pay' => [
        ///////////////
        //支付宝支付
        ///////////////
        'alipay_signType' => 'RSA2', //RSA、RSA2
        'alipay_appId' => '20191113693164120',
        //类似MIICXAIBAAKBgQDGLFBZ......(RSA)
        //类似MIIEpAIBAAKCAQEAr5l......(RSA2)
        //应用私钥
        'alipay_rsaPrivateKey' => 'MIIEogIBAAKCAQEAnLJ3A2pnfyb9Lis9DJF4i5p02wT8AAvReItVZj4GwcoB9QdhsNDHCRGc1OGeY7CiWL5Fl57BetZxMxxd4mw0Iow7x0MPxNx6B/hJ/WnSrWSTkTchC3l8IfCMinRn8lpLUiLIxTgYdx8fegJhVvg8nLw8c2HyHZj6CuGL9Y8D04oOQvtsySXuvKZX2/+iPOWQWQ/mzSWTTl3S1SxwzlzpDxBwyS+bm9pSIC/2gi5+Xov43zxSg/hUJAxvjAwXESrxP6iFa9xWK/3BC/8JXg/wqF5S53euKrlEYYXXzFOxUqnpW+k7lcF5fz+1AHpcmhjblVUzFBYPaly8yCnwkfd6gQIDAQABAoIBAETdYdSMdMO63NXnhsL5Q6MG1IhU3nMevoZdpcBzTgDoyIdpIRPWDUL9DJG6PiPNwuuBY5BI4vsbN1ChDNmveBIe0z61SD7aLn9GLS5DzaUKaJgPRGSIlXdNAlbu5afq6uNECjwydP4VHX4/gYv5OJohV+fVidRcILebfCkFG0dh2Se+5oStE2ToRvUcI6By/teKJec5lYZEg+a3fdH4TigFijgkg6i2Z9m1lldvW4XI4hSUAiDWWf9v9gTlMmCtOSQWYS76koth8YvaGComtHl2bKFv0VYZg188BrbU3S+UDVWfugcqnbOlqGxG5Uh4T/GH4N4Q0Je9S7seCLJ6X2ECgYEA5rai7ztXMvBx1K6Er17wvZPTJnhY+53K9hTDiOMvovZR+B/sR1qJ7PzHEKAGGqExmA+p1C6l0w6D2jYMR/752Uw4O1spUspbnS5uHFtASOdkjziCO6T566NKxbU7sSnhG6qZfBu5o3yFEqzs+ZVd4TixCUbZHQxs8uOXDYGqI7UCgYEArd8Tw5RaWZUZy6PqZq0MK5PKvMyeHjTHCPNvEdKgVxscaxCgiIZpMDduX2meIgcD8ybxcxK+r1Khwn+fARhqx7Iq3FGM3nW7+pui2Cy4c4rowFxGreWXTQEBOet5TywP4j7Ej4sOCEM6YC4xKn6lQFl6hu77FCxJq+r+Qkm/Ex0CgYAvVpQR8CQRMKK6l+Bjt2BAFb/CafkgD51gZpxETxEsJ8xj0/EAJwSb2rXhpGP8PMzXPYeEINondxf4wxQ8BYYwXuI+05KkCR9JHgi7ysFi9A9/D4IxSMV64vNpo6kOzeABdDyv6WRqthb74P1yrIIpvx8cHyxXOIB2SdppjAxj7QKBgGVag4/f2+cahkXyEEK4Hz8L0QPLl2RuSZPLK/TMvhDoyeETeTD8gTbNKTbPP5IAWuoTym3VAHj9BzR05oBh9mTgVCHIHADVyrVkokG3l/IqiFERuVXHXzJRIqDbxgRD4CJ3imdOnOhuIApPAvQNjVD8l/MuPghdZLilrH+Bs7N1AoGAFmDqrL8FNsWebc+W79JwXtCbZE/VxvWEQwo/smx7vSXmrvuDYwkNLtm8BozoWW1+F/KKbIilrA/Mmt4Y/yV/XlZLmiOCaCED16JeWhJerTEgJs1+wLQBQHTy1CV0uIuV0tFE4ZgYlJ0lc0o0uzI3I2yCrIsjz9j6qUJc3Uwf6UM=', //'请填写开发者私钥去头去尾去回车，一行字符串'
        //类似MIGfMA0GCSqGSIb3DQEB......(RSA)
        //类似MIIBIjANBgkqhkiG9w0B......(RSA2)
        //支付宝公钥，非应用公钥
        'alipay_alipayRsaPublicKey' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkMyFaBHq7DQ5iNpYFTtVVt1JMyFFCTmJ/MZ7NDcYf7JyJk6KRrecctxqMifNJiJSdxOf3XtiXRoFEqLCAc7OQ3ICtnxlyWsc2IO6l0LyCUGtHJlYzZ843aQdNGa2qGxMZHd0bjvwt2fB75G+8KN842rk5wVramR+o2TXTjFv+7FeuTBkgAmrZtrmM/su6l71YTerscvPlukC/Jf3tV42Lx2KIG0vGrn/5BhR5sdevz713IilPw20re/scHAOVxzJQzSBo5KygdLcP4lIkzL+Fa99AanlMpYhTSFbhIh1ITHss20RpSB5dRsWb7aIcVDEC7DrGm3u1XzQjKg0TqtEfwIDAQAB', //'请填写支付宝公钥，一行字符串'
        'alipay_rsaPrivateKeyFilePath' => '',
        'alipay_alipayRsaPublicKeyFilePath' => '',
        'alipay_notifyUrl'=>'http://xxxx.domain.com/notifyAlipay.php', //异部通知
        ///////////////
        //微信支付
        ///////////////
        'wxpay_appId' => 'wx1111111111111',
        'wxpay_merchantId' => '1111111111',
        'wxpay_signType' => 'MD5', //仅有MD5，无RSA、RSA2
        'wxpay_signSecretKey' => 'xxxxxxxxxxxxxxxxx', //32位API密钥
        'wxpay_notifyUrl' => 'http://xxxx.domain.com/notifyWechat.php',
        ///////////////
        //微信小程序支付
        ///////////////
        'wxmp_miniAppId' => '',
        'wxmp_miniAppSecret' => '',
        'wxmp_notifyUrl' => 'notifyWechat.php',
        ///////////////
        //银行卡支付 (待实现)
        ///////////////
        'bank_' => '',
    ],
    //分享
    'ca_share' => [
        'mob_accesskey' => '', //mob一键分享（极光）
    ],
    //推送
    'ca_jpush' => [
        'mob_account' => '', //推送账号（极光）
        'mob_password' => '', //推送口令（极光）
    ],
    //快递
    'ca_express' => [
        'use_express' => 'jisu', //选项（jisu》极速；）
        'jisu_accesskey' => 'xxxxxxxxxxxxxxx', //极速数据
    ],
    //地图
    'ca_map' => [
        'use_map' => 'gaode', //选项（gaode》高德；baidu》百度；）
        'gaode_accesskey' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', //高德
        'baidu_accesskey' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', //百度
    ],
    //第三方
    'ca_third' =>[
        //微信
        'wechat'=>[
            'wx_mp_appid'=>'wx1111111111111111111',
            'wx_mp_appsecret'=>'xxxxxxxxxxxxxxxxxxxxxxxx',
        ]
    ],
    // +----------------------------------------------------------------------
    // | 框架固定设置
    // +----------------------------------------------------------------------
    'ca_pagesize' => 10, //分页设置
    //上传
    'ca_upload' =>[
        //选项：local.local》runtime/storage中（禁止公共访问）；local.public》public中（可公共访问）；oss_inspur》浪潮云；oss_aliyun》阿里云；oss_baidu》百度云；oss_tencent》腾讯云；
        'use_upload'=>'local.public',
        'file_max_size' => 1024 * 1024 * 1, //1MB
        'file_extension' => 'gif,jpg,jpeg,bmp,png',
        'file_mime_type' => 'image/gif,image/jpeg,image/bmp,image/png',
    ]
];
