<?php
/**
 * +----------------------------------------------------------------------
 * | @Author: codeavatar   @Year：2021
 * +----------------------------------------------------------------------
 * | @Email: codeavatar@aliyun.com
 * +----------------------------------------------------------------------
 **/

// +----------------------------------------------------------------------
// | caadmin 框架核心配置
// +----------------------------------------------------------------------

return [
    // 开发模式
    'app_dev'           => env('apiapp.app_dev',false),
    // 应用版本
    'app_version'       => env('apiapp.app_version','v1.0'),
    //应用地址
    'app_url'=>[
        //应用极目录(含域名)
        'web_url'           => str_replace('index.php','',app('request')->baseFile(true))
    ],
    //用户终端配置
    'member' =>[
        'android_version' => '1.0',
        'android_force_upgrade_version' => [],
        'android_apk' => 'http://www.domain.com/appName.apk',
        'ios_version' => '1.0',
        'ios_force_upgrade_version' => [],
        'ios_ipa' => 'http://www.domain.com/appName.ipa', //AppStore下载地址
        'miniprogram_version' => '1.0',
        'miniprogramforce_upgrade_version' => []
    ],
    //商户终端配置
    'merchant' =>[
        'android_version' => '1.0',
        'android_force_upgrade_version' => [],
        'android_apk' => 'http://www.domain.com/appName.apk',
        'ios_version' => '1.0',
        'ios_force_upgrade_version' => [],
        'ios_ipa' => 'http://www.domain.com/appName.ipa', //AppStore下载地址
        'miniprogram_version' => '1.0',
        'miniprogramforce_upgrade_version' => []
    ],
    //DB缓存时长（单位秒）
    'app_db_cache_time'     => 3600,
    // 混淆密钥
    'app_pwd_confuse_str'   => env('caapi.app_pwd_confuse_str','I am coder, so I love to write code very much. The year is 2021.'),
];
